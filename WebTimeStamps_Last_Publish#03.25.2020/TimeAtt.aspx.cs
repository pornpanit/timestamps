﻿using MySql.Data.MySqlClient;
using MySqlDatabase;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static WebTimeStamps.Logik;

namespace WebTimeStamps
{
    public partial class TimeAtt : System.Web.UI.Page
    {
        public const string SELECTED_STAFF_INDEX = "SelectedStaffIndex";

        public string m_sSelectionCmd = "";
        public List<int> m_Clickable = null;
        public bool m_bCheckAll = false;
        public class checkboxValue {
            public bool bState = false;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!this.IsPostBack)
            {
                try
                {
                    // + Convert.ToString(Session["password"]);
                    bool bStatus = false;

                    string sUserName = "";
                    string sSamaccountname = "";
                    string sName = "";
                    string sEmail = "";
                    string sPassword = "";
                    string sTitle = "";

                    sUserName = Convert.ToString(Session["user"]);
                    sPassword = Convert.ToString(Session["password"]);
                    sSamaccountname = Convert.ToString(Session["samaccountname"]);
                    sName = Convert.ToString(Session["name"]);
                    sEmail = Convert.ToString(Session["email"]);
                    sTitle = Convert.ToString(Session["title"]);
                    if (ValidateSession())
                    {
                        if (String.Equals(lb_username_surname.Text, "Label", StringComparison.OrdinalIgnoreCase))
                        {
                            // lb_username_surname.Text = sUserName;

                            Logik process = new Logik();
                            bStatus = process.CallUserInfo(sName);
                            if (!bStatus)
                            {
                                txtDateStart.Enabled = false;
                                txtDateEnd.Enabled = false;
                                btn_load.Enabled = false;
                                lb_username_surname.Text = "";

                                string redirectScript = "window.location.href = 'LoginAttendance.aspx';";
                                System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "alert('ไม่พบข้อมูลในระบบ กรุณาติดต่อเจ้าหน้าที่ดูแลระบบ');" + redirectScript, true);
                            }
                            else
                            {
                                string sStartDate = "";
                                string sEndDate = "";

                                if (process.m_personInfo != null)
                                {
                                    lb_username_surname.Text = "สวัสดี : คุณ" + process.m_personInfo.fullNameTH;

                                    if (string.IsNullOrEmpty(process.m_personInfo.id))
                                    {
                                        txt_userid.Text = "*";
                                    }
                                    else
                                    {
                                        txt_userid.Text = process.m_personInfo.id;
                                    }

                                    Session["userid"] = txt_userid.Text;

                                    lbl_PS.Text = sTitle;
                                    lbl_name.Text = process.m_personInfo.usernameTH;
                                    lbl_surname.Text = process.m_personInfo.SurnameTH;
                                    Session["usernamesurenameth"] = lb_username_surname.Text;
                                }

                                DateTime dtCurrent = DateTime.Now;
                                // txtDateStart.Text = dtCurrent.ToString("dd/MM/yyyy");
                                //txtDateEnd.Text = dtCurrent.ToString("dd/MM/yyyy");

                                process.GetStartAndEndDate(ref sStartDate, ref sEndDate);
                                txtDateStart.Text = sStartDate;
                                txtDateEnd.Text = sEndDate;

                                txtDateStartStaff.Text = dtCurrent.ToString("dd/MM/yyyy");
                                txtDateEndStaff.Text = dtCurrent.ToString("dd/MM/yyyy");

                                //Get current date data
                                BindGrid(true);
                                //YPT 03.24.2020
                                GetStaffUnder(ref process, sEmail, sPassword);
                                if (process.m_personInfo.arrPersonInfo != null)
                                {
                                    if (process.m_personInfo.arrPersonInfo.Count > 0)
                                    {
                                        EnableStaffMode(true);
                                    }
                                }
                            }
                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    // MessageBox.Show(ex.Message);

                }

            }
        }
        protected void GetStaffUnder(ref Logik process, string sEmail, string sPwd)
        {
            bool bStatus = false;
            string sInputjSon = "";
            sInputjSon = process.generateInput(sEmail, sPwd);
            bStatus = process.CallADServiceStaffUnder(sInputjSon);
            if (bStatus)
            {
                AddDataStaffUnderToTable(process.m_personInfo.arrPersonInfo, process.m_arrListStaff);

                dpp_Staff.Items.Clear();
                if (process.m_personInfo.arrPersonInfo != null)
                {
                    if (process.m_personInfo.arrPersonInfo.Count() > 0)
                    {
                        AddStaffToList(process.m_personInfo.arrPersonInfo);
                    }
                }
            }
        }
        protected void AddStaffToList(List<PersonInfoWithUnder> arrPersonInfos)
        {
            string sText = "";
            string sValue = "";
            foreach (PersonInfoWithUnder itemDetails in arrPersonInfos)
            {
                sValue = itemDetails.id;
                sText = itemDetails.fullNameTH;
                dpp_Staff.Items.Add(new ListItem(sText, sValue));
                if (itemDetails.arrPersonInfo != null)
                {
                    if (itemDetails.arrPersonInfo.Count() > 0)
                    {
                        AddStaffToList(itemDetails.arrPersonInfo);
                    }
                }
            }
        }
        protected void btnload_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid(true);

                btnUpdate.Visible = false;
                btnCancle.Visible = false;
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        protected void btnloadStaff_Click(object sender, EventArgs e)
        {
            try
            {
               BindGridStaffUnder();
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        protected void SubmitAppraisalGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindGrid(false);
                myGridView.PageIndex = e.NewPageIndex;
                //myGridView.DataSource = m_dtTableDataSource;
                myGridView.DataBind();

                btnUpdate.Visible = false;
                btnCancle.Visible = false;
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }

        public bool ValidateWorkingHours(DateTime dtTime)
        {
            bool bStatus = false;
            int nStatus = 0;

            try
            {
                TimeSpan tBase = new TimeSpan(9, 0, 0);
                TimeSpan tHours = new TimeSpan(dtTime.Hour, dtTime.Minute, dtTime.Second);

                nStatus = TimeSpan.Compare(tHours, tBase);
                if ((nStatus == 1) || (nStatus == 0))
                {
                    bStatus = true;
                }
                else
                {
                    bStatus = false;
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }

        protected void OnUpdate(object sender, EventArgs e)
        {
            try
            {
                    //GridViewRow row = myGridView.Rows[myGridView.EditIndex];
                foreach (GridViewRow row in myGridView.Rows)
                { 
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        string sComment = "";
                        string sStartTime = "";
                        string sEndTime = "";
                        string sTemperature = "";
                        string sDate = "";
                        string sUserID = "";
                        DateTime dtDateTime = DateTime.Now;
                        bool bTimeValid = true;

                        Logik process = new Logik();

                        for (int i = 0; i < row.Cells.Count; i++)
                        {
                            if (i == 0)
                            {
                                sDate = row.Cells[i].Text;
                                dtDateTime = DateTime.ParseExact(sDate, "dd-MM-yyyy", null);
                                sDate = dtDateTime.ToString("yyyy-MM-dd");
                            }
                            if (i == 1)
                            {
                                sUserID = row.Cells[i].Text;
                            }

                            if (row.Cells[i].Controls.OfType<TextBox>().ToList().Count > 0)
                            {
                                if (i == 11)
                                {
                                    //YPT 03.25.2020#FixThaiproblem???
                                    sComment = row.Cells[i].Controls.OfType<TextBox>().FirstOrDefault().Text;
                                }
                            }
                            if (row.Cells[i].Controls.OfType<Label>().ToList().Count > 0)
                            {
                               if (i == 7)
                                {
                                    sStartTime = row.Cells[i].Controls.OfType<Label>().FirstOrDefault().Text;
                                    if (String.Equals(sStartTime, "-", StringComparison.OrdinalIgnoreCase))
                                    {
                                        sStartTime = "00:00";
                                    }
                                    else
                                    {
                                        bTimeValid = process.IsValidTime(sStartTime);
                                        if (!bTimeValid)
                                        {
                                            i = row.Cells.Count;
                                        }
                                    }

                                }
                                else if (i == 8)
                                {
                                    sEndTime = row.Cells[i].Controls.OfType<Label>().FirstOrDefault().Text;
                                    if (String.Equals(sEndTime, "-", StringComparison.OrdinalIgnoreCase))
                                    {
                                        sEndTime = "00:00";
                                    }
                                    else
                                    {
                                        bTimeValid = process.IsValidTime(sEndTime);
                                        if (!bTimeValid)
                                        {
                                            i = row.Cells.Count;
                                        }
                                    }
                                }
                            }
                            if (bTimeValid)
                            {
                                if (row.Cells[i].Controls.OfType<DropDownList>().ToList().Count > 0)
                                {
                                    if (i == 10)
                                    {
                                        sTemperature = row.Cells[i].Controls.OfType<DropDownList>().FirstOrDefault().Text;

                                        // sTemperature = row.Cells[i].Controls.OfType<Label>().FirstOrDefault().Text;
                                        sTemperature = GetNameTemperature(sTemperature);
                                    }
                                }
                            }
                        }
                        if (bTimeValid)
                        {
                            bool bStatus = false;
                            int nStatus = 0;
                            DateTime dtWorkingHours = DateTime.Now;
                            TimeSpan tWorkignHours = new TimeSpan(0, 0, 0);
                            DateTime dtDeafultStartTime = DateTime.Now;
                            DateTime dtStartTime = DateTime.Now;
                            DateTime dtEndTime = DateTime.Now;

                            dtStartTime = DateTime.ParseExact(sStartTime, "HH:mm", null);
                            dtEndTime = DateTime.ParseExact(sEndTime, "HH:mm", null);
                            dtWorkingHours = DateTime.ParseExact("00:00", "HH:mm", null);

                            if ((!String.Equals(sStartTime, "00:00", StringComparison.OrdinalIgnoreCase)) && (!String.Equals(sEndTime, "00:00:00", StringComparison.OrdinalIgnoreCase)))
                            {
                                dtDeafultStartTime = process.CreateTimeFromDate(7, 0, 0, dtDateTime);
                                dtStartTime = process.CreateTimeFromDate(dtStartTime.Hour, dtStartTime.Minute, dtStartTime.Second, dtDateTime);
                                dtEndTime = process.CreateTimeFromDate(dtEndTime.Hour, dtEndTime.Minute, dtEndTime.Second, dtDateTime);

                                bStatus = process.CalculateWorkingHours(dtDeafultStartTime, dtStartTime, dtEndTime, ref tWorkignHours);
                                if (bStatus)
                                {
                                    nStatus = 0;
                                }
                                else
                                {
                                    nStatus = 1;
                                }
                                dtWorkingHours = process.CreateTimeFromDate(tWorkignHours.Hours, tWorkignHours.Minutes, tWorkignHours.Seconds, dtDateTime);
                            }
                            string sCmd = "UPDATE timestampinfo SET comment = ";
                            sCmd += "'";
                            sCmd += sComment;
                            sCmd += "',";
                            sCmd += "starttimem='";
                            sCmd += dtStartTime.ToString("HH:mm:ss");
                            sCmd += "',";
                            sCmd += "endtimem='";
                            sCmd += dtEndTime.ToString("HH:mm:ss");
                            sCmd += "',";
                            sCmd += "status_health='";
                            sCmd += sTemperature;
                            sCmd += "',";
                            sCmd += "statusinfo='";
                            sCmd += nStatus.ToString();
                            sCmd += "',";
                            sCmd += "workinghoursm='";
                            sCmd += dtWorkingHours.ToString("HH:mm:ss");
                            sCmd += "'";
                            sCmd += " WHERE userid = ";
                            sCmd += "'";
                            sCmd += sUserID;
                            sCmd += "' and datetime=";
                            sCmd += "'";
                            sCmd += sDate;
                            sCmd += "'";
                            process.UpdateData(sCmd, "UPDATE");

                            btnUpdate.Visible = false;
                            btnCancle.Visible = false;
                            this.BindGrid(false);
                        }
                        else
                        {
                            Response.Write("<script>alert('ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูล (Format time : 00:00)');</script>");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        private void BindGrid(bool bSearch)
        {
            bool bStatus = false;
            DateTime dtStart = DateTime.Now;
            DateTime dtEnd = DateTime.Now;
            string sStartDate = "";
            string sEndDate = "";
            bool bFoundData = false;

            try
            {

                sStartDate = txtDateStart.Text;
                sEndDate = txtDateEnd.Text;

                if (!string.IsNullOrEmpty(sStartDate))
                {
                    lbl_err.Text = "";
                    lbl_err.Visible = false;

                    if (string.IsNullOrEmpty(sStartDate))
                    {
                        sEndDate = DateTime.Now.ToString("dd/MM/yyyy");
                    }

                    dtStart = DateTime.ParseExact(sStartDate, "dd/MM/yyyy", null);
                    dtEnd = DateTime.ParseExact(sEndDate, "dd/MM/yyyy", null);

                    Logik logikProcess = new Logik();
                    bStatus = logikProcess.ValidateDateTime(dtStart, dtEnd);
                    if (!bStatus)
                    {
                        lbl_err.Text = "วันที่ไม่ถูกต้อง";
                        lbl_err.Visible = true;

                        myGridView.Visible = false;
                    }
                    else
                    {
                        lbl_err.Text = "";
                        lbl_err.Visible = false;

                        DataTable dtTableInfo = null;
                        string sUserId = "000" + txt_userid.Text; //00000024
                                                                  //Get data from database
                        dtTableInfo = logikProcess.SelectPersonalData(dtStart.ToString("yyyy-MM-dd"), dtEnd.ToString("yyyy-MM-dd"), sUserId);

                        if (dtTableInfo != null)
                        {
                            DataTable dt = new DataTable();
                            dt.Columns.AddRange(new DataColumn[12] { new DataColumn("datetime"), new DataColumn("userid"), new DataColumn("username"), new DataColumn("surname"), new DataColumn("starttime"), new DataColumn("endtime"), new DataColumn("workingh"), new DataColumn("starttimem"), new DataColumn("endtimem"), new DataColumn("workinghm"), new DataColumn("status_health"), new DataColumn("comment") });

                            string sStartDateTime = "";
                            string sEndDateTime = "";

                            foreach (DataRow row in dtTableInfo.Rows)
                            {
                                DateTime dtDatetime = (DateTime)row["datetime"];
                                string sDatetimeS = dtDatetime.ToString("dd-MM-yyyy");
                                string sUserid = row["userid"].ToString();
                                string sUsername = row["username"].ToString();
                                string sSurname = row["surname"].ToString();

                                TimeSpan tTime = (TimeSpan)row["starttime"];
                                string sStarttime = tTime.ToString("hh\\:mm");
                                if (String.Equals(sStarttime, "00:00", StringComparison.OrdinalIgnoreCase))
                                {
                                    sStarttime = "-";
                                }
                                tTime = (TimeSpan)row["endtime"];
                                string sEndtime = tTime.ToString("hh\\:mm");
                                if (String.Equals(sEndtime, "00:00", StringComparison.OrdinalIgnoreCase))
                                {
                                    sEndtime = "-";
                                }
                                if (String.Equals(sStarttime, sEndtime, StringComparison.OrdinalIgnoreCase))
                                {
                                    sEndtime = "-";
                                }

                                string sWorkingh = "00:00";
                                tTime = (TimeSpan)row["workinghours"];
                                if (tTime != null)
                                {
                                   sWorkingh = tTime.ToString("hh\\:mm");
                                }

                                //YPT 03.22.2020
                                string sStarttimem = "00:00";
                                tTime = (TimeSpan)row["starttimem"];
                                if (tTime != null)
                                {
                                    sStarttimem = tTime.ToString("hh\\:mm");
                                }
                                
                                if (String.Equals(sStarttimem, "00:00", StringComparison.OrdinalIgnoreCase))
                                {
                                    sStarttimem = "-";
                                }

                                string sEndtimem = "00:00";
                                tTime = (TimeSpan)row["endtimem"];
                                if (tTime != null)
                                {
                                    sEndtimem = tTime.ToString("hh\\:mm");
                                }
                                   
                                if (String.Equals(sEndtimem, "00:00", StringComparison.OrdinalIgnoreCase))
                                {
                                    sEndtimem = "-";
                                }

                                string sWorkinghoursm = "00:00";
                                tTime = (TimeSpan)row["workinghoursm"];
                                if (tTime != null)
                                {
                                    sWorkinghoursm = tTime.ToString("hh\\:mm");
                                }

                                string stemperature = "";
                                stemperature = row["status_health"].ToString();
                                if (String.IsNullOrEmpty(stemperature))
                                {
                                    stemperature = "0";
                                }
                                stemperature = GetNameTemperature(stemperature);

                                string scomment = row["comment"].ToString();
                                if (string.IsNullOrEmpty(scomment))
                                {
                                    scomment = "[double click]";
                                }
                                dt.Rows.Add(sDatetimeS, sUserid, sUsername, sSurname, sStarttime, sEndtime, sWorkingh, sStarttimem, sEndtimem, sWorkinghoursm, stemperature, scomment);
                                bFoundData = true;
                            }

                            myGridView.PageSize = logikProcess.GetManDay(DateTime.Now); //limit items
                            myGridView.AllowPaging = true;
                            myGridView.DataSource = dt;
                            if (bSearch)
                            {
                                myGridView.PageIndex = 0;
                            }
                            myGridView.DataBind();
                            myGridView.Visible = true;
                        }
                        if (!bFoundData)
                        {
                            lbl_err.Text = "ไม่พบข้อมูล";
                            lbl_err.Visible = true;
                        }
                        else
                        {
                            lbl_err.Visible = false;
                        }

                    }
                }
                else
                {
                    lbl_err.Text = "กรุณาใส่ข้อมูล";
                    lbl_err.Visible = true;
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        protected string GetNameTemperature(string sValue)
        {
            string sReturn = "";
            if (String.Equals(sValue, "0", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "ไม่มีไข้";
            }
            else if (String.Equals(sValue, "1", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "มีไข้";
            }
            else if (String.Equals(sValue, "ไม่มีไข้", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "0";
            }
            else if (String.Equals(sValue, "มีไข้", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "1";
            }
            else
            {
                sReturn = "0";
            }
            return sReturn;
        }
        protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
           
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                try
                {
                    bool bStatusStart = false;
                    bool bStatusWorking = false;

                    if ((!string.IsNullOrEmpty(e.Row.Cells[4].Text)) && (!String.Equals(e.Row.Cells[4].Text, "-", StringComparison.OrdinalIgnoreCase)))
                    {
                        DateTime dtStart = DateTime.Parse(e.Row.Cells[4].Text);
                        bStatusStart = ValidateEntryTime(dtStart);
                        DateTime dtHours = DateTime.Parse(e.Row.Cells[6].Text);
                        bStatusWorking = ValidateWorkingHours(dtHours);
                    }
                    if (!String.Equals(e.Row.Cells[4].Text, "-", StringComparison.OrdinalIgnoreCase))
                    {
                        if (!bStatusStart)
                        {
                            e.Row.Cells[4].ForeColor = System.Drawing.Color.Red;
                        }
                        if (bStatusWorking)
                        {
                            if (!bStatusStart)
                            {
                                e.Row.Cells[6].ForeColor = System.Drawing.Color.Red;
                            }
                            else
                            {
                                e.Row.Cells[6].ForeColor = System.Drawing.Color.Green;
                            }

                        }
                        else
                        {
                            e.Row.Cells[6].ForeColor = System.Drawing.Color.Red;
                        }
                    }
                    
                    if (e.Row.Cells[7].Controls.OfType<Label>().ToList().Count > 0)
                    { 
                        string sLabel7 = "";
                        sLabel7 = e.Row.Cells[7].Controls.OfType<Label>().FirstOrDefault().Text;

                        if ((!string.IsNullOrEmpty(sLabel7)) && (!String.Equals(sLabel7, "-", StringComparison.OrdinalIgnoreCase)))
                        {
                            DateTime dtStart = DateTime.Parse(sLabel7);
                            bStatusStart = ValidateEntryTime(dtStart);
                            DateTime dtHours = DateTime.Parse(e.Row.Cells[9].Text);
                            bStatusWorking = ValidateWorkingHours(dtHours);
                        }
                        if (!String.Equals(sLabel7, "-", StringComparison.OrdinalIgnoreCase))
                        {
                            if (!bStatusStart)
                            {
                                e.Row.Cells[7].ForeColor = System.Drawing.Color.Red;
                            }
                            if (bStatusWorking)
                            {
                                if (!bStatusStart)
                                {
                                    e.Row.Cells[9].ForeColor = System.Drawing.Color.Red;
                                }
                                else
                                {
                                    e.Row.Cells[9].ForeColor = System.Drawing.Color.Green;
                                }

                            }
                            else
                            {
                                e.Row.Cells[9].ForeColor = System.Drawing.Color.Red;
                            }
                        }
                    }

                    e.Row.Cells[11].Attributes["ondblclick"] = Page.ClientScript.GetPostBackClientHyperlink(myGridView, "Edit$" + e.Row.RowIndex);
                    e.Row.Cells[11].Attributes["style"] = "cursor:pointer";
                    
                    DropDownList ddltemperatures = (e.Row.FindControl("ddltemperatures") as DropDownList);

                    //Add Default Item in the DropDownList
                    ddltemperatures.Items.Insert(0, new ListItem("ไม่มีไข้"));
                    ddltemperatures.Items.Insert(1, new ListItem("มีไข้"));

                    //Select the Country of Customer in DropDownList
                    string sTemperature = (e.Row.FindControl("lbl_temperature") as Label).Text;
                    ddltemperatures.Items.FindByValue(sTemperature).Selected = true;

                }
                catch (Exception ex)
                {
                    // MessageBox.Show(ex.Message);

                }
            }
        }

        public void EnableEdit(int nIndex)
        {
            bool isUpdateVisible = false;
            try
            {
                GridViewRow row = myGridView.Rows[nIndex];

                if (row.RowType == DataControlRowType.DataRow)
                {
                    bool isChecked = true;
                    for (int i = 1; i < row.Cells.Count; i++)
                    {
                        if (row.Cells[i].Controls.OfType<Label>().ToList().Count > 0)
                        {
                            row.Cells[i].Controls.OfType<Label>().FirstOrDefault().Visible = !isChecked;
                            if (i == 11)
                            {
                                row.Cells[i].Controls.OfType<Label>().FirstOrDefault().Width = Unit.Pixel(280);
                            }
                            
                        }

                        if (row.Cells[i].Controls.OfType<TextBox>().ToList().Count > 0)
                        {
                            row.Cells[i].Controls.OfType<TextBox>().FirstOrDefault().Visible = isChecked;
                            if (i == 11)
                            {
                                row.Cells[i].Controls.OfType<TextBox>().FirstOrDefault().Width = Unit.Pixel(280);
                            }

                            if (String.Equals(row.Cells[i].Controls.OfType<TextBox>().FirstOrDefault().Text, "[double click]", StringComparison.OrdinalIgnoreCase))
                            {
                                row.Cells[i].Controls.OfType<TextBox>().FirstOrDefault().Text = "";
                            }
                        }
                        if (isChecked && !isUpdateVisible)
                        {
                            isUpdateVisible = true;
                        }
                    }
                }

                btnUpdate.Visible = isUpdateVisible;
                btnCancle.Visible = isUpdateVisible;
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        protected void ddltemperature_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnUpdate.Visible = true;
            btnCancle.Visible = true;
            /* DropDownList ddlMappingStatus = (DropDownList)sender;
             if (ddlMappingStatus.SelectedItem.Text.ToUpper() == "DONE")
             {
             }

             GridViewRow row = (GridViewRow)ddlMappingStatus.NamingContainer;*/
        }

        protected void OnRowEditing(object sender, GridViewEditEventArgs e)
        {
            myGridView.EditIndex = e.NewEditIndex;
            this.BindGrid(false);
            EnableEdit(e.NewEditIndex);
        }
        protected void OnCancel(object sender, EventArgs e)
        {
            myGridView.EditIndex = -1;
            this.BindGrid(false);
            btnUpdate.Visible = false;
            btnCancle.Visible = false;
        }

        public bool ValidateEntryTime(DateTime dtTime)
        {
            bool bStatus = false;
            int nStatus = 0;

            try
            {
                TimeSpan tBase = new TimeSpan(9, 31, 0);
                TimeSpan tHours = new TimeSpan(dtTime.Hour, dtTime.Minute, dtTime.Second);

                nStatus = TimeSpan.Compare(tHours, tBase);
                if ((nStatus == 1) || (nStatus == 0))
                {
                    bStatus = false;
                }
                else
                {
                    bStatus = true;
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }
        public void Onlogout(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("LoginAttendance.aspx");
        }
        public void OnTimeSheet(object sender, EventArgs e)
        {
            Response.Redirect("TimeSheets.aspx");
        }
        public void OnReport(object sender, EventArgs e)
        {
            Response.Redirect("ReportViewer.aspx");
        }
        protected void SubmitAppraisalGridStaff_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindStaffInfo();
                foreach (GridViewRow row in GridViewStaff.Rows)
                {
                    var chkBox = row.FindControl("chkSelect") as CheckBox;

                    IDataItemContainer container = (IDataItemContainer)chkBox.NamingContainer;

                    if (chkBox.Checked)
                    {
                        PersistRowIndex(container.DataItemIndex);
                    }
                    else
                    {
                        RemoveRowIndex(container.DataItemIndex);
                    }
                }
                GridViewStaff.PageIndex = e.NewPageIndex;
                GridViewStaff.DataBind();
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        private List<Int32> SelectedCustomersIndex
        {
            get
            {
                if (ViewState[SELECTED_STAFF_INDEX] == null)
                {
                    ViewState[SELECTED_STAFF_INDEX] = new List<Int32>();
                }

                return (List<Int32>)ViewState[SELECTED_STAFF_INDEX];
            }
        }
        private void RemoveRowIndex(int index)
        {
            SelectedCustomersIndex.Remove(index);
        }
        private void PersistRowIndex(int index)
        {
            if (!SelectedCustomersIndex.Exists(i => i == index))
            {
                SelectedCustomersIndex.Add(index);
            }
        }
        public void AddDataStaffUnderToTable(List<PersonInfoWithUnder> lsPerson, List<PersonInfoWithUnder> lsPersonStaff)
        {
            AddStaffUnder(lsPerson, lsPersonStaff);
        }
        public void AddStaffUnder(List<PersonInfoWithUnder> lsPerson, List<PersonInfoWithUnder> lsPersonStaff)
        {
            try
            {
                foreach (PersonInfoWithUnder item in lsPerson)
                {
                    AddDataList(item, lsPersonStaff);
                    if (item.arrPersonInfo != null)
                    {
                        if (item.arrPersonInfo.Count > 0)
                        {
                            AddStaffUnder(item.arrPersonInfo, lsPersonStaff);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        public void AddDataList(PersonInfoWithUnder item, List<PersonInfoWithUnder> lsPerson)
        {
            string sId = "";
            try
            {

                foreach (PersonInfoWithUnder itemDetails in lsPerson)
                {
                    if (String.Equals(item.name, itemDetails.fullNameEN, StringComparison.OrdinalIgnoreCase))
                    {
                        sId = itemDetails.id;
                        item.fullNameTH = itemDetails.fullNameTH;
                        item.id = sId;
                        item.depNameTH = itemDetails.depNameTH;

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        protected void SubmitAppraisalGridStaffUnder_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindGridStaffUnder();
                GridViewStaffUnder.PageIndex = e.NewPageIndex;
                GridViewStaffUnder.DataBind();
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        protected void OnCheckedChanged(object sender, EventArgs e)
        {
            bool bCheckAll = false;
            int nIndexRow = 0;
            bool isUpdateVisible = false;
            CheckBox chk = (sender as CheckBox);
            if (chk.ID == "chkAll")
            {
                foreach (GridViewRow row in GridViewStaff.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        row.Cells[0].Controls.OfType<CheckBox>().FirstOrDefault().Checked = chk.Checked;
                    }
                }
                bCheckAll = true;
            }
        }
        protected void OnRowDataBoundStaffUnders(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                try
                {
                    bool bStatusStart = false;
                    bool bStatusWorking = false;
                    if ((!string.IsNullOrEmpty(e.Row.Cells[4].Text)) && (!String.Equals(e.Row.Cells[4].Text, "-", StringComparison.OrdinalIgnoreCase)))
                    {
                        DateTime dtStart = DateTime.Parse(e.Row.Cells[4].Text);
                        bStatusStart = ValidateEntryTime(dtStart);
                        DateTime dtHours = DateTime.Parse(e.Row.Cells[6].Text);
                        bStatusWorking = ValidateWorkingHours(dtHours);
                    }
                    if (!String.Equals(e.Row.Cells[4].Text, "-", StringComparison.OrdinalIgnoreCase))
                    {
                        if (!bStatusStart)
                        {
                            e.Row.Cells[4].ForeColor = System.Drawing.Color.Red;
                        }
                        if (bStatusWorking)
                        {
                            if (!bStatusStart)
                            {
                                e.Row.Cells[6].ForeColor = System.Drawing.Color.Red;
                            }
                            else
                            {
                                e.Row.Cells[6].ForeColor = System.Drawing.Color.Green;
                            }

                        }
                        else
                        {
                            e.Row.Cells[6].ForeColor = System.Drawing.Color.Red;
                        }
                    }
                    if ((!string.IsNullOrEmpty(e.Row.Cells[7].Text)) && (!String.Equals(e.Row.Cells[7].Text, "-", StringComparison.OrdinalIgnoreCase)))
                    {
                        DateTime dtStart = DateTime.Parse(e.Row.Cells[7].Text);
                        bStatusStart = ValidateEntryTime(dtStart);
                        DateTime dtHours = DateTime.Parse(e.Row.Cells[9].Text);
                        bStatusWorking = ValidateWorkingHours(dtHours);
                    }
                    if (!String.Equals(e.Row.Cells[7].Text, "-", StringComparison.OrdinalIgnoreCase))
                    {
                        if (!bStatusStart)
                        {
                            e.Row.Cells[7].ForeColor = System.Drawing.Color.Red;
                        }
                        if (bStatusWorking)
                        {
                            if (!bStatusStart)
                            {
                                e.Row.Cells[9].ForeColor = System.Drawing.Color.Red;
                            }
                            else
                            {
                                e.Row.Cells[9].ForeColor = System.Drawing.Color.Green;
                            }

                        }
                        else
                        {
                            e.Row.Cells[9].ForeColor = System.Drawing.Color.Red;
                        }
                    }

                    // e.Row.Cells[7].Attributes["ondblclick"] = Page.ClientScript.GetPostBackClientHyperlink(myGridView, "Edit$" + e.Row.RowIndex);
                    //e.Row.Cells[7].Attributes["style"] = "cursor:pointer";
                }
                catch (Exception ex)
                {
                    // MessageBox.Show(ex.Message);

                }
            }

        }
        public void BindStaffInfo()
        {
            bool bStatus = false;
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[2] { new DataColumn("userid"), new DataColumn("username") });
                
                foreach (ListItem item in dpp_Staff.Items)
                {
                    string sUserid = item.Value;
                    string sUserName = item.Text;

                    dt.Rows.Add(sUserid, sUserName);
                   
                }
                GridViewStaff.PageSize = 10;
                GridViewStaff.AllowPaging = true;
                GridViewStaff.DataSource = dt;
                GridViewStaff.PageIndex = 0;
                GridViewStaff.DataBind();
                bStatus = true;

                /*SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();

                dtTableInfo = sqlCon.SelectPersonByManager(txt_userid.Text);
                if (dtTableInfo != null)
                {
                    if (dtTableInfo.Rows.Count > 0)
                    {
                        GridViewStaff.PageSize = 10;
                        GridViewStaff.AllowPaging = true;
                        GridViewStaff.DataSource = dtTableInfo;
                        GridViewStaff.PageIndex = 0;
                        GridViewStaff.DataBind();
                        bStatus = true;
                    }
                }*/
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
      
        private void BindGridStaffUnder()
        {
            bool bStatus = false;

            bool bFoundData = false;
            DateTime dtStart = DateTime.Now;
            DateTime dtEnd = DateTime.Now;
            string sStartDate = "";
            string sEndDate = "";
            string sCommand = "";

            try
            {
                sStartDate = txtDateStartStaff.Text;
                sEndDate = txtDateEndStaff.Text;
                dtStart = DateTime.ParseExact(sStartDate, "dd/MM/yyyy", null);
                dtEnd = DateTime.ParseExact(sEndDate, "dd/MM/yyyy", null);

                if ((dtStart != null) && (dtEnd != null))
                {
                    //lbl_errorlist.Text = "";
                    //lbl_errorlist.Visible = false;
                    DataTable dtTableInfo = null;
                    string sSelectStaffUnder = "";
                    //if (ck_all.Checked)
                    //{
                        sSelectStaffUnder = "All";
                    //}
                    // sSelectStaffUnder = ddlStaff.SelectedValue;
                    Logik process = new Logik();
                    sCommand = GenerateCommand(sSelectStaffUnder, dtStart, dtEnd);
                    dtTableInfo = process.SelectDataByCmd(sCommand);
                    //YPT 03.18.2020

                    //Select value
                    if (dtTableInfo != null)
                    {
                        DataTable dt = new DataTable();
                        //dt.Columns.AddRange(new DataColumn[8] { new DataColumn("datetime"), new DataColumn("userid"), new DataColumn("username"), new DataColumn("surname"), new DataColumn("starttime"), new DataColumn("endtime"), new DataColumn("workingh"), new DataColumn("comment") });
                        dt.Columns.AddRange(new DataColumn[12] { new DataColumn("datetime"), new DataColumn("userid"), new DataColumn("username"), new DataColumn("surname"), new DataColumn("starttime"), new DataColumn("endtime"), new DataColumn("workingh"), new DataColumn("starttimem"), new DataColumn("endtimem"), new DataColumn("workinghm"), new DataColumn("status_health"), new DataColumn("comment") });
                        foreach (DataRow row in dtTableInfo.Rows)
                        {
                            DateTime dtDatetime = (DateTime)row["datetime"];
                            string sDatetimeS = dtDatetime.ToString("dd-MM-yyyy");
                            string sUserid = row["userid"].ToString();
                            string sUsername = row["username"].ToString();
                            string sSurname = row["surname"].ToString();

                            TimeSpan tTime = (TimeSpan)row["starttime"];
                            string sStarttime = tTime.ToString("hh\\:mm");
                            if (String.Equals(sStarttime, "00:00", StringComparison.OrdinalIgnoreCase))
                            {
                                sStarttime = "-";
                            }
                            tTime = (TimeSpan)row["endtime"];
                            string sEndtime = tTime.ToString("hh\\:mm");
                            if (String.Equals(sEndtime, "00:00", StringComparison.OrdinalIgnoreCase))
                            {
                                sEndtime = "-";
                            }

                            tTime = (TimeSpan)row["workinghours"];
                            string sWorkingh = tTime.ToString("hh\\:mm");

                            //YPT 03.22.2020
                            string sStarttimem = "00:00";
                            tTime = (TimeSpan)row["starttimem"];
                            if (tTime != null)
                            {
                                sStarttimem = tTime.ToString("hh\\:mm");
                            }

                            if (String.Equals(sStarttimem, "00:00", StringComparison.OrdinalIgnoreCase))
                            {
                                sStarttimem = "-";
                            }

                            string sEndtimem = "00:00";
                            tTime = (TimeSpan)row["endtimem"];
                            if (tTime != null)
                            {
                                sEndtimem = tTime.ToString("hh\\:mm");
                            }

                            if (String.Equals(sEndtimem, "00:00", StringComparison.OrdinalIgnoreCase))
                            {
                                sEndtimem = "-";
                            }

                            string sWorkinghoursm = "00:00";
                            tTime = (TimeSpan)row["workinghoursm"];
                            if (tTime != null)
                            {
                                sWorkinghoursm = tTime.ToString("hh\\:mm");
                            }

                            string stemperature = "";
                            stemperature = row["status_health"].ToString();
                            if (String.IsNullOrEmpty(stemperature))
                            {
                                stemperature = "0";
                            }
                            stemperature = GetNameTemperature(stemperature);

                            string scomment = row["comment"].ToString();

                            if (String.Equals(scomment, "[double click]", StringComparison.OrdinalIgnoreCase))
                            {
                                scomment = "";
                            }
                            // dt.Rows.Add(sDatetimeS, sUserid, sUsername, sSurname, sStarttime, sEndtime, sWorkingh, scomment);
                            dt.Rows.Add(sDatetimeS, sUserid, sUsername, sSurname, sStarttime, sEndtime, sWorkingh, sStarttimem, sEndtimem, sWorkinghoursm, stemperature, scomment);
                            bFoundData = true;
                        }

                        GridViewStaffUnder.PageSize = 10; //limit items
                        GridViewStaffUnder.AllowPaging = true;
                        GridViewStaffUnder.DataSource = dt;
                        GridViewStaffUnder.PageIndex = 0;
                        GridViewStaffUnder.DataBind();

                        GridViewStaffUnder.Visible = true;
                        /*if (m_bFoundStaff)
                        {
                            lbl_staff.Visible = true;
                            ddlStaff.Visible = true;

                        }*/
                    }
                    if (!bFoundData)
                    {
                        lbl_errStaff.Text = "ไม่พบข้อมูล";
                        lbl_errStaff.Visible = true;
                        GridViewStaffUnder.Visible = false;

                    }
                    else
                    {
                        lbl_errStaff.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        public string GenerateCommand(string sMode, DateTime dtStartDate, DateTime dtEndDate)
        {
            string sReturnString = "";
            string sUserid = "";
            int nIndex = 0;
            bool bAddFirst = false;
            try {
                string query = "select * from (select * from timestampinfo order by userid) as t where (datetime between ";
                query += "'";
                query += dtStartDate.ToString("yyyy-MM-dd");
                query += "' ";
                query += "AND '";
                query += dtEndDate.ToString("yyyy-MM-dd");

                query += "') AND (";

                if (String.Equals(sMode, "All", StringComparison.OrdinalIgnoreCase))
                {
                    foreach (ListItem item in dpp_Staff.Items)
                    {
                        query += "userid='000";
                        query += item.Value;
                        query += "'";
                        if (nIndex != dpp_Staff.Items.Count - 1)
                        {
                            query += " OR ";
                        }
                        nIndex++;
                    }
                }
                else
                {
                    foreach (GridViewRow row in GridViewStaff.Rows)
                    {
                        if (nIndex < GridViewStaff.Rows.Count)
                        {

                            bool isChecked = row.Cells[0].Controls.OfType<CheckBox>().FirstOrDefault().Checked;

                            if (isChecked)
                            {
                                if (bAddFirst)
                                {
                                    query += " OR ";
                                }

                                query += "userid='000";
                                query += row.Cells[1].Text;
                                query += "'";
                                bAddFirst = true;

                            }

                        }
                        nIndex++;
                    }
                }

                query += ") ORDER BY DATE( t.datetime) ASC;";

                sReturnString = query;
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            
            return sReturnString;
        }

        protected void OnCheckedAllChanged(object sender, EventArgs e)
        {
            try {

                myGridView.EditIndex = -1;
                this.BindGrid(false);
                btnUpdate.Visible = false;
                btnCancle.Visible = false;

                CheckBox chk = (sender as CheckBox);
                if (chk.Checked)
                {
                    GridViewStaff.Visible = false;
                    BindGridStaffUnder();

                }
                else
                {
                    GridViewStaff.Visible = true;
                    GridViewStaffUnder.Visible = false;
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
             
        }
        protected void AddDepartmentToList(string sValue)
        {
            /* bool bFound = false;
             for (int i = 0; i < ddp_dep.Items.Count; i++)
             {
                 if (String.Equals(sValue, ddp_dep., StringComparison.OrdinalIgnoreCase))
                 {
                 }
             }
             if (!bFound)
             {
                 ddp_dep.Items.Add(sValue);
             }
             */
        }

        protected void ck_staff_CheckedChanged(object sender, EventArgs e)
        {
            ShowStaffControl(ck_staff.Checked);
            if (!ck_staff.Checked)
            {
                GridViewStaffUnder.Visible = ck_staff.Checked;
            }
        }
        protected void ShowStaffControl(bool bState)
        {
            //ck_all.Visible = bState;
            //if (bState)
            //{
               // ck_all.Checked = true;
           // }
            lbl_to.Visible = bState;

            txtDateStartStaff.Visible = bState;
            txtDateEndStaff.Visible = bState;
            btn_loadStaff.Visible = bState;
           
        }

        protected void ck_all_CheckedChanged(object sender, EventArgs e)
        {
            if (ck_all.Checked)
            {
                GridViewStaff.Visible = false;
            }
            else
            {
                GridViewStaff.Visible = true;
                BindStaffInfo();
            }
        }
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "InTime")
            {
                try{
                    string[] commandArguments = e.CommandArgument.ToString().Split(new char[] { ',' });

                    DateTime dtTime = DateTime.Now;
                    DateTime dtStartTime = DateTime.Now;
                    DateTime dtEndTime = DateTime.Now;
                    string sDate = "";
                    string sStartTime = "";
                    string sEndTime = "";
                    string sMode = "";
                    string argument1 = "";
                    string argument2 = "";

                    argument1 = commandArguments[0];
                    argument2 = commandArguments[1];

                    sMode = commandArguments[2];
                    DateTime dtDateTime = DateTime.ParseExact(argument1, "dd-MM-yyyy", null);
                    sDate = dtDateTime.ToString("yyyy-MM-dd");
                    if (String.Equals(argument2, "-", StringComparison.OrdinalIgnoreCase))
                    {
                        argument2 = "00:00";
                    }
                    DateTime dtDateTime2 = DateTime.ParseExact(argument2, "HH:mm", null);

                    if (String.Equals(sMode, "Start", StringComparison.OrdinalIgnoreCase))
                    {
                        dtStartTime = dtTime;
                        dtEndTime = dtDateTime2;
                    }
                    else
                    {
                        dtStartTime = dtDateTime2;
                        dtEndTime = dtTime;
                    }

                    sStartTime = dtStartTime.ToString("HH:mm");
                    sEndTime = dtEndTime.ToString("HH:mm");

                    UpdateTime(sStartTime, sEndTime, sDate);
                }
                catch (Exception ex)
                {
                    // MessageBox.Show(ex.Message);

                }
            }
        }
        protected void UpdateTime(string sStartTime, string sEndTime, string sDate)
        {
            try {

                Logik process = new Logik();
                bool bStatus = false;
                int nStatus = 0;
                DateTime dtWorkingHours = DateTime.Now;
                TimeSpan tWorkignHours = new TimeSpan(0, 0, 0);
                DateTime dtDeafultStartTime = DateTime.Now;
                DateTime dtDateTime = DateTime.Now;

                DateTime dtStartTime = DateTime.ParseExact(sStartTime, "HH:mm", null);
                DateTime dtEndTime = DateTime.ParseExact(sEndTime, "HH:mm", null);
                dtWorkingHours = DateTime.ParseExact("00:00", "HH:mm", null);
                dtDateTime = DateTime.ParseExact(sDate, "yyyy-MM-dd", null);

                if ((!String.Equals(sStartTime, "00:00", StringComparison.OrdinalIgnoreCase)) && (!String.Equals(sEndTime, "00:00:00", StringComparison.OrdinalIgnoreCase)))
                {
                    dtDeafultStartTime = process.CreateTimeFromDate(7, 0, 0, dtDateTime);
                    dtStartTime = process.CreateTimeFromDate(dtStartTime.Hour, dtStartTime.Minute, dtStartTime.Second, dtDateTime);
                    dtEndTime = process.CreateTimeFromDate(dtEndTime.Hour, dtEndTime.Minute, dtEndTime.Second, dtDateTime);

                    bStatus = process.CalculateWorkingHours(dtDeafultStartTime, dtStartTime, dtEndTime, ref tWorkignHours);
                    if (bStatus)
                    {
                        nStatus = 0;
                    }
                    else
                    {
                        nStatus = 1;
                    }
                    dtWorkingHours = process.CreateTimeFromDate(tWorkignHours.Hours, tWorkignHours.Minutes, tWorkignHours.Seconds, dtDateTime);
                   
                }

                string sCmd = "UPDATE timestampinfo SET ";
                sCmd += "starttimem='";
                sCmd += dtStartTime.ToString("HH:mm:ss");
                sCmd += "',";
                sCmd += "endtimem='";
                sCmd += dtEndTime.ToString("HH:mm:ss");
                sCmd += "',";
                sCmd += "workinghoursm='";
                sCmd += dtWorkingHours.ToString("HH:mm:ss");
                sCmd += "',";
                sCmd += "statusinfo='";
                sCmd += nStatus.ToString();
                sCmd += "'";
                sCmd += " WHERE userid = ";
                sCmd += "'";
                sCmd += "000";
                sCmd += txt_userid.Text;
                sCmd += "' and datetime=";
                sCmd += "'";
                sCmd += sDate;
                sCmd += "'";
                process.UpdateData(sCmd, "UPDATE");

                btnUpdate.Visible = false;
                btnCancle.Visible = false;
                this.BindGrid(false);
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            
        }
        public bool GetVisible(object value)
        {
            try {
                DateTime dtDateTime = DateTime.ParseExact(value.ToString(), "dd-MM-yyyy", null);
                DateTime dtDateTimeNow = DateTime.Now;
                Logik process = new Logik();
                dtDateTime = process.CreateTimeFromDate(0, 0, 0, dtDateTime);
                dtDateTimeNow = process.CreateTimeFromDate(0, 0, 0, dtDateTimeNow);
                if (DateTime.Compare(dtDateTime, dtDateTimeNow) == 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            
            return false;
        }

        public void EnableStaffMode(bool bStatus)
        {
            if (String.Equals(lbl_PS.Text, "Division Director", StringComparison.OrdinalIgnoreCase))
            {
                ck_staff.Visible = true;
            }
            else
            {
                ck_staff.Visible = false;
                //For test
                if (String.Equals(lbl_PS.Text, "Assistant Division Director", StringComparison.OrdinalIgnoreCase))
                {
                    ck_staff.Visible = true;

                }
                else
                {
                    ck_staff.Visible = false;

                }
            }
        }
        public bool ValidateSession()
        {
            bool bStatus = false;
            string sEmail = "";
            string sPassword = "";

            sPassword = Convert.ToString(Session["password"]);
            sEmail = Convert.ToString(Session["email"]);

            if ((string.IsNullOrEmpty(sPassword)) || (string.IsNullOrEmpty(sEmail)))
            {
                Session.Clear();
                Response.Redirect("LoginAttendance.aspx");
            }
            else
            {
                bStatus = true;
            }

            return bStatus;
        }
    }
}