﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="ReportViewer.aspx.cs" Inherits="WebTimeStamps.ReportViewer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link href="assets/css/Gridstyle.css" rel="stylesheet" type="text/css" />

    <link type="text/css" href="assets/css/ui-lightness/jquery-ui-1.8.19.custom.css" rel="stylesheet" />
    <link type="text/css" href="assets/css/main.css" rel="stylesheet" />
    <script type="text/javascript" src="Scripts/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.8.19.custom.min.js"></script>
    <script type="text/javascript">

        $(function () {
            $("#txtDateStart").datepicker();
            $("#txtDateEnd").datepicker();
            $("#txtDateStartStaff").datepicker();
            $("#txtDateEndStaff").datepicker();
            $("#txt_Date").datepicker();
        });
    </script>
    <style type="text/css">
         .auto-style5 {
             height: 26px;
             width: 400px;
         }
         .auto-style6 {
             width: 324px;
         }
         .auto-style11 {
             margin: auto;
             width: 511px;
             padding: 0px;
         }
         .auto-style12 {
             width: 511px;
             height: 26px;
             font-family: Font Awesome 5 Free;
             font-size: 16px;
         }
     </style>
     <script>
         if (window.history.replaceState) {
             window.history.replaceState(null, null, window.location.href);
         }
    </script>
</head>
<body>
    <form id="form1" runat="server" class="center">
        <div>
            <table class="auto-style3">
                 <tr>
                     <td align="right" class="auto-style6">
                         <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                     </td>
                     <td class="auto-style2" align="right">
                         <asp:LinkButton id="lk_logout" runat="server" OnClick="Onlogout">ออกจากระบบ</asp:LinkButton>
                     </td>
                 </tr>
                 <tr>
                     <td align="right" class="auto-style5" align="right">
                         <asp:Label ID="lbl_name" runat="server" Text="Label" Visible="False"></asp:Label>
                         <asp:Label ID="lbl_surname" runat="server" Text="Label" Visible="False"></asp:Label>
                     </td>
                     <td class="auto-style2" align="right">
                         
                     </td>
                 </tr>
                 <tr>
                     <td align="right" class="auto-style5" align="right">
                         <asp:Label ID="lbl_PS" runat="server" Visible="False"></asp:Label>
                     </td>
                     <td class="auto-style2" align="right">
                         <asp:Label ID="lb_username_surname" runat="server" Text="Label"></asp:Label>
                     </td>
                 </tr>
             </table>
            <br/>
             <table class="auto-style3">
                 <tr>
                     <td class="auto-style2" align="Left">
                         <asp:LinkButton id="LnK1" runat="server" OnClick="OnTimeAtt">Time attendance</asp:LinkButton>&nbsp;&nbsp;&nbsp;<asp:LinkButton id="LnK2" runat="server" OnClick="OnTimeSheet">TimeSheet</asp:LinkButton>
                    
                     </td>
                 </tr>
             </table>
             <br/>
            <div class="centerwithborder">
            <br/>
            <table align="center">
                <tr>
                       <td class="auto-style1" align="right">
                            <div>
                                <asp:label ID="lbl_userid" Text="รหัสพนักงาน : " AutoCompleteType="Disabled" runat="server" Visible="False"/>
                            </div>
                        </td>
                        <td class="auto-style1">
                            <asp:TextBox ID="txt_userid" runat="server" Visible="False"></asp:TextBox>
                        </td>
                    </tr>
                <tr>
                    <td class="auto-style1"  align="right">
                        <div>
                            <asp:label ID="lbl_DatStart" Text="ตั้งแต่วันที่ : " AutoCompleteType="Disabled" align="right" runat="server"/>
                        </div>
                    </td>
                    <td class="auto-style1">
                        <div>
                        <asp:TextBox ID="txtDateStart" AutoCompleteType="Disabled" runat="server"/>
                        </div>
                    </td>
                    <td class="auto-style1-small"  align="center">
                        <div>
                            <asp:label ID="Label1" Text="ถึง" AutoCompleteType="Disabled" align="right" runat="server"/>
                        </div>
                    </td>
                    <td class="auto-style1">
                        <div>
                            <asp:TextBox ID="txtDateEnd"  AutoCompleteType="Disabled" runat="server"/>
                        </div>
                    </td>
                </tr>
                    <tr>
                        <td class="auto-style1">
                        </td>
                        <td class="auto-style1">
                           
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style1">
                            
                        </td>
                        <td class="auto-style1">
                            <asp:Button class="buttonRounded" ID="btn_load" runat="server" OnClick="btnload_Click" Text="ค้นหา" />
                            <asp:Label ID="lbl_err" runat="server" Visible="false"></asp:Label>
                        </td>
                     </tr>
            </table>
             <br/>
        </div>
        <br/>
            <table align="center" class="center">
                <tr>
                    <td class="auto-style11">
                       <asp:CheckBox ID="Ck_Export" runat="server" CssClass="checkbox-centered" Text="Export data" OnCheckedChanged="Ck_Export_CheckedChanged" AutoPostBack="true" Visible="False"/> &nbsp;
                        <asp:ImageButton ID="imgExcel" runat="server" Height="24px" ImageUrl="~/img/ex.png" Width="28px" Visible="false" OnClick="imgExcel_Click" ImageAlign="Top" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                    </td>
                </tr>
                <tr>
                    <td class="center">
                        <asp:GridView ID="GridViewReport" runat="server" CssClass="Grid center" AlternatingRowStyle-CssClass="alt" OnPageIndexChanging="SubmitAppraisalGrid_PageIndexChanging">
                        </asp:GridView>
                     </td>
               </tr>
            </table>
           <br/>
        </div>
    </form>
</body>
</html>
