﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebTimeStamps
{
    public partial class ReportViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sStartDate = "";
            string sEndDate = "";
            string sUserId = "";
            string sUsernamesurenameth = "";

            sUserId = Convert.ToString(Session["userid"]);

            if (ValidateSession())
            {
                sUsernamesurenameth = Convert.ToString(Session["usernamesurenameth"]);
                lb_username_surname.Text = sUsernamesurenameth;

                if (!this.IsPostBack)
                {
                    Logik process = new Logik();
                    process.GetStartAndEndDate(ref sStartDate, ref sEndDate);
                    txtDateStart.Text = sStartDate;
                    txtDateEnd.Text = sEndDate;

                    txtDateStart.Text = sStartDate;
                    txtDateEnd.Text = sEndDate;
                    txt_userid.Text = sUserId;

                    DateTime dtCurrent = DateTime.Now;
                    txtDateStart.Text = dtCurrent.ToString("dd/MM/yyyy");
                    txtDateEnd.Text = dtCurrent.ToString("dd/MM/yyyy");

                    BindGrid();
                }
            }
        }
        protected void SubmitAppraisalGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindGrid();
                GridViewReport.PageIndex = e.NewPageIndex;
                GridViewReport.DataBind();
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        private void BindGrid()
        {
            string sStartDate = "";
            string sEndDate = "";
            string sUserId = "";
            try {

                DateTime dtStart = DateTime.ParseExact(txtDateStart.Text, "dd/MM/yyyy", null);
                DateTime dtEnd = DateTime.ParseExact(txtDateEnd.Text, "dd/MM/yyyy", null);
                sStartDate = dtStart.ToString("yyyy-MM-dd");
                sEndDate = dtEnd.ToString("yyyy-MM-dd");
                sUserId = txt_userid.Text;

                Logik process = new Logik();
                DataTable dtTable = process.SelectReport(sUserId, sStartDate, sEndDate);
                if (dtTable != null)
                {
                    if (dtTable.Rows.Count > 0)
                    {
                        GridViewReport.PageSize = 20; //limit items
                        GridViewReport.AllowPaging = true;
                        GridViewReport.PageIndex = 0;
                        GridViewReport.DataSource = dtTable;
                        GridViewReport.DataBind();
                        Ck_Export.Visible = true;
                    }
                    else
                    {
                        Ck_Export.Visible = false;
                    }
                    
                }
                else
                {
                    Ck_Export.Visible = false;
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            
        }
        public void OnTimeAtt(object sender, EventArgs e)
        {
            Response.Redirect("TimeAtt.aspx");
        }

        public void OnTimeSheet(object sender, EventArgs e)
        {
            Response.Redirect("TimeSheets.aspx");
        }
        public void Onlogout(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("LoginAttendance.aspx");
        }
        protected void imgExcel_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ExportGridToExcel();

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        private void ExportGridToExcel()
        {
            try
            {
                Response.Clear();
                Response.Buffer = true;
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Charset = "";

                string sDateStart = "";
                string sDateEnd = "";

                string sFileName = "";

                DateTime dtDateS = DateTime.ParseExact(txtDateStart.Text, "dd/MM/yyyy", null);
                DateTime dtDateE = DateTime.ParseExact(txtDateEnd.Text, "dd/MM/yyyy", null);
                sDateStart = dtDateS.ToString("yyyy-MM-dd");
                sDateEnd = dtDateE.ToString("yyyy-MM-dd");
                if (String.Equals(sDateStart, sDateEnd, StringComparison.OrdinalIgnoreCase))
                {
                    sFileName = "attachment;filename=TimeSheet_" + sDateStart + ".xlsx";
                }
                else
                {
                    sFileName = "attachment;filename=TimeSheet_" + sDateStart + "_" + sDateEnd + ".xlsx";
                }

                
                DataTable dt = new DataTable("Report");

                Logik process = new Logik();
                DataTable dtTable = process.SelectReport(txt_userid.Text, sDateStart, sDateEnd);
                if (dtTable != null)
                {
                    if (dtTable.Rows.Count > 0)
                    {
                        foreach (TableCell cell in GridViewReport.HeaderRow.Cells)
                        {
                            dt.Columns.Add(cell.Text);
                        }
                        foreach (DataRow row in dtTable.Rows)
                        {
                            dt.Rows.Add(row.ItemArray);
                            
                        }
                    }
                }
                
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dt);

                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", sFileName);
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        protected void btnload_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        protected void Ck_Export_CheckedChanged(object sender, EventArgs e)
        {
            if (Ck_Export.Checked)
            {
                imgExcel.Visible = true;
            }
            else
            {
                imgExcel.Visible = false;
            }

        }
        public bool ValidateSession()
        {
            bool bStatus = false;
            string sEmail = "";
            string sPassword = "";

            sPassword = Convert.ToString(Session["password"]);
            sEmail = Convert.ToString(Session["email"]);

            if ((string.IsNullOrEmpty(sPassword)) || (string.IsNullOrEmpty(sEmail)))
            {
                Session.Clear();
                Response.Redirect("LoginAttendance.aspx");
            }
            else
            {
                bStatus = true;
            }

            return bStatus;
        }
    }
}