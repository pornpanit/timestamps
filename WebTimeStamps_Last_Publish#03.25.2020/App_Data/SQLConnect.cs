﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySqlDatabase
{
    class SQLConnect
    {
        private MySqlConnection connection;
        private string server = "";
        private string database = "";
        private string uid = "";
        private string password = "";
        public string m_sSelectionCmd = "";

        public void Initialize()
        {
            string connectionString;

           /*server = "localhost";
            database = "etda_timeattendance";
            uid = "root";
            password = "1234";
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";*/


            // server = "10.3.2.201";
            server = "ctd.etda.or.th";
            database = "etda_timeattendance";
            uid = "etda_timeattenda";
            password = "S7$JME7Cda";
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";CharSet=utf8;";

            connection = new MySqlConnection(connectionString);
        }

        public bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        //MessageBox.Show("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        //MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }
        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                //MessageBox.Show(ex.Message);
                return false;
            }
        }
        public DataTable Select(string sDateStart, string sDateEnd, string sUserid)
        {
            bool bStatus = false;
            DataTable dtTableInfo = null;
            // string query = "INSERT INTO timestampinfo(id,userid,username,surname,starttime,endtime) VALUE ('2','0002','test','tsetsur','08:00:00','10:00:00')";
            try
            {
                //SELECT* FROM usertimestamp.timestampinfo WHERE datetime = '2020-02-03' and userid = '00000024';
                string query = "SELECT * FROM timestampinfo WHERE datetime BETWEEN";
                query += "'";
                query += sDateStart;
                query += "' ";
                query += "AND '";
                query += sDateEnd;
                query += "'";

                if (!String.Equals(sUserid, "000*", StringComparison.OrdinalIgnoreCase))
                {
                    query += " AND userid = '";
                    query += sUserid;
                    query += "'";
                }
                query += " ORDER BY DATE(datetime) ASC;";
                //open connection
                if (this.OpenConnection() == true)
                {
                    //create command and assign the query and connection from the constructor
                    // SqlCommand cmd = new SqlCommand(query);
                    MySqlCommand cmd = new MySqlCommand(query, connection);

                    using (MySqlDataAdapter da = new MySqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            da.Fill(dt);
                            dtTableInfo = dt;
                            // GridView1.DataSource = dt;
                            // GridView1.DataBind();
                        }
                    }
                    //close connection
                    this.CloseConnection();
                    bStatus = true;
                }
            }
            catch (MySqlException ex)
            {
                //MessageBox.Show(ex.Message);

            }

            return dtTableInfo;
        }
        public DataTable SelectPersonInfo(string sUserid)
        {
            bool bStatus = false;
            DataTable dtTableInfo = null;
            // string query = "INSERT INTO timestampinfo(id,userid,username,surname,starttime,endtime) VALUE ('2','0002','test','tsetsur','08:00:00','10:00:00')";
            try
            {
                //SELECT* FROM usertimestamp.timestampinfo WHERE datetime = '2020-02-03' and userid = '00000024';
                string query = "SELECT * FROM tempstaffinfo WHERE ";

                if (!String.Equals(sUserid, "000*", StringComparison.OrdinalIgnoreCase))
                {
                    query += "userid = '";
                    query += sUserid;
                    query += "'";
                }
                //open connection
                if (this.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(query, connection);

                    using (MySqlDataAdapter da = new MySqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            da.Fill(dt);
                            dtTableInfo = dt;
                        }
                    }
                    //close connection
                    this.CloseConnection();
                    bStatus = true;
                }
            }
            catch (MySqlException ex)
            {
                //MessageBox.Show(ex.Message);

            }

            return dtTableInfo;
        }
        public DataTable SelectPersonByManager(string sMGid)
        {
            bool bStatus = false;
            DataTable dtTableInfo = null;
            // string query = "INSERT INTO timestampinfo(id,userid,username,surname,starttime,endtime) VALUE ('2','0002','test','tsetsur','08:00:00','10:00:00')";
            try
            {
                //SELECT* FROM usertimestamp.timestampinfo WHERE datetime = '2020-02-03' and userid = '00000024';
                string query = "SELECT * FROM tempstaffinfo WHERE ";

                if (!String.Equals(sMGid, "000*", StringComparison.OrdinalIgnoreCase))
                {
                    query += "managerid = '";
                    query += sMGid;
                    query += "'";
                }
                //open connection
                if (this.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(query, connection);

                    using (MySqlDataAdapter da = new MySqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            da.Fill(dt);
                            dtTableInfo = dt;
                        }
                    }
                    //close connection
                    this.CloseConnection();
                    bStatus = true;
                }
            }
            catch (MySqlException ex)
            {
                //MessageBox.Show(ex.Message);

            }

            return dtTableInfo;
        }
        public bool InsertManual(int nId, string sDate, string sUserid, string sUsername, string sSurname, string sStarttime, string sEndtime, string sWorkingHours, int nStatus)
        {
            bool bStatus = false;
            // string query = "INSERT INTO timestampinfo(id,userid,username,surname,starttime,endtime) VALUE ('2','0002','test','tsetsur','08:00:00','10:00:00')";
            try
            {
                string query = "INSERT INTO timestampinfo(id,datetime,userid,username,surname,starttime,endtime,workinghours,statusinfo) VALUE (";
                query += "'";
                query += nId.ToString();
                query += "'";
                query += ",";
                query += "'";
                query += sDate;
                query += "'";
                query += ",";
                query += "'";
                query += sUserid;
                query += "'";
                query += ",";
                query += "'";
                query += sUsername;
                query += "'";
                query += ",";
                query += "'";
                query += sSurname;
                query += "'";
                query += ",";
                query += "'";
                query += sStarttime;
                query += "'";
                query += ",";
                query += "'";
                query += sEndtime;
                query += "'";
                query += ",";
                query += "'";
                query += sWorkingHours;
                query += "'";
                query += ",";
                query += "'";
                query += nStatus.ToString();
                query += "'";
                query += ")";
                //open connection
                if (this.OpenConnection() == true)
                {
                    //create command and assign the query and connection from the constructor
                    MySqlCommand cmd = new MySqlCommand(query, connection);

                    //Execute command
                    cmd.ExecuteNonQuery();
                    this.CloseConnection();
                }

                bStatus = true;
            }
            catch (MySqlException ex)
            {
                return false;
            }

            return bStatus;
        }
        public bool InsertTimesheet(int sTransId, string sEmpId, string sTaskId, string sWorkDate, string sWorkHour, string sTxtRemark, string sUpdUsreId)
        {
            bool bStatus = false;
            // string query = "INSERT INTO timestampinfo(id,userid,username,surname,starttime,endtime) VALUE ('2','0002','test','tsetsur','08:00:00','10:00:00')";
            try
            {
                string query = "insert into etda_t_timesheet (trans_id, userid, task_id, work_date, work_hour, txt_remark, upd_user_id, last_upd_date ) values (";
                query += "'";
                query += sTransId.ToString();
                query += "'";
                query += ",";
                query += "'";
                query += sEmpId;
                query += "'";
                query += ",";
                query += "'";
                query += sTaskId;
                query += "'";
                query += ",";
                query += "'";
                query += sWorkDate;
                query += "'";
                query += ",";
                query += "'";
                query += sWorkHour;
                query += "'";
                query += ",";
                query += "'";
                query += sTxtRemark;
                query += "'";
                query += ",";
                query += "'";
                query += sUpdUsreId;
                query += "'";
                query += ", now());";

                //open connection
                if (this.OpenConnection() == true)
                {
                    //create command and assign the query and connection from the constructor
                    MySqlCommand cmd = new MySqlCommand(query, connection);

                    //Execute command
                    cmd.ExecuteNonQuery();

                    //close connection
                    this.CloseConnection();
                    bStatus = true;
                }
            }
            catch (MySqlException ex)
            {
                return false;
            }

            return bStatus;
        }
        public int CountRow()
        {
            int nCount = 0;
            try
            {
                //string query = "SELECT COUNT(*) FROM etda_t_timesheet;";
                string query = "SELECT MAX(trans_id) FROM etda_t_timesheet;";

                //open connection
                if (this.OpenConnection() == true)
                {
                    //create command and assign the query and connection from the constructor
                    // SqlCommand cmd = new SqlCommand(query);
                    MySqlCommand cmd = new MySqlCommand(query, connection);

                    using (MySqlDataAdapter da = new MySqlDataAdapter())
                    {
                        if (!string.IsNullOrEmpty(cmd.ExecuteScalar().ToString()))
                        {
                            nCount = Convert.ToInt32(cmd.ExecuteScalar());
                        }

                    }
                    //close connection
                    this.CloseConnection();
                }
            }
            catch (MySqlException ex)
            {
                //MessageBox.Show(ex.Message);

            }

            return nCount;
        }
        public DataTable SelectWithCommand(string sCommand)
        {
            bool bStatus = false;
            DataTable dtTableInfo = null;
            // string query = "INSERT INTO timestampinfo(id,userid,username,surname,starttime,endtime) VALUE ('2','0002','test','tsetsur','08:00:00','10:00:00')";
            try
            {
                if (!string.IsNullOrEmpty(sCommand))
                {
                    //open connection
                    if (this.OpenConnection() == true)
                    {
                        MySqlCommand cmd = new MySqlCommand(sCommand, connection);

                        using (MySqlDataAdapter da = new MySqlDataAdapter())
                        {
                            da.SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                da.Fill(dt);
                                dtTableInfo = dt;
                            }
                        }
                        //close connection
                        this.CloseConnection();
                        bStatus = true;
                    }
                }

            }
            catch (MySqlException ex)
            {
                //MessageBox.Show(ex.Message);

            }

            return dtTableInfo;
        }
        public bool Delete(string nId)
        {
            bool bStatus = false;
            // string query = "INSERT INTO timestampinfo(id,userid,username,surname,starttime,endtime) VALUE ('2','0002','test','tsetsur','08:00:00','10:00:00')";
            try
            {
                string query = "delete from etda_t_timesheet where trans_id='";
                query += nId;
                query += "';";

                //open connection
                if (this.OpenConnection() == true)
                {
                    //create command and assign the query and connection from the constructor
                    MySqlCommand cmd = new MySqlCommand(query, connection);

                    //Execute command
                    cmd.ExecuteNonQuery();
                    this.CloseConnection();
                }

                bStatus = true;
            }
            catch (MySqlException ex)
            {
                return false;
            }

            return bStatus;
        }
        public bool Update(string sDate, string sUserid, string sStarttime, string sEndtime, string sWorkingHours, int nStatus)
        {
            bool bStatus = false;
            try
            {
                string query = "UPDATE timestampinfo set starttime=";
                query += "'";
                query += sStarttime;
                query += "', endtime=";
                query += "'";
                query += sEndtime;
                query += "', workinghours=";
                query += "'";
                query += sWorkingHours;
                query += "', statusinfo=";
                query += "'";
                query += nStatus.ToString();
                query += "'";

                query += " WHERE userid = ";
                query += "'";
                query += sUserid;
                query += "' and datetime=";
                query += "'";
                query += sDate;
                query += "'";

                //open connection
                if (this.OpenConnection() == true)
                {
                    //create command and assign the query and connection from the constructor
                    MySqlCommand cmd = new MySqlCommand(query, connection);

                    //Execute command
                    cmd.ExecuteNonQuery();

                    //close connection
                    this.CloseConnection();
                    bStatus = true;
                }
            }
            catch (MySqlException ex)
            {
                //MessageBox.Show(ex.Message);
                return false;
            }

            return bStatus;
        }
        public DataTable GetReportByUserID(string sUserId, string sStartDate, string sEndDate)
        {
            DataTable dt = null;

            bool bStatus = false;
            // string query = "INSERT INTO timestampinfo(id,userid,username,surname,starttime,endtime) VALUE ('2','0002','test','tsetsur','08:00:00','10:00:00')";
            try
            {
                //open connection
                if (this.OpenConnection() == true)
                {
                    using (MySqlCommand cmd = new MySqlCommand("spGetAbsenceEmp", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@EmpId", sUserId);
                        cmd.Parameters.AddWithValue("@StartDate", sStartDate);
                        cmd.Parameters.AddWithValue("@EndDate", sEndDate);
                        using (MySqlDataAdapter sda = new MySqlDataAdapter(cmd))
                        {
                            dt = new DataTable();
                            sda.Fill(dt);
                        }
                    }
                    this.CloseConnection();
                }

                bStatus = true;
            }
            catch (MySqlException ex)
            {

            }
            return dt;
        }
        public DataTable ExecuteQuery(string sCmd, string sAction)
        {
            DataTable dt = null;

            bool bStatus = false;
            // string query = "INSERT INTO timestampinfo(id,userid,username,surname,starttime,endtime) VALUE ('2','0002','test','tsetsur','08:00:00','10:00:00')";
            try
            {
                //open connection
                if (this.OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(sCmd, connection);

                    switch (sAction)
                    {
                        case "SELECT":
                            using (MySqlDataAdapter da = new MySqlDataAdapter())
                            {
                                da.SelectCommand = cmd;
                                using (dt = new DataTable())
                                {
                                    da.Fill(dt);
                                    return dt;
                                }
                            }

                        case "UPDATE":

                            cmd.ExecuteNonQuery();

                            break;
                    }

                    //close connection
                    this.CloseConnection();
                    bStatus = true;
                }

                bStatus = true;
            }
            catch (MySqlException ex)
            {

            }
            return dt;
        }
    }
}
