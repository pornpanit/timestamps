﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebTimeStamps
{
    public class PersonInfoWithUnder
    {
        public string name = "";
        public string id = "";
        public string fullNameTH = "";
        public string fullNameEN = "";
        public string mail = "";
        public string department = "";
        public string title = "";
        public string manager = "";
        public string samaccountname = "";
        public string distinguishedname = "";
        public string depNameTH = "";
        public string usernameTH = "";
        public string SurnameTH = "";

        public List<PersonInfoWithUnder> arrPersonInfo = null;

    }
}
