﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginAttendance.aspx.cs" Inherits="WebTimeStamps.LoginTimeSheet" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link type="text/css" href="assets/css/mainlogin.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->
    <h2 class="active"> attendance system </h2>
    <!-- Icon 
    <div class="fadeIn first">
      <img src="http://danielzawadzki.com/codepen/01/icon.svg" id="icon" alt="User Icon" />
    </div>-->

    <!-- Login Form -->
    <form>
       <asp:TextBox ID="txt_user" type="text" placeholder="email address" AutoCompleteType="Disabled" runat="server" />
       <asp:TextBox ID="txt_pwd" type="password" placeholder="password" AutoCompleteType="Disabled" runat="server" />
      <asp:Button class="button" ID="btn_login" runat="server" OnClick="submit_Click" Text="เข้าระบบ" />
    </form>

    <!-- Remind Passowrd 
    <div id="formFooter">
      <a class="underlineHover" href="#">Forgot Password?</a>
    </div>-->

  </div>
</div>
    </form>
</body>
</html>
