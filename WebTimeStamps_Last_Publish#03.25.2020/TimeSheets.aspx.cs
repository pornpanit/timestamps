﻿using ClosedXML.Excel;
using MySqlDatabase;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebTimeStamps
{
    public partial class TimeSheets : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                try
                {
                    // + Convert.ToString(Session["password"]);
                    bool bStatus = false;

                    string sUserName = "";
                    string sSamaccountname = "";
                    string sName = "";
                    string sEmail = "";
                    string sPassword = "";
                    string sTitle = "";

                    sUserName = Convert.ToString(Session["user"]);
                    sPassword = Convert.ToString(Session["password"]);
                    sSamaccountname = Convert.ToString(Session["samaccountname"]);
                    sName = Convert.ToString(Session["name"]);
                    sEmail = Convert.ToString(Session["email"]);
                    sTitle = Convert.ToString(Session["title"]);
                    if (ValidateSession())
                    {
                        if (String.Equals(lb_username_surname.Text, "Label", StringComparison.OrdinalIgnoreCase))
                        {
                            // lb_username_surname.Text = sUserName;

                            Logik process = new Logik();
                            bStatus = process.CallUserInfo(sName);
                            if (!bStatus)
                            {
                                txtDate.Enabled = false;
                                btn_load.Enabled = false;
                                lb_username_surname.Text = "";

                                string redirectScript = "window.location.href = 'LoginAttendance.aspx';";
                                System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "alert('ไม่พบข้อมูลในระบบ กรุณาติดต่อเจ้าหน้าที่ดูแลระบบ');" + redirectScript, true);
                            }
                            else
                            {
                                string sStartDate = "";
                                string sEndDate = "";

                                if (process.m_personInfo != null)
                                {
                                    lb_username_surname.Text = "สวัสดี : คุณ" + process.m_personInfo.fullNameTH;

                                    if (string.IsNullOrEmpty(process.m_personInfo.id))
                                    {
                                        txt_userid.Text = "*";
                                    }
                                    else
                                    {
                                        txt_userid.Text = process.m_personInfo.id;
                                    }
                                    lbl_PS.Text = sTitle;
                                }

                                DateTime dtCurrent = DateTime.Now;

                                //process.GetStartAndEndDate(ref sStartDate, ref sEndDate);
                                txtDate.Text = dtCurrent.ToString("dd/MM/yyyy");

                                //process.GetStartAndEndDate(ref sStartDate, ref sEndDate);
                                txtDateStart.Text = txtDate.Text;
                                txtDateEnd.Text = txtDate.Text;
                                GetProjectList();

                                //Get staff under
                                GetStaffUnder(ref process, sEmail, sPassword);

                                if (process.m_personInfo.arrPersonInfo != null)
                                {
                                    if (process.m_personInfo.arrPersonInfo.Count > 0)
                                    {
                                        EnableStaffMode(true);
                                    }
                                }
                            }
                            BindData(true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    // MessageBox.Show(ex.Message);

                }

            }
        }
        protected void GetStaffUnder(ref Logik process,string sEmail,string sPwd)
        {
            bool bStatus = false;
            string sInputjSon = "";
            sInputjSon = process.generateInput(sEmail, sPwd);
            bStatus = process.CallADServiceStaffUnder(sInputjSon);
            if (bStatus)
            {
                AddDataStaffUnderToTable(process.m_personInfo.arrPersonInfo, process.m_arrListStaff);
               
                dpp_Staff.Items.Clear();
                if (process.m_personInfo.arrPersonInfo != null)
                {
                    if (process.m_personInfo.arrPersonInfo.Count() > 0)
                    {
                        AddStaffToList(process.m_personInfo.arrPersonInfo);
                    }
                }
            }
        }
        protected void AddStaffToList(List<PersonInfoWithUnder> arrPersonInfos)
        {
            string sText = "";
            string sValue = "";
            foreach (PersonInfoWithUnder itemDetails in arrPersonInfos)
            {
                sValue = itemDetails.id;
                sText = itemDetails.fullNameTH;
                dpp_Staff.Items.Add(new ListItem(sText, sValue));
                if (itemDetails.arrPersonInfo != null)
                {
                    if (itemDetails.arrPersonInfo.Count() > 0)
                    {
                        AddStaffToList(itemDetails.arrPersonInfo);
                    }
                }
            }
        }
        protected void btnload_Click(object sender, EventArgs e)
        {
            try
            {
                //Insert data
                string sEmpId = "";
                string sTaskId = "";
                string sWorkDate = "";
                string sWorkHour = "";
                string sTxtRemark = "";
                string sUpdUsreId = "";

                sEmpId = txt_userid.Text;
                sTaskId = ddp_delisub.SelectedValue;

                DateTime dtDate = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null);

                sWorkDate = dtDate.ToString("yyyy-MM-dd");
                sWorkHour = ddp_hours.SelectedValue;
                sTxtRemark = txt_additional.Text;
                sUpdUsreId = txt_userid.Text;

                Logik process = new Logik();
                process.InsertData(sEmpId, sTaskId, sWorkDate, sWorkHour, sTxtRemark, sUpdUsreId);

                BindData(true);
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        public void Onlogout(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("LoginAttendance.aspx");
        }
        public void OnTimeAtt(object sender, EventArgs e)
        {
            Response.Redirect("TimeAtt.aspx");
        }
        public bool GetProjectList()
        {
            bool bStatus = false;
            string sCommand = "";
            DataTable dtResult;
            string sSelectionId = "";
            try
            {
                Logik process = new Logik();
                sCommand = "select distinct substring(task_id,1,3) task_id, project from etda_t_project order by 2;";
                dtResult = process.SelectDataByCmd(sCommand);
                if (dtResult != null)
                {
                    string sID = "";
                    string sValue = "";

                    foreach (DataRow row in dtResult.Rows)
                    {
                        sID = row["task_id"].ToString();
                        sValue = row["project"].ToString();
                        if ((!string.IsNullOrEmpty(sValue)) && (!string.IsNullOrEmpty(sID)))
                        {
                            ddp_mainproject.Items.Add(new ListItem(sValue, sID));
                        }
                    }
                }
                sSelectionId = ddp_mainproject.SelectedValue;
                GetMainAcivityList(sSelectionId);

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        public bool GetMainAcivityList(string sID)
        {
            bool bStatus = false;
            string sCommand = "";
            DataTable dtResult;
            string sSelectionId = "";
            try
            {
                Logik process = new Logik();
                if (string.IsNullOrEmpty(sID))
                {
                    sCommand = "select distinct substring(task_id,1,6) task_id, main_activity from etda_t_project;";
                }
                else
                {
                    sCommand = "select distinct substring(task_id,1,6) task_id, main_activity from etda_t_project where substring(task_id,1,3) = substring(";
                    sCommand += "'";
                    sCommand += sID;
                    sCommand += "'";
                    sCommand += ",1,3) order by 2;";
                }

                dtResult = process.SelectDataByCmd(sCommand);
                if (dtResult != null)
                {
                    string sIndex = "";
                    string sValue = "";
                    ddp_delimain.Items.Clear();

                    foreach (DataRow row in dtResult.Rows)
                    {
                        sIndex = row["task_id"].ToString();
                        sValue = row["main_activity"].ToString();
                        ddp_delimain.Items.Add(new ListItem(sValue, sIndex));
                    }
                }
                sSelectionId = ddp_delimain.SelectedValue;
                GetSubAcivityList(sSelectionId);
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        public bool GetSubAcivityList(string sID)
        {
            bool bStatus = false;
            string sCommand = "";
            DataTable dtResult;
            try
            {
                Logik process = new Logik();
                if (string.IsNullOrEmpty(sID))
                {
                    sCommand = "select distinct task_id, sub_activity from etda_t_project;";
                }
                else
                {
                    sCommand = "select distinct task_id, sub_activity from etda_t_project where substring(task_id,1,6) = substring(";
                    sCommand += "'";
                    sCommand += sID;
                    sCommand += "'";
                    sCommand += ",1,6) order by 2;";
                }


                dtResult = process.SelectDataByCmd(sCommand);
                if (dtResult != null)
                {
                    string sIndex = "";
                    string sValue = "";

                    ddp_delisub.Items.Clear();

                    foreach (DataRow row in dtResult.Rows)
                    {
                        sIndex = row["task_id"].ToString();
                        sValue = row["sub_activity"].ToString();
                        ddp_delisub.Items.Add(new ListItem(sValue, sIndex));
                    }

                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        protected void SubmitAppraisalmygridview_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindData(false);
                myGridView.PageIndex = e.NewPageIndex;
                myGridView.DataBind();

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        public void BindData(bool bSearch)
        {
            bool bStatus = false;
            DataTable dtTableInfo = null;
            string sCommand = "";
            string sDateStart = "";
            string sDateEnd = "";
            try
            {
                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();
                Logik process = new Logik();
                process.GetStartAndEndDateView(ref sDateStart, ref sDateEnd);

                sCommand = "select  trans_id,work_date ,project,main_activity, sub_activity, work_hour, txt_remark from etda_t_project a, etda_t_timesheet b where a.task_id = b.task_id and userid = '";
                sCommand += txt_userid.Text;
                sCommand += "' ";
                sCommand += "and (work_date BETWEEN ";
                sCommand += "'";
                sCommand += sDateStart;
                sCommand += "' ";
                sCommand += "AND '";
                sCommand += sDateEnd;
                sCommand += "')";

                dtTableInfo = sqlCon.SelectWithCommand(sCommand);
                if (dtTableInfo != null)
                {
                    if (dtTableInfo.Rows.Count > 0)
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.AddRange(new DataColumn[7] { new DataColumn("trans_id"), new DataColumn("work_date"), new DataColumn("project"), new DataColumn("main_activity"), new DataColumn("sub_activity"), new DataColumn("work_hour"), new DataColumn("txt_remark") });

                        foreach (DataRow row in dtTableInfo.Rows)
                        {
                            DateTime dtDatetime = (DateTime)row["work_date"];
                            string sDatetimeS = dtDatetime.ToString("dd-MM-yyyy");
                            string sProject = row["project"].ToString();
                            string sMainAc = row["main_activity"].ToString();
                            string sSubAc = row["sub_activity"].ToString();
                            string sWH = row["work_hour"].ToString();
                            string sRemark = row["txt_remark"].ToString();
                            string sTransID = row["trans_id"].ToString();

                            dt.Rows.Add(sTransID, sDatetimeS, sProject, sMainAc, sSubAc, sWH, sRemark);

                        }

                        myGridView.PageSize = 10;
                        myGridView.AllowPaging = true;
                        myGridView.DataSource = dt;
                        if (bSearch)
                        {
                            myGridView.PageIndex = 0;
                        }
                        myGridView.DataBind();
                        bStatus = true;
                        myGridView.Visible = true;
                        
                        
                    }
                    else
                    {
                        myGridView.Visible = false;
                    }
                }
                else
                {
                    myGridView.Visible = false;
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        public void BindDataStaff()
        {
            bool bStatus = false;
            DataTable dtTableInfo = null;
            string sCommand = "";
            try
            {
                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();

                string sDateStart = "";
                string sDateEnd = "";
                DateTime dtDateS = DateTime.ParseExact(txtDateStart.Text, "dd/MM/yyyy", null);
                DateTime dtDateE = DateTime.ParseExact(txtDateEnd.Text, "dd/MM/yyyy", null);

                sDateStart = dtDateS.ToString("yyyy-MM-dd");
                sDateEnd = dtDateE.ToString("yyyy-MM-dd");

                Logik process = new Logik();
                dtTableInfo = process.ExportDatafromDatabase(txt_userid.Text, sDateStart, sDateEnd, ck_Staff.Checked, dpp_Staff.Items);
                
                if (dtTableInfo != null)
                {
                    if (dtTableInfo.Rows.Count > 0)
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.AddRange(new DataColumn[7] { new DataColumn("work_date"),new DataColumn("username"), new DataColumn("project"), new DataColumn("main_activity"), new DataColumn("sub_activity"), new DataColumn("work_hour"), new DataColumn("txt_remark") });

                        foreach (DataRow row in dtTableInfo.Rows)
                        {
                            DateTime dtDatetime = (DateTime)row["work_date"];
                            string sDatetimeS = dtDatetime.ToString("dd-MM-yyyy");
                            string sProject = row["project"].ToString();
                            string sMainAc = row["main_activity"].ToString();
                            string sSubAc = row["sub_activity"].ToString();
                            string sWH = row["work_hour"].ToString();
                            string sRemark = row["txt_remark"].ToString();
                            string sUserID = row["userid"].ToString();
                            string sUserName = "";
                            sUserName = GetName(sUserID);

                            dt.Rows.Add(sDatetimeS, sUserName, sProject, sMainAc, sSubAc, sWH, sRemark);

                        }

                        GridViewStaff.PageSize = 10;
                        GridViewStaff.AllowPaging = true;
                        GridViewStaff.DataSource = dt;
                        GridViewStaff.PageIndex = 0;
                        GridViewStaff.DataBind();
                        bStatus = true;
                        GridViewStaff.Visible = true;
                        lbl_errexport.Visible = false;
                    }
                    else
                    {
                        GridViewStaff.Visible = false;
                        //lbl_errexport.Visible = true;
                        //lbl_errexport.Text = "ไม่พบข้อมูล";
                    }
                }
                else
                {
                    GridViewStaff.Visible = false;
                    //lbl_errexport.Visible = true;
                    //lbl_errexport.Text = "ไม่พบข้อมูล";
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        
        protected void showStaffControl(bool bState)
        {
            txtDateStart.Visible = bState;
            lbl_to.Visible = bState;
            txtDateEnd.Visible = bState;
            btn_loadStaff.Visible = bState;
            GridViewStaff.Visible = bState;
            Ck_Export.Visible = bState;
        }
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "dele")
            {
                Logik process = new Logik();
                process.DeleteData(e.CommandArgument.ToString());
                BindData(false);
            }
        }
        protected void ddpproject_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sSelectionId = "";
            sSelectionId = ddp_mainproject.SelectedValue;
            GetMainAcivityList(sSelectionId);
        }
        protected void ddpamin_SelectedIndexChanged(object sender, EventArgs e)
        {

            string sSelectionId = "";
            sSelectionId = ddp_delimain.SelectedValue;
            GetSubAcivityList(sSelectionId);
        }

        protected void imgExcel_Click(object sender, ImageClickEventArgs e)
        {
            bool bStatus = false;
            string sDateStart = "";
            string sDateEnd = "";
            try {

                DateTime dtDateS = DateTime.ParseExact(txtDateStart.Text, "dd/MM/yyyy", null);
                DateTime dtDateE = DateTime.ParseExact(txtDateEnd.Text, "dd/MM/yyyy", null);

                Logik logikProcess = new Logik();
                bStatus = logikProcess.ValidateDateTime(dtDateS, dtDateE);
                if (!bStatus)
                {
                    Response.Write("<script>alert('วันที่ไม่ถูกต้อง กรุณาใส่ข้อมูลใหม่');</script>");

                    //System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "alert('วันที่ไม่ถูกต้อง กรุณาใส่ข้อมูลใหม่');", true);
                }
                else
                {
                    sDateStart = dtDateS.ToString("yyyy-MM-dd");
                    sDateEnd = dtDateE.ToString("yyyy-MM-dd");
                    DataTable dtTable = null;
                    dtTable = logikProcess.ExportDatafromDatabase(txt_userid.Text, sDateStart, sDateEnd, ck_Staff.Checked, dpp_Staff.Items);
                    if (dtTable != null)
                    {
                        if (dtTable.Rows.Count > 0)
                        {
                            //lbl_errexport.Visible = false;
                            ExportExcel(dtTable);
                        }
                        else
                        {
                            //lbl_errexport.Visible = true;
                            //lbl_errexport.Text = "ไม่พบข้อมูล";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {

        }
        
        public void AddDataStaffUnderToTable(List<PersonInfoWithUnder> lsPerson, List<PersonInfoWithUnder> lsPersonStaff)
        {
            AddStaffUnder(lsPerson, lsPersonStaff);
        }
        public void AddStaffUnder(List<PersonInfoWithUnder> lsPerson, List<PersonInfoWithUnder> lsPersonStaff)
        {
            try{
                foreach (PersonInfoWithUnder item in lsPerson)
                {
                    AddDataList(item, lsPersonStaff);
                    if (item.arrPersonInfo != null)
                    {
                        if (item.arrPersonInfo.Count > 0)
                        {
                            AddStaffUnder(item.arrPersonInfo, lsPersonStaff);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
           
        }
        public void AddDataList(PersonInfoWithUnder item,List<PersonInfoWithUnder> lsPerson)
        {
            string sId = "";
            try {

                foreach (PersonInfoWithUnder itemDetails in lsPerson)
                {
                    if (String.Equals(item.name, itemDetails.fullNameEN, StringComparison.OrdinalIgnoreCase))
                    {
                        sId = itemDetails.id;
                        item.fullNameTH = itemDetails.fullNameTH;
                        item.id = sId;
                        item.depNameTH = itemDetails.depNameTH;

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            
        }

        protected void Ck_Export_CheckedChanged(object sender, EventArgs e)
        {
            if (Ck_Export.Checked)
            {
                imgExcel.Visible = true;
            }
            else
            {
                imgExcel.Visible = false;
            }
           
        }
        public void ExportExcel(DataTable dtValue)
        {
            try
            {
                DataTable dt = null;
                string sDate = "";
                string sUserid = "";
                string sProject = "";
                string sMain_activity = "";
                string sSub_activity = "";
                string sWork_hour = "";
                string sRemark = "";
                string sDateStart = "";
                string sDateEnd = "";
                string sColName = "";
                string sUseridTemp = "";
                string sUserName = "";

                DateTime dtDate = DateTime.Now;
                XLWorkbook wb = null;
                int nIndex = 0;

                using (wb = new XLWorkbook())
                {
                    foreach (DataRow row in dtValue.Rows)
                    {
                        sUserid = row["userid"].ToString();

                        sUserName = GetName(sUserid);

                        if (!String.Equals(sUseridTemp, sUserid, StringComparison.OrdinalIgnoreCase))
                        {
                            if (dt != null)
                            {
                                wb.Worksheets.Add(dt);
                            }

                            dt = new DataTable(sUserName);
                            sColName = "ลำดับ";
                            dt.Columns.Add(sColName);

                            for (int i = 0; i < dtValue.Columns.Count; i++)
                            {
                                sColName = dtValue.Columns[i].ColumnName.ToString();
                                if ((!String.Equals(sColName, "transid", StringComparison.OrdinalIgnoreCase)) && (!String.Equals(sColName, "&nbsp;", StringComparison.OrdinalIgnoreCase)))
                                {
                                    sColName = GetColName(sColName);
                                    dt.Columns.Add(sColName);
                                }
                            }
                            sUseridTemp = sUserid;
                        }
                        if (dt != null)
                        {
                            nIndex++;
                            dtDate = (DateTime)row["work_date"];
                            sDate = dtDate.ToString("yyyy-MM-dd");

                            sProject = row["project"].ToString();
                            sMain_activity = row["main_activity"].ToString();
                            sSub_activity = row["sub_activity"].ToString();
                            sWork_hour = row["work_hour"].ToString();
                            sRemark = row["txt_remark"].ToString();

                            dt.Rows.Add();
                            dt.Rows[dt.Rows.Count - 1][0] = nIndex.ToString();
                            dt.Rows[dt.Rows.Count - 1][1] = sDate;
                            dt.Rows[dt.Rows.Count - 1][2] = sUserName;
                            dt.Rows[dt.Rows.Count - 1][3] = sProject;
                            dt.Rows[dt.Rows.Count - 1][4] = sMain_activity;
                            dt.Rows[dt.Rows.Count - 1][5] = sSub_activity;
                            dt.Rows[dt.Rows.Count - 1][6] = sWork_hour;
                            dt.Rows[dt.Rows.Count - 1][7] = sRemark;
                        }
                    }
                    if (dt != null)
                    {
                        wb.Worksheets.Add(dt);
                    }
                }
                
                if (wb != null)
                {
                    DateTime dtDateS = DateTime.ParseExact(txtDateStart.Text, "dd/MM/yyyy", null);
                    DateTime dtDateE = DateTime.ParseExact(txtDateEnd.Text, "dd/MM/yyyy", null);
                    sDateStart = dtDateS.ToString("yyyy-MM-dd");
                    sDateEnd = dtDateE.ToString("yyyy-MM-dd");

                    string sFileName = "attachment;filename=TimeSheet_" + sDateStart + "_" + sDateEnd + ".xlsx";

                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    //Response.AddHeader("content-disposition", "attachment;filename=TimeSheet.xlsx");
                    Response.AddHeader("content-disposition", sFileName);

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                   
                }

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        protected string GetName(string sUserid)
        {
            string sReturn = "";
            foreach (ListItem item in dpp_Staff.Items)
            {
                if (String.Equals(item.Value, sUserid, StringComparison.OrdinalIgnoreCase))
                {
                    sReturn = item.Text;
                }

            }
            return sReturn;
        }
        protected string GetColName(string sValue)
        {
            string sReturn = "";
            if (String.Equals(sValue, "userid", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "ชื่อ - สกุล";
            }
            else if (String.Equals(sValue, "work_date", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "วันที่";
            }
            else if (String.Equals(sValue, "project", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "โครงการสำนัก";
            }
            else if (String.Equals(sValue, "main_activity", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "สิ่งส่งมอบหลัก";
            }
            else if (String.Equals(sValue, "sub_activity", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "สิ่งส่งมอบย่อย";
            }
            else if (String.Equals(sValue, "work_hour", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "จำนวนชม.";
            }
            else if (String.Equals(sValue, "txt_remark", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "คำอธิบาย";
            }
            else
            {
                sReturn = sValue;
            }
            return sReturn;
        }

        protected void btnloadStaff_Click(object sender, EventArgs e)
        {
            try
            {
                BindDataStaff();
                
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        protected void SubmitAppraisalGridStaff_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindDataStaff();
                GridViewStaff.PageIndex = e.NewPageIndex;
                GridViewStaff.DataBind();
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }

        protected void ck_Staff2_CheckedChanged(object sender, EventArgs e)
        {
            if (ck_Staff.Checked)
            {
                showStaffControl(true);
            }
            else
            {
                showStaffControl(false);
                GridViewStaff.Visible = false;
            }
        }
        public void EnableStaffMode(bool bStatus)
        {
            if (String.Equals(lbl_PS.Text, "Division Director", StringComparison.OrdinalIgnoreCase))
            {
                ck_Staff.Visible = true;
            }
            else
            {
                ck_Staff.Visible = false;
                //For test
                if (String.Equals(lbl_PS.Text, "Assistant Division Director", StringComparison.OrdinalIgnoreCase))
                {
                    ck_Staff.Visible = true;

                }
                else
                {
                    ck_Staff.Visible = false;

                }
            }
        }
        public bool ValidateSession()
        {
            bool bStatus = false;
            string sEmail = "";
            string sPassword = "";

            sPassword = Convert.ToString(Session["password"]);
            sEmail = Convert.ToString(Session["email"]);

            if ((string.IsNullOrEmpty(sPassword)) || (string.IsNullOrEmpty(sEmail)))
            {
                Session.Clear();
                Response.Redirect("LoginAttendance.aspx");
            }
            else
            {
                bStatus = true;
            }

            return bStatus;
        }
    }
}