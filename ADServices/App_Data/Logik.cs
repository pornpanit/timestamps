﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml;

namespace ADServices
{
    public class Logik
    {
        public DirectorySearcher dirSearch = null;
        public string m_sErrorMessage = "";
        public List<PersonInfoWithUnder> m_arrListStaff = null;

        public PersonInfoWithUnder ValidateUserInfo(string username, string passowrd, string domain, bool bUnder)
        {
            SearchResult rs = null;
            PersonInfoWithUnder userInfo = null;

            rs = SearchUserByEmail(GetDirectorySearcher(username, passowrd, domain), username);

            if (rs != null)
            {
                userInfo = GetuserInfo(rs);
                if (bUnder == true)
                {
                    if (userInfo != null)
                    {
                        SearchMemberByManager(GetDirectorySearcher(username, passowrd, domain), userInfo,true);
                    }
                }
            }

            return userInfo;
        }
        //YPT 11.26.2020
        public PersonInfoWithUnder ValidateUserInfoByGroup(string sGroup,string username, string passowrd, string domain, bool bUnder)
        {
            SearchResult rs = null;
            PersonInfoWithUnder userInfo = null;

            rs = SearchUserByEmailGroup(GetDirectorySearcher(username, passowrd, domain), username, sGroup);

            if (rs != null)
            {
                userInfo = GetuserInfo(rs);
            }

            return userInfo;
        }
        private SearchResult SearchUserByEmail(DirectorySearcher ds, string email)
        {
            SearchResult userObject = null;
            try
            {
                ds.Filter = "(&((&(objectCategory=Person)(objectClass=User)))(userprincipalname=" + email + "))";
                
                ds.SearchScope = SearchScope.Subtree;
                ds.ServerTimeLimit = TimeSpan.FromSeconds(90);

                userObject = ds.FindOne();

            }
            catch (Exception ex)
            {
                // Failed to authenticate. Most likely it is caused by unknown user
                // id or bad strPassword.
                m_sErrorMessage = ex.Message.ToString();
            }
            finally
            {

            }

            if (userObject != null)
                return userObject;
            else
                return null;

        }
        //YPT 11.16.2020
        private SearchResult SearchUserByEmailGroup(DirectorySearcher ds, string email,string sGroup)
        {
            SearchResult userObject = null;
            bool bFound = false;
            try
            {
                ds.Filter = "(&((&(objectCategory=Person)(objectClass=User)))(userprincipalname=" + email + "))";
               
                ds.SearchScope = SearchScope.Subtree;
                ds.ServerTimeLimit = TimeSpan.FromSeconds(90);

                userObject = ds.FindOne();

                foreach (string GroupPath in userObject.Properties["memberOf"])
                {
                    if (GroupPath.Contains(sGroup))
                    {
                        bFound = true;
                    }
                }

            }
            catch (Exception ex)
            {
                // Failed to authenticate. Most likely it is caused by unknown user
                // id or bad strPassword.
                m_sErrorMessage = ex.Message.ToString();
            }
            finally
            {

            }

            if ((userObject != null) && (bFound))
                return userObject;
            else
                return null;

        }
        private DirectorySearcher GetDirectorySearcher(string username, string passowrd, string domain)
        {
            if (dirSearch == null)
            {
                try
                {
                    dirSearch = new DirectorySearcher(
                        new DirectoryEntry("LDAP://" + domain, username, passowrd));
                }
                catch (DirectoryServicesCOMException e)
                {
                    //MessageBox.Show("Connection Creditial is Wrong!!!, please Check.", "Erro Info", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //e.Message.ToString();
                    m_sErrorMessage = e.Message.ToString();
                }
                return dirSearch;
            }
            else
            {
                return dirSearch;
            }
        }
       
        private PersonInfoWithUnder GetuserInfo(SearchResult rs)
        {
            string sName = "";
            string sSurename = "";
            string sFullName = "";
            PersonInfoWithUnder userInfo = new PersonInfoWithUnder(); ;

            if (rs != null)
            {
                try
                {
                    if (rs.GetDirectoryEntry().Properties["name"].Value != null)
                        sFullName = rs.GetDirectoryEntry().Properties["name"].Value.ToString();
                    if (rs.GetDirectoryEntry().Properties["givenname"].Value != null)
                        sName = rs.GetDirectoryEntry().Properties["givenname"].Value.ToString();
                    if (rs.GetDirectoryEntry().Properties["sn"].Value != null)
                        sSurename = rs.GetDirectoryEntry().Properties["sn"].Value.ToString();
                    if (rs.GetDirectoryEntry().Properties["department"].Value != null)
                        userInfo.department = rs.GetDirectoryEntry().Properties["department"].Value.ToString();
                    if (rs.GetDirectoryEntry().Properties["title"].Value != null)
                        userInfo.title = rs.GetDirectoryEntry().Properties["title"].Value.ToString();
                    if (rs.GetDirectoryEntry().Properties["manager"].Value != null)
                        userInfo.manager = rs.GetDirectoryEntry().Properties["manager"].Value.ToString();
                    if (rs.GetDirectoryEntry().Properties["mail"].Value != null)
                        userInfo.mail = rs.GetDirectoryEntry().Properties["mail"].Value.ToString();
                    if (rs.GetDirectoryEntry().Properties["samaccountname"].Value != null)
                        userInfo.samaccountname = rs.GetDirectoryEntry().Properties["samaccountname"].Value.ToString();
                    if (rs.GetDirectoryEntry().Properties["distinguishedname"].Value != null)
                        userInfo.distinguishedname = rs.GetDirectoryEntry().Properties["distinguishedname"].Value.ToString();

                    if ((!string.IsNullOrEmpty(sName)) && (!string.IsNullOrEmpty(sSurename)))
                    {
                        userInfo.name = sName + " " + sSurename;
                    }
                    else
                    {
                        userInfo.name = sFullName;
                    }

                }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
                catch (DirectoryServicesCOMException e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
                {
                    //m_sErrorMessage = e.Message.ToString();
                }
            }
            return userInfo;
        }
        private void SearchMemberByManager(DirectorySearcher ds, PersonInfoWithUnder personInfoParent,bool bSub)
        {
            try
            {
                //Have to change it to correct data
                //ds.Filter = "(&(objectClass=user)(manager=CN=Chitnarong Baibang,OU=OIT,OU=Main,DC=etda,DC=or,DC=th))";
                if (!bSub)
                {
                    ds.Filter = "(&(objectClass=user)(manager=CN=Chitnarong Baibang,OU=OIT,OU=Main,DC=etda,DC=or,DC=th))";
                }
                else
                {
                    ds.Filter = "(&(objectClass=user)(manager=" + personInfoParent.distinguishedname + "))";
                }
               
                ds.PropertiesToLoad.Add("department");
                ds.PropertiesToLoad.Add("title");
                ds.PropertiesToLoad.Add("name");
                ds.PropertiesToLoad.Add("manager");
                ds.PropertiesToLoad.Add("mail");
                ds.PropertiesToLoad.Add("distinguishedname");
                ds.PropertiesToLoad.Add("givenname");
                ds.PropertiesToLoad.Add("sn");

                SearchResult result;
                SearchResultCollection resultCol = ds.FindAll();
                if (resultCol != null)
                {
                    PersonInfoWithUnder personInfo = null;
                    for (int counter = 0; counter < resultCol.Count; counter++)
                    {
                        result = resultCol[counter];

                        if (result != null)
                        {
                            personInfo = GetuserInfo(result);
                            if (personInfo != null)
                            {
                                if (personInfoParent.arrPersonInfo == null)
                                {
                                    personInfoParent.arrPersonInfo = new List<PersonInfoWithUnder>();
                                }
                                if (personInfoParent.arrPersonInfo != null)
                                {
                                    SearchMemberByManager(ds, personInfo,true);
                                    personInfoParent.arrPersonInfo.Add(personInfo);
                                }
                            }
                        }
                    }

                }
            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch (DirectoryServicesCOMException e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {

            }

        }
    }
}