﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Script.Serialization;
using DirectoryEntry = System.DirectoryServices.DirectoryEntry;

namespace ADServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IADServices
    {
        public string ValidateAccess(string username,string password)
        {
            string sReturnstring = "";
            PersonInfoWithUnder res = null;
            string sDomain = "etda.or.th";
            Logik process = new Logik();
            res = process.ValidateUserInfo(username, password, sDomain,false);
            if (res != null)
            {
                JsonSerializerSettings settings = new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore
                };
                sReturnstring = JsonConvert.SerializeObject(res, settings);
            }

            return sReturnstring;
        }
        //YPT 11.16.2020
        public string ValidateAccessByGroup(string group, string username, string password)
        {
            string sReturnstring = "";
            PersonInfoWithUnder res = null;
            string sDomain = "etda.or.th";
            Logik process = new Logik();
            res = process.ValidateUserInfoByGroup(group, username, password, sDomain, false);
            if (res != null)
            {
                JsonSerializerSettings settings = new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore
                };
                sReturnstring = JsonConvert.SerializeObject(res, settings);
            }

            return sReturnstring;
        }
        public string GetStaffUnder(string username, string password)
        {
            string sReturnstring = "";
            PersonInfoWithUnder res = null;
            string sDomain = "etda.or.th";
            Logik process = new Logik();
            res = process.ValidateUserInfo(username, password, sDomain, true);
            if (res != null)
            {
                JsonSerializerSettings settings = new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore
                };
                sReturnstring = JsonConvert.SerializeObject(res, settings);
            }

            return sReturnstring;
        }
    }
}
