﻿using MySql.Data.MySqlClient;
using System;
using System.IO;
using System.Windows.Forms;

namespace ManageTimeStamp
{
    public partial class Form1 : Form
    {

        MySqlConnection connection;
        string server = "";
        string database = "";
        string uid = "";
        string password = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool bStatus = false;

            if ((string.IsNullOrEmpty(txt_range.Text)) || (string.IsNullOrEmpty(txt_csv.Text)))
            {
                MessageBox.Show("Please insert the input data!");
            }
            else {
                if (File.Exists(txt_csv.Text))
                {
                    button1.Text = "Processing...";
                    bStatus = ValidateUser(txt_range.Text);
                    if (bStatus)
                    {
                        MessageBox.Show("Finished!");
                    }
                    button1.Text = "Load";
                }
                else {
                    MessageBox.Show("Cannot found the csv file, Please check it!");
                }
            }

        }

        
        private bool ValidateUser(string sRangeMonth)
        {
            bool bStatus = false;
            try
            {
                Logik process = new Logik();
               
                bStatus = process.ReadcsvFile(txt_csv.Text);
                if (bStatus)
                {
                    bStatus = process.InsertUser(process.m_userDataList, sRangeMonth);

                }
            }
            catch (MySqlException ex)
            {

            }
            return bStatus;
        }

        private void btn_browse_Click(object sender, EventArgs e)
        {
            /* FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
             if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
             {
                 txt_csv.Text = folderBrowserDialog1.SelectedPath;
             }*/
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            //openFileDialog1.InitialDirectory = @"C:\";
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.Title = "Browse csv Files";
            openFileDialog1.DefaultExt = "csv";
            openFileDialog1.Filter = "csv files (*.csv)|*.csv";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txt_csv.Text = openFileDialog1.FileName;
            }


        }
    }
}
