﻿using IniComponent;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using TraceComponent;

namespace ManageTimeStamp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try {
                bool m_bTesting = false;
                TraceManager.InitTrace("ManageTimeStamp", ref m_bTesting);
                
                string sInputParameter = "";
                string sSecondParameter = "";
                string sThirdParameter = "";

                Logik callLogik = new Logik();

                if ((IniManager.GetModuleIni(ref callLogik.m_sIniFile)) && (args.Length > 0))
                {
                    sInputParameter = args[0];
                    sInputParameter = sInputParameter.Trim('"');

                    TraceManager.ProcessTrace(String.Format("Input parameter [{0}].", sInputParameter));

                    //YPT 03.17.2021
                    if (String.Equals(sInputParameter, "InsertNewUser", StringComparison.OrdinalIgnoreCase))
                    {
                        Application.EnableVisualStyles();
                        Application.SetCompatibleTextRenderingDefault(false);
                        Application.Run(new Form1());
                    }
                    else {

                        if (args.Length > 1)
                        {
                            sSecondParameter = args[1];
                            TraceManager.ProcessTrace(String.Format("Second parameter [{0}].", sSecondParameter));
                        }
                        if (args.Length > 2)
                        {
                            sThirdParameter = args[2];
                            TraceManager.ProcessTrace(String.Format("Thirs parameter [{0}].", sThirdParameter));
                        }

                        if (!string.IsNullOrEmpty(sThirdParameter))
                        {
                            if (String.Equals(sThirdParameter, "2", StringComparison.OrdinalIgnoreCase))
                            {
                                string sFileName = "";
                                DirectoryInfo di = new DirectoryInfo(sInputParameter);
                                FileInfo[] files = di.GetFiles("*.log");

                                foreach (FileInfo file in files)
                                {
                                    sFileName = file.FullName;
                                    callLogik.CallTimeStampProcess(sFileName);
                                    callLogik.InsertPersonData(sSecondParameter);
                                }
                            }
                            else
                            {
                                callLogik.CallAllUserInfo(sThirdParameter);
                            }
                        }
                        else if (!string.IsNullOrEmpty(sInputParameter))
                        {
                            DateDataInfo dtInfo = null;
                            //Validate Date
                            dtInfo = callLogik.GenerateDateFile(sInputParameter);
                            if (dtInfo != null)
                            {
                                if (!string.IsNullOrEmpty(dtInfo.m_sMNFile))
                                {
                                    callLogik.CallTimeStampProcess(dtInfo.m_sMNFile);
                                    callLogik.InsertPersonData(sSecondParameter);
                                }

                                if (!string.IsNullOrEmpty(dtInfo.m_sCRFile))
                                {
                                    callLogik.CallTimeStampProcess(dtInfo.m_sCRFile);
                                    callLogik.InsertPersonData(sSecondParameter);
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception e)
            {
               
            }
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
        }
    }
}
