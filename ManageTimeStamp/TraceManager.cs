﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Collections;
using IniComponent;

namespace TraceComponent
{
    public static class TraceManager
    {
        private static StreamWriter m_traceWriter = null;

        private static String m_sApplicationName = "";
        private static String m_sApplicationDir = "";
        private static String m_sAssemblyName = "";

        private static String m_sDefaultTraceFile = "";
        private static String m_sTraceFile = "";
        private static String m_sTracePath = "";
        private static int m_nTraceMode = 0;

        private static String m_sIndent = "";
        private static Stack<DateTime> m_startTimeList = null;
        private static Stack<String> m_moduleList = null;

        public static void SetDefaultTraceFile(String sDefaultTraceFile)
        {
            if (sDefaultTraceFile != null)
                m_sDefaultTraceFile = sDefaultTraceFile;
        }
        public static String GetDefaultTraceFile()
        {
            return m_sDefaultTraceFile;
        }
        public static String GetCurrenttraceFile()
        {
            return m_sTraceFile;
        }
        public static int GetTraceMode()
        {
            return m_nTraceMode;
        }
        public static String GetTracePath()
        {
            return m_sTracePath;
        }

        public static void InitTrace(String sModuleName, ref bool bTesting)
        {
            if (sModuleName == null)
            {
                sModuleName = "";
            }
            if(m_startTimeList == null)
            {
                m_startTimeList = new Stack<DateTime>();
            }
            if (m_moduleList == null)
            {
                m_moduleList = new Stack<String>();
            }

            if(m_startTimeList.Count == 0)
            {
                //Get application name and path
                m_sApplicationName = Assembly.GetExecutingAssembly().Location;
                m_sApplicationDir = m_sApplicationName.Substring(0, m_sApplicationName.LastIndexOf("\\") + 1);
                m_sApplicationName = m_sApplicationName.Replace(m_sApplicationDir, "");
                m_sAssemblyName = m_sApplicationName;
                m_sApplicationName = m_sApplicationName.Remove(m_sApplicationName.LastIndexOf("."));

                String sIniFile = "";
                if (File.Exists(sIniFile = m_sApplicationDir + m_sApplicationName + ".ini"))
                {
                    IniManager.SetIniFile(sIniFile);

                    String sTraceOutput = IniManager.Read("Trace", "TraceOutput");
                    if (sTraceOutput.CompareTo("") != 0)
                    {
                        m_nTraceMode = Convert.ToInt32(sTraceOutput);
                    }
                    else
                    {
                        IniManager.Write("Trace", "TraceOutput", "0");
                        m_nTraceMode = 0;
                    }
                    if ((m_nTraceMode == 2) || (m_nTraceMode == 4))
                    {
                        bTesting = true;
                    }

                    m_sTracePath = IniManager.Read("TRACE", "TracePath");
                    if (m_sTracePath.CompareTo("") == 0)
                    {
                        m_sTracePath = "C:\\Temp\\";
                        IniManager.Write("Trace", "TracePath", m_sTracePath);
                    }
                    else if (m_sTracePath.LastIndexOf("\\") != (m_sTracePath.Length - 1))
                    {
                        m_sTracePath += "\\";
                    }

                    m_sTraceFile = IniManager.Read("Trace", "TraceFile");
                    if (m_sTraceFile.CompareTo("") == 0)
                    {
                        if (m_sDefaultTraceFile.CompareTo("") != 0)
                        {
                            m_sTraceFile = "C:\\Temp\\" + m_sDefaultTraceFile;
                        }
                        else
                        {
                            m_sTraceFile = "C:\\Temp\\TraceFile.log";
                        }
                        //YPT 10.10.2013. Fixed write data base to ini file.
                       // IniManager.Write("Trace", "TraceFile", m_sTraceFile);
                    }
                    else
                    {
                        //TraceFile = C:\temp
                        int nIndex = -1;
                        if ((nIndex = m_sTraceFile.LastIndexOf(".")) <= 0)
                        {
                            if (m_sTraceFile.LastIndexOf("\\") != m_sTraceFile.Length - 1)
                            {
                                m_sTraceFile += "\\";
                            }
                            m_sTraceFile += m_sDefaultTraceFile;
                            //YPT 10.10.2013. Fixed write data base to ini file.
                            //IniManager.Write("Trace", "TraceFile", m_sTraceFile);
                        }
                        //TraceFile = TraceFile_PDFMerger.log
                        else if (!m_sTraceFile.Contains("\\"))
                        {
                            m_sTraceFile = m_sApplicationDir + m_sTraceFile;
                        }
                        //TraceFile = \TraceFile_PDFMerger.log
                        else if (m_sTraceFile.StartsWith("\\"))
                        {
                            m_sTraceFile = m_sApplicationDir + m_sTraceFile;
                        }
                    }

                    String sTraceFileDir = m_sTraceFile.Substring(0, m_sTraceFile.LastIndexOf("\\") + 1);
                    if (m_nTraceMode == 3)
                    {
                        if (!File.Exists(m_sTraceFile))
                        {
                            if (!Directory.Exists(sTraceFileDir))
                            {
                                try
                                {
                                    Directory.CreateDirectory(sTraceFileDir);
                                }
                                catch(Exception e)
                                {
                                    ErrorTrace("Can't create directory.");
                                    ErrorTrace(e.Message);
                                }

                                try
                                {
                                    m_traceWriter = File.CreateText(m_sTraceFile);
                                }
                                catch(Exception e)
                                {
                                    ErrorTrace("Can't access file.");
                                    ErrorTrace(e.Message);
                                    m_traceWriter = null;
                                }
                            }
                            else
                            {
                                try
                                {
                                    m_traceWriter = File.CreateText(m_sTraceFile);
                                }
                                catch (Exception e)
                                {
                                    ErrorTrace("Can't access file.");
                                    ErrorTrace(e.Message);
                                    m_traceWriter = null;
                                }
                            }
                        }
                        else
                        {
                            try
                            {
                                m_traceWriter = File.CreateText(m_sTraceFile);
                            }
                            catch (Exception e)
                            {
                                ErrorTrace("Can't access file.");
                                ErrorTrace(e.Message);
                                m_traceWriter = null;
                            }
                        }
                    }
                }
            }
            m_startTimeList.Push(DateTime.Now);
            m_moduleList.Push(sModuleName);
            ProcessTrace(String.Format("[{0}>>>>{1}] trace starts on : [{2}]", m_sAssemblyName, sModuleName, getOSInfo()));
            ProcessTrace(String.Format("Product version : [{0}]", FileVersionInfo.GetVersionInfo(m_sApplicationDir + m_sAssemblyName).ProductVersion));
            ProcessTrace(String.Format("File version : [{0}]", FileVersionInfo.GetVersionInfo(m_sApplicationDir + m_sAssemblyName).FileVersion));
        }

        public static void EndTrace()
        {
            if (m_startTimeList.Count > 0)
            {
                ProcessTrace("Trace ends.");
                TimeSpan diffTime = DateTime.Now - m_startTimeList.Pop();
                ProcessTrace(String.Format("Trace time: {0}.{1} sec.", diffTime.Seconds.ToString(), diffTime.Milliseconds.ToString()));

                if (m_startTimeList.Count == 0)
                {
                    if (m_traceWriter != null)
                    {
                        m_traceWriter.Close();
                        m_traceWriter = null;
                    }
                    m_sIndent = "";
                }
            }
            else
            {
                ProcessTrace("[Error] EndTrace doesn't match InitTrace.");
            }
        }

        public static void FunctionStartTrace(String sFunctionName)
        {
            ProcessTrace(String.Format("Enter the function {0}()!", sFunctionName));
            ProcessTrace("{");
            m_sIndent += "    ";
        }

        public static void FunctionEndTrace(String sFunctionName, IConvertible Result)
        {
            if (m_sIndent.Length == 1)
            {
                m_sIndent = "";
            }
            else if (m_sIndent.Length > 1)
            {
                m_sIndent = m_sIndent.Substring(4);
            }

            ProcessTrace("}");
            ProcessTrace(String.Format("Result from {0}() is ({1})!", sFunctionName, Result.ToString()));
        }

        public static void ProcessTrace(String sProcessTrace)
        {
            if (m_startTimeList == null)
            {
                return;
            }
            else if (m_nTraceMode == 0)
            {
                return;
            }

            sProcessTrace = (sProcessTrace == null) ? "" : sProcessTrace;

            if (sProcessTrace.Contains("\n"))
            {
                String[] sTokenList = sProcessTrace.Split('\n');
                foreach (String sToken in sTokenList)
                {
                    if(m_nTraceMode != 3)
                        Trace.WriteLine(m_sIndent + sToken);
                    if (m_traceWriter != null)
                        m_traceWriter.WriteLine(m_sIndent + sToken);
                }
            }
            else
            {
                if (m_nTraceMode != 3)
                    Trace.WriteLine(m_sIndent + sProcessTrace);
                if (m_traceWriter != null)
                    m_traceWriter.WriteLine(m_sIndent + sProcessTrace);
            }

            if(m_traceWriter != null)
                m_traceWriter.Flush();
        }

        public static void ErrorTrace(String sProcessTrace)
        {
            if (m_startTimeList == null)
            {
                return;
            }

            sProcessTrace = (sProcessTrace == null) ? "" : sProcessTrace;

            if (sProcessTrace.Contains("\n"))
            {
                Trace.WriteLine("[ERROR] ");
                if (m_traceWriter != null)
                {
                    m_traceWriter.WriteLine("[ERROR] ");
                }
                String[] sTokenList = sProcessTrace.Split('\n');
                foreach (String sToken in sTokenList)
                {
                    Trace.WriteLine(m_sIndent + sToken);
                    if (m_traceWriter != null)
                        m_traceWriter.WriteLine(m_sIndent + sToken);
                }
            }
            else
            {
                Trace.WriteLine("[ERROR] " + sProcessTrace);
                if (m_traceWriter != null)
                    m_traceWriter.WriteLine(m_sIndent + "[ERROR] " + sProcessTrace);
            }

            if (m_traceWriter != null)
                m_traceWriter.Flush();
        }

        private static int GetOSArchitecture()
        {
            String pa = Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE");
            //YPT 06.27.2013. Fixed problem on RKCD VMWare.
            if (pa == null)
            {
                pa = "";
            }
            return ((String.IsNullOrEmpty(pa) || String.Compare(pa, 0, "x86", 0, 3, true) == 0) ? 32 : 64);
        }

        private static String getOSInfo()
        {
            //Get Operating system information.
            OperatingSystem os = Environment.OSVersion;
            //Get version information about the os.
            Version vs = os.Version;

            //Variable to hold our return value
            String operatingSystem = "";

            if (os.Platform == PlatformID.Win32Windows)
            {
                //This is a pre-NT version of Windows
                switch (vs.Minor)
                {
                    case 0:
                        operatingSystem = "95";
                        break;
                    case 10:
                        if (vs.Revision.ToString() == "2222A")
                            operatingSystem = "98SE";
                        else
                            operatingSystem = "98";
                        break;
                    case 90:
                        operatingSystem = "Me";
                        break;
                    default:
                        break;
                }
            }
            else if (os.Platform == PlatformID.Win32NT)
            {
                switch (vs.Major)
                {
                    case 3:
                        operatingSystem = "NT 3.51";
                        break;
                    case 4:
                        operatingSystem = "NT 4.0";
                        break;
                    case 5:
                        if (vs.Minor == 0)
                            operatingSystem = "2000";
                        else
                            operatingSystem = "XP";
                        break;
                    case 6:
                        if (vs.Minor == 0)
                            operatingSystem = "Vista";
                        else
                            operatingSystem = "7";
                        break;
                    default:
                        break;
                }
            }
            //Make sure we actually got something in our OS check
            //We don't want to just return " Service Pack 2" or " 32-bit"
            //That information is useless without the OS version.
            if (operatingSystem != "")
            {
                //Got something.  Let's prepend "Windows" and get more info.
                operatingSystem = "Windows " + operatingSystem;
                //See if there's a service pack installed.
                if (os.ServicePack != "")
                {
                    //Append it to the OS name.  i.e. "Windows XP Service Pack 3"
                    operatingSystem += " " + os.ServicePack;
                }
                //Append the OS architecture.  i.e. "Windows XP Service Pack 3 32-bit"
                operatingSystem += " " + GetOSArchitecture().ToString() + "-bit";
            }
            //Return the information we've gathered.
            return operatingSystem;
        }
    }
}
