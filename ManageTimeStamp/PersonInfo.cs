﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManageTimeStamp
{
    public class PersonInfoWithUnder
    {
        public string id = "";
        public string nameTH = "";
        public string surenameTH = "";
        public string mail = "";
        public string department = "";
        public string title = "";
        public string manager = "";
        public string samaccountname = "";
        public string distinguishedname = "";

        public List<PersonInfoWithUnder> arrPersonInfo = null;

    }
}
