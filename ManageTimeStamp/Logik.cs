﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using TraceComponent;

namespace ManageTimeStamp
{
    class personalData
    {
        public string m_sUserID = "";
        public string m_sUserName = "";
        public string m_sSurname = "";
        public DateTime m_dtStartTime = DateTime.Now;
        public DateTime m_dtEndTime = DateTime.Now;
        public DateTime m_dtWorkingHours = DateTime.Now;
        public DateTime m_dtDate = DateTime.Now;
        public int m_nStatus = 0;
        public int m_nSTypeEntrans = 0;
        public bool m_bStartTime = false;
    }
    class userData
    {
        public string m_sUserID = "";
        public string m_sPrefix = "";
        public string m_sName = "";
        public string m_sSurname = "";
        public string m_sNameTH = "";
        public string m_sSurnameTH = "";
        public string m_sDepCode = "";
        public string m_sDepName = "";
        public string m_sSection = "";
        public string m_sPosition = "";

    }
    public class DateDataInfo
    {
        public string m_sCRFile = "";
        public string m_sMNFile = "";
    }

    class Logik
    {
        public List<personalData> m_personalDataList = null;
        public List<personalData> m_personalDataSortList = null;
        public string m_sIniFile = "";
        public string m_sAmisPath = "";
        //YPT 03.25.2020
        public int m_nStartMonth = 0;
        public int m_nEndMonth = 0;
        //YPT 03.20.2021
        public List<userData> m_userDataList = null;
        public string ModifyData(string sValue)
        {
            int nResult = -1;
            string sReturn = "";
            try
            {
                sValue = sValue.Substring(0, 6);

                int nIndex = 2;

                for (int i = 0; i < 2; i++)
                {
                    sValue = sValue.Insert(nIndex, ":");
                    nIndex = nIndex + 3;
                }
                sReturn = sValue;
                nResult = 0;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);

            }
            return sReturn;
        }

        public bool CallTimeStampProcess(string sInputFile)
        {
            bool bStatus = false;
            try
            {
                string sBufferData = "";
                if (m_personalDataList != null)
                {
                    m_personalDataList.Clear();
                }
                using (StreamReader FileStream = new StreamReader(sInputFile, System.Text.Encoding.Default, true))
                {
                    while ((sBufferData = FileStream.ReadLine()) != null)
                    {
                        if (sBufferData.Length > 0)
                        {
                            bStatus = ReadDataToRecord(sBufferData);
                        }
                    }

                    FileStream.Close();
                }

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        public bool CalculateWorkingHours(DateTime timeStartDef, DateTime timeStart, DateTime timeEnd, ref TimeSpan tWorkingH)
        {
            bool bStatus = false;
            int nStatus = 0;

            try
            {
                TimeSpan tBase = new TimeSpan(9, 0, 0);
                //TimeSpan tBreak = new TimeSpan(1, 0, 0);
                timeStart = ValidateStartTime(timeStartDef, timeStart);

                if (timeEnd < timeStart)
                {
                    timeEnd = timeStart;
                }

                tWorkingH = timeEnd - timeStart;
                //tWorkingH = tWorkingH - tBreak;
                nStatus = TimeSpan.Compare(tWorkingH, tBase);

                if ((nStatus == 1) || (nStatus == 0))
                {
                    // Console.Write("Working is greater than basetime");
                    bStatus = true;
                }
                else
                {
                    //Console.Write("basetime is greater than workingTime");
                    bStatus = false;
                    if (timeEnd == timeStart)
                    {
                        timeEnd = CreateTimeFromDate(0, 0, 0, timeEnd);
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }
        public DateTime ValidateStartTime(DateTime dtDateTDef, DateTime dtDate)
        {
            bool bStatus = false;
            DateTime dtTimeBase;
            DateTime dtTimeReturn = DateTime.Now;

            if (dtDateTDef == null)
            {
                dtTimeBase = CreateTimeFromDate(7, 0, 0, dtDate);
            }
            else
            {
                dtTimeBase = dtDateTDef;
            }

            bStatus = ValidateDateTime(dtTimeBase, dtDate);
            if (bStatus)
            {
                dtTimeReturn = dtDate;
            }
            else
            {
                dtTimeReturn = dtTimeBase;
            }

            return dtTimeReturn;
        }
        public bool ReadDataToRecord(string sValue)
        {
            bool bStatus = false;
            int nIndex = 1;
            int nCount = 0;
            bool bEntans = true; //Allow any floor
            try
            {

                string[] words = sValue.Split(';');

                nCount = words.Length;

                if (nCount > 0)
                {
                    if (m_personalDataList == null)
                    {
                        m_personalDataList = new List<personalData>();
                    }
                    personalData pPerson = new personalData();

                    // Part 3: loop over result array.
                    string cstemp = "";
                    string cstempOrg = "";

                    foreach (string data in words)
                    {
                        // Console.WriteLine("List: " + data);
                        if (nIndex == 1)
                        {
                            bool containsSearchResult = data.Contains("#m");
                            if (!containsSearchResult)
                            {
                                break;
                            }
                        }
                        else if (nIndex == 2)
                        {
                            pPerson.m_dtDate = DateTime.ParseExact(data, "yyyyMMdd", null);

                        }
                        else if (nIndex == 3)
                        {
                            cstemp = ModifyData(data);

                            //pPerson.m_dtStartTime = DateTime.Parse(cstemp);
                            pPerson.m_dtStartTime = DateTime.ParseExact(cstemp, "HH:mm:ss", CultureInfo.InvariantCulture);
                            pPerson.m_dtStartTime = CreateDateFromTime(pPerson.m_dtDate.Year, pPerson.m_dtDate.Month, pPerson.m_dtDate.Day, pPerson.m_dtStartTime);
                            pPerson.m_dtEndTime = pPerson.m_dtStartTime;
                            pPerson.m_bStartTime = true;
                        }
                        else if (nIndex == 14)
                        {
                            pPerson.m_sUserID = data;
                        }
                        else if (nIndex == 15)
                        {
                            pPerson.m_sSurname = data;
                        }
                        else if (nIndex == 16)
                        {
                            pPerson.m_sUserName = data;
                        }
                        else if (nIndex == 17)
                        {
                            cstempOrg = data;
                        }//For door
                        //YPT 10.29.2020 ignore this condition
                        /*else if (nIndex == 8)
                        {
                            cstempOrg = data;
                            string SearchString = "entry reader";
                            int FirstChr = cstempOrg.IndexOf(SearchString);
                            if (FirstChr == -1)
                            {
                                SearchString = "exit reader";
                                FirstChr = cstempOrg.IndexOf(SearchString);

                                if (FirstChr != -1)
                                {
                                    pPerson.m_nSTypeEntrans = 1;
                                }
                            }
                            else
                            {
                                pPerson.m_nSTypeEntrans = 0;
                            }
                        }*/
                        /*else if (nIndex == 9)
                        {
                            cstempOrg = data;

                            bEntans = ValidateEntrans(cstempOrg);

                        }*/
                        nIndex++;

                    }
                    if (bEntans)
                    {
                        //YPT 03.05.2020#Fixed problem when Org is empty.
                        //if ((!string.IsNullOrEmpty(cstempOrg)) && (!ValidatePersonalData(pPerson)))
                        if ((!string.IsNullOrEmpty(pPerson.m_sUserID)) && (!ValidatePersonalData(pPerson)))
                        {
                            m_personalDataList.Add(pPerson);
                            bStatus = true;
                            TraceManager.ProcessTrace(String.Format("Add user [{0}] [{1}] to list.", pPerson.m_sUserID, pPerson.m_sUserName, pPerson.m_dtStartTime.ToString("HH:mm:ss"), pPerson.m_dtEndTime.ToString("HH:mm:ss")));

                        }
                        else
                        {
                            TraceManager.ProcessTrace(String.Format("Don't add user [{0}] [{1}] to list.", pPerson.m_sUserID, pPerson.m_sUserName, pPerson.m_dtStartTime.ToString("HH:mm:ss"), pPerson.m_dtEndTime.ToString("HH:mm:ss")));
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        public DateTime CreateDateFromTime(int nYear, int nMonth, int nDay, DateTime dtTime)
        {
            return new DateTime(nYear, nMonth, nDay, dtTime.Hour, dtTime.Minute, dtTime.Second);
        }
        public DateTime CreateTimeFromDate(int nHour, int nMinute, int nSecond, DateTime dtTime)
        {
            return new DateTime(dtTime.Year, dtTime.Month, dtTime.Day, nHour, nMinute, nSecond);
        }
        public bool ValidatePersonalData(personalData pData)
        {
            bool bStatus = false;

            string sUserID = "";
            string sUserIDN = "";

            DateTime dtEndTime;
            DateTime dtEndTimeN;

            try
            {

                sUserIDN = pData.m_sUserID;
                dtEndTimeN = pData.m_dtStartTime;

                foreach (personalData un in m_personalDataList)
                {
                    sUserID = un.m_sUserID;

                    if (String.Equals(sUserIDN, sUserID, StringComparison.OrdinalIgnoreCase))
                    {
                        dtEndTime = un.m_dtEndTime;

                        if (ValidateDateTime(dtEndTime, dtEndTimeN))
                        {
                            //YPT 10.29.2020
                            /*if (pData.m_nSTypeEntrans == 1)
                            {
                                //UpdateTime
                                un.m_dtEndTime = pData.m_dtEndTime;
                                if (!un.m_bStartTime)
                                {
                                    un.m_dtStartTime = CreateTimeFromDate(0, 0, 0, un.m_dtEndTime);
                                }
                            }
                            else
                            {
                                if (!un.m_bStartTime)
                                {
                                    un.m_dtStartTime = pData.m_dtEndTime;
                                    un.m_bStartTime = true;
                                }
                            }*/

                            if (!un.m_bStartTime)
                            {
                                un.m_dtStartTime = pData.m_dtEndTime;
                                un.m_bStartTime = true;
                            }
                            else {
                                un.m_dtEndTime = dtEndTimeN;
                            }
                        }
                        bStatus = true;
                    }
                }
                /*if (!bStatus)
                {
                    if (pData.m_nSTypeEntrans == 0)
                    {
                        pData.m_bStartTime = true;
                    }
                }*/
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        public bool ValidateDateTime(DateTime sTimeOld, DateTime sTimeNew)
        {
            bool bStatus = false;
            int nStatus = 0;
            try
            {
                nStatus = DateTime.Compare(sTimeOld, sTimeNew);
                if (nStatus < 0)
                {
                    bStatus = true;
                }
                else
                {
                    //Console.Write("t1 is greater than t2");
                    bStatus = false;
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        public bool InsertPersonData(string sDefaultStartTime)
        {
            bool bStatus = false;
            bool bUsedefault = false;

            int nIndex = 0;
            try
            {

                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();

                TimeSpan tWorkignHours = new TimeSpan(0, 0, 0);
                DateTime dtDeafultStartTime = DateTime.Now;

                foreach (personalData un in m_personalDataList)
                {
                    TraceManager.ProcessTrace(String.Format("Count row [{0}]", nIndex));
                    bStatus = false;
                    if (!bUsedefault)
                    {
                        if (!string.IsNullOrEmpty(sDefaultStartTime))
                        {
                            dtDeafultStartTime = DateTime.ParseExact(sDefaultStartTime, "HH:mm", null);
                            dtDeafultStartTime = CreateDateFromTime(un.m_dtStartTime.Year, un.m_dtStartTime.Month, un.m_dtStartTime.Day, dtDeafultStartTime);
                        }
                        else
                        {
                            dtDeafultStartTime = CreateTimeFromDate(7, 0, 0, un.m_dtStartTime);
                        }

                        bUsedefault = true;
                    }
                    if (un.m_bStartTime)
                    {
                        bStatus = CalculateWorkingHours(dtDeafultStartTime, un.m_dtStartTime, un.m_dtEndTime, ref tWorkignHours);
                    }
                    if (bStatus)
                    {
                        un.m_nStatus = 0;
                    }
                    else
                    {
                        un.m_nStatus = 1;
                    }
                    //Validate user
                    un.m_dtWorkingHours = CreateTimeFromDate(tWorkignHours.Hours, tWorkignHours.Minutes, tWorkignHours.Seconds, un.m_dtDate);

                    bStatus = ValidateUser(un);
                    if (!bStatus)
                    {
                        nIndex = sqlCon.CountRow();
                        TraceManager.ProcessTrace(String.Format("Insert user [{0}] [{1}]", un.m_sUserName, un.m_dtStartTime.ToString("HH:mm:ss")));
                        nIndex++;
                        bStatus = sqlCon.Insert(nIndex, un.m_dtDate.ToString("yyyy-MM-dd"), un.m_sUserID, un.m_sUserName, un.m_sSurname, un.m_dtStartTime.ToString("HH:mm:ss"), un.m_dtEndTime.ToString("HH:mm:ss"), un.m_dtWorkingHours.ToString("HH:mm:ss"), un.m_nStatus);
                        if (bStatus)
                        {
                            TraceManager.ProcessTrace("Done");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }
        public int GetItemCountFormDataBase()
        {
            int nCount = 0;
            try
            {
                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();
                nCount = sqlCon.CountRow();
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return nCount;
        }
        public DateDataInfo GenerateDateFile(string sDirectory)
        {
            bool bStatus = false;
            string sCRFileName = "";
            string sMNFileName = "";

            DateDataInfo dtInfo = new DateDataInfo();

            try
            {
                DateTime dtCurrent = DateTime.Now;
                DateTime dtHistory = dtCurrent.AddDays(-1);

                if (sDirectory.LastIndexOf("\\") != (sDirectory.Length - 1))
                {
                    sDirectory += "\\";
                }
                sCRFileName = sDirectory;
                sCRFileName += "Msg" + dtCurrent.ToString("yyyyMMdd") + ".log";

                sMNFileName = sDirectory;
                sMNFileName += "Msg" + dtHistory.ToString("yyyyMMdd") + ".log";

                if (File.Exists(sCRFileName))
                {
                    dtInfo.m_sCRFile = sCRFileName;
                    TraceManager.ProcessTrace(String.Format("Set CR file [{0}]", dtInfo.m_sCRFile));
                }
                if (File.Exists(sMNFileName))
                {
                    dtInfo.m_sMNFile = sMNFileName;
                    TraceManager.ProcessTrace(String.Format("Set MN file [{0}]", dtInfo.m_sMNFile));
                }
                if (!string.IsNullOrEmpty(dtInfo.m_sCRFile))
                {
                    bStatus = true;
                }

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return dtInfo;
        }
        public bool ValidateUser(personalData unData)
        {
            bool bStatus = false;

            try
            {
                string sCurrentDate = "";

                DataTable dtTableInfo = null;
                DateTime dtCurrent = unData.m_dtDate;

                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();

                sCurrentDate = dtCurrent.ToString("yyyy-MM-dd");
                TraceManager.ProcessTrace(String.Format("Date [{0}] user [{1}]", sCurrentDate, unData.m_sUserID));

                dtTableInfo = sqlCon.Select(sCurrentDate, sCurrentDate, unData.m_sUserID);
                if (dtTableInfo != null)
                {
                    if (dtTableInfo.Rows.Count > 0)
                    {
                        TraceManager.ProcessTrace(String.Format("Update [{0}]", unData.m_sUserID, unData.m_dtStartTime.ToString("HH:MM:SS")));
                        //sDate = dtTableInfo.Rows[i].ItemArray[j].ToString();
                        bStatus = sqlCon.Update(sCurrentDate, unData.m_sUserID, unData.m_dtStartTime.ToString("HH:mm:ss"), unData.m_dtEndTime.ToString("HH:mm:ss"), unData.m_dtWorkingHours.ToString("HH:mm:ss"), unData.m_nStatus);
                        if (bStatus)
                        {
                            TraceManager.ProcessTrace("Done");
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }
        public bool ValidateEntrans(string sValue)
        {
            bool bStatus = false;

            try
            {
                string sEntrans = "MAIN ENTRANS";

                int FirstChr = sValue.IndexOf(sEntrans);
                if (FirstChr == -1)
                {
                    sEntrans = "MAIN ENTRANCE";
                    FirstChr = sValue.IndexOf(sEntrans);
                    if (FirstChr != -1)
                    {
                        bStatus = true;
                    }
                }
                else
                {
                    bStatus = true;
                }
                if (bStatus)
                {
                    //Ignore 15 floor
                    if (String.Equals(sValue, "MAIN ENTRANS 15Fl", StringComparison.OrdinalIgnoreCase))
                    {
                        bStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }
        public bool InsertRecordForCalendar(PersonInfoWithUnder pUserinfo)
        {
            bool bStatus = false;
            try
            {
                bool bFinish = false;
                DateTime now = DateTime.Now;
                DateTime dtNextDay = DateTime.Now;
                //YPT 03.25.2020
                //DateTime dtStartDate = new DateTime(now.Year, now.Month, 1);
                for (int i = m_nStartMonth; i <= m_nEndMonth; i++)
                {
                    bFinish = false;

                    DateTime dtStartDate = new DateTime(now.Year, i, 1);
                    DateTime dtEndDate = dtStartDate.AddMonths(1).AddDays(-1);
                    dtNextDay = dtStartDate;
                    string sStartDate = "";
                    TraceManager.ProcessTrace(String.Format("Insert [{0}].", dtNextDay.ToString("yyyy-MM-dd")));

                    int nIndex = 0;
                    do
                    {
                        if (dtNextDay <= dtEndDate)
                        {
                            sStartDate = dtNextDay.ToString("yyyy-MM-dd");

                            //Insert data

                            SQLConnect sqlCon = new SQLConnect();
                            sqlCon.Initialize();

                            nIndex = sqlCon.CountRow();
                            nIndex++;

                            bStatus = sqlCon.Insert(nIndex, sStartDate, pUserinfo.id, pUserinfo.nameTH, pUserinfo.surenameTH, "00:00:00", "00:00:00", "00:00:00", 1);
                            if (!bStatus)
                            {
                                bFinish = true;
                            }
                            dtNextDay = dtNextDay.AddDays(+1);
                            nIndex++;
                        }
                        else
                        {
                            bFinish = true;
                        }

                    } while (!bFinish);

                }

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }
        public bool CallAllUserInfo(string sValue)
        {
            bool bStatus = false;
            string sSoapInput = "";

            try
            {
                TraceManager.ProcessTrace(String.Format("CallAllUserInfo [{0}].", sValue));
                //YPT 03.25.2020
                bStatus = ValidateMonth(sValue);

                if (bStatus)
                {
                    sSoapInput = GenerateSoapInput();

                    //Builds the connection to the WebService.
                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create("https://hr.etda.or.th/webservice-production/Service.asmx");
                    req.Headers.Add("SOAPAction", "http://tempuri.org/InquiryPersonnelNewInfo");
                    req.ContentType = "text/xml; charset=\"utf-8\"";
                    req.Accept = "text/xml";
                    req.Method = "POST";

                    //Passes the SoapRequest String to the WebService
                    using (Stream stm = req.GetRequestStream())
                    {
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(sSoapInput);
                        }
                    }

                    using (WebResponse Serviceres = req.GetResponse())
                    {
                        using (StreamReader rd = new StreamReader(Serviceres.GetResponseStream()))
                        {
                            var ServiceResult = rd.ReadToEnd();
                            bStatus = GetStaffInfo(ServiceResult);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }

            return bStatus;

        }
        public string GenerateSoapInput()
        {
            string sRequest = "";
            sRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">";
            sRequest = sRequest + "<soapenv:Header/>";
            sRequest = sRequest + "<soapenv:Body>";
            sRequest = sRequest + "<tem:InquiryPersonnelNewInfo>";
            sRequest = sRequest + " <tem:staffID>";
            sRequest = sRequest + "</tem:staffID>";
            sRequest = sRequest + "</tem:InquiryPersonnelNewInfo>";
            sRequest = sRequest + "</soapenv:Body>";
            sRequest = sRequest + "</soapenv:Envelope>";

            return sRequest;
        }
        public bool GetStaffInfo(string sSoapData)
        {
            bool bStatus = false;
            string sStaffNameEN = "";
            string sstaffSurnameEN = "";
            string sStaffNameTH = "";
            string sstaffSurnameTH = "";
            string sId = "";

            int nCount = 0;

            try
            {
                if (!string.IsNullOrEmpty(sSoapData))
                {
                    PersonInfoWithUnder personInfo = new PersonInfoWithUnder();

                    XmlDocument Doc = new XmlDocument();

                    Doc.LoadXml(sSoapData);

                    nCount = Doc.GetElementsByTagName("PersonnelNewInfo").Count;

                    for (int i = 0; i < nCount; i++)
                    {
                        XmlNode node = Doc.GetElementsByTagName("PersonnelNewInfo").Item(i);

                        foreach (XmlNode item in node.ChildNodes)
                        {
                            if ((item).NodeType == XmlNodeType.Element)
                            {
                                //Get the Element value here
                                if (String.Equals(item.Name, "staffID", StringComparison.OrdinalIgnoreCase))
                                {
                                    sId = ((item).FirstChild).Value;
                                }
                                else if (String.Equals(item.Name, "staffNameEN", StringComparison.OrdinalIgnoreCase))
                                {
                                    sStaffNameEN = ((item).FirstChild).Value;
                                }
                                else if (String.Equals(item.Name, "staffSurnameEN", StringComparison.OrdinalIgnoreCase))
                                {
                                    sstaffSurnameEN = ((item).FirstChild).Value;
                                }
                                else if (String.Equals(item.Name, "staffNameTH", StringComparison.OrdinalIgnoreCase))
                                {
                                    sStaffNameTH = ((item).FirstChild).Value;
                                }
                                else if (String.Equals(item.Name, "staffSurnameTH", StringComparison.OrdinalIgnoreCase))
                                {
                                    sstaffSurnameTH = ((item).FirstChild).Value;
                                }
                            }
                            else
                            {
                                //Get the Element value here
                                string errorField2 = (item).Value;
                                Console.WriteLine("NodeValue = " + errorField2);
                            }

                        }
                        if (!string.IsNullOrEmpty(sId))
                        {
                            PersonInfoWithUnder person = new PersonInfoWithUnder();
                            person.id = "000" + sId;
                            person.nameTH = sStaffNameTH;
                            person.surenameTH = sstaffSurnameTH;

                            bStatus = InsertRecordForCalendar(person);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        public bool ValidateMonth(string sValue)
        {
            bool bStatus = false;
            try
            {
                TraceManager.ProcessTrace(String.Format("ValidateMonth [{0}].", sValue));
                string sTempMS = "";
                string sTempME = "";

                string[] month = sValue.Split('-');

                sTempMS = month[0];
                if (month.Length > 1)
                {
                    sTempME = month[1];
                }

                if ((!string.IsNullOrEmpty(sTempMS)))
                {
                    m_nStartMonth = int.Parse(sTempMS);
                    bStatus = true;
                }
                if ((!string.IsNullOrEmpty(sTempME)))
                {
                    m_nEndMonth = int.Parse(sTempME);
                }
                else
                {
                    m_nEndMonth = int.Parse(sTempMS);
                }
                TraceManager.ProcessTrace(String.Format("Start [{0}] End [{1}] .", m_nStartMonth, m_nEndMonth));
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }


        public bool isUserExists(string sValue)
        {
            bool bStatus = false;
            try
            {
                TraceManager.ProcessTrace(String.Format("Userid [{0}].", sValue));

                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();

                DataTable dtTableInfo = null;

                dtTableInfo = sqlCon.SelectUserInfo(sValue);
                if (dtTableInfo != null)
                {
                    bStatus = true;
                }

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }

        public bool InsertUser(List<userData> userData, string sRangeMonth)
        {
            bool bStatus = false;
            try
            {
                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();

                foreach (userData un in m_userDataList)
                {
                    TraceManager.ProcessTrace(String.Format("Userid [{0}].", un.m_sUserID));

                    bStatus = sqlCon.InsertUserInfo(un.m_sUserID, un.m_sPrefix, un.m_sNameTH, un.m_sSurnameTH, un.m_sName, un.m_sSurname, un.m_sDepCode, un.m_sDepName, un.m_sSection, un.m_sPosition);

                    if (!bStatus)
                    {
                        TraceManager.ProcessTrace(String.Format("Failed to add Userid [{0}].", un.m_sUserID));
                        break;
                    }
                    else {
                        
                        bStatus = ValidateMonth(sRangeMonth);

                        PersonInfoWithUnder person = new PersonInfoWithUnder();
                        person.id = "000" + un.m_sUserID;
                        person.nameTH = un.m_sNameTH;
                        person.surenameTH = un.m_sSurnameTH;

                        bStatus = InsertRecordForCalendar(person);

                        if (!bStatus)
                        {
                            TraceManager.ProcessTrace(String.Format("Failed to add Userid [{0}].", un.m_sUserID));
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }
        public bool ReadcsvFile(string sFilename)
        {
            bool bStatus = false;
            try
            {
                string sBufferData = "";

                Logik process = new Logik();

                using (StreamReader FileStream = new StreamReader(sFilename, System.Text.Encoding.Default, true))
                {
                    while ((sBufferData = FileStream.ReadLine()) != null)
                    {
                        if (sBufferData.Length > 0)
                        {
                            int nIndex = 0;
                            int nCount = 0;

                            string[] words = sBufferData.Split(';');

                            nCount = words.Length;

                            if (nCount > 0)
                            {
                                if (m_userDataList == null)
                                {
                                    m_userDataList = new List<userData>();
                                }
                                userData pPerson = new userData();

                                foreach (string data in words)
                                {
                                    // Console.WriteLine("List: " + data);
                                    if (nIndex == 0)
                                    {
                                        pPerson.m_sUserID = data;
                                    }
                                    else if (nIndex == 1)
                                    {
                                        pPerson.m_sPrefix = data;

                                    }
                                    else if (nIndex == 2)
                                    {
                                        pPerson.m_sNameTH = data;
                                    }
                                    else if (nIndex == 3)
                                    {
                                        pPerson.m_sSurnameTH = data;
                                    }
                                    else if (nIndex == 4)
                                    {
                                        pPerson.m_sName = data;
                                    }
                                    else if (nIndex == 5)
                                    {
                                        pPerson.m_sSurname = data;
                                    }
                                    else if (nIndex == 6)
                                    {
                                        pPerson.m_sDepCode = data;
                                    }
                                    else if (nIndex == 7)
                                    {
                                        pPerson.m_sDepName = data;
                                    }
                                    else if (nIndex == 8)
                                    {
                                        pPerson.m_sSection = data;
                                    }
                                    else if (nIndex == 9)
                                    {
                                        pPerson.m_sPosition = data;
                                    }
                                    nIndex++;

                                }
                                if (!string.IsNullOrEmpty(pPerson.m_sUserID))
                                {
                                    if (!process.isUserExists(pPerson.m_sUserID))
                                    {
                                        m_userDataList.Add(pPerson);
                                        bStatus = true;
                                        TraceManager.ProcessTrace(String.Format("Add Userid [{0}] to list.", pPerson.m_sUserID));
                                    }
                                    else {
                                        TraceManager.ProcessTrace(String.Format("Userid [{0}] Exists.", pPerson.m_sUserID));
                                    }
                                  
                                }
                            }
                        }
                    }

                    FileStream.Close();
                }


            }
            catch (Exception ex)
            {

            }
            return bStatus;
        }

    }
}
