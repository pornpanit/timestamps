﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Reflection;
using System.IO;
using TraceComponent;
using ManageTimeStamp;

namespace IniComponent
{
    public static class IniManager
    {
        private static String m_sFilePath = "";
         
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
 
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        public static void SetIniFile(String sIniFile)
        {
            m_sFilePath = sIniFile;
        }
 
        public static void Write(string section, string key, string value)
        {
            WritePrivateProfileString(section, key, value, m_sFilePath);
        }
 
        public static string Read(string section, string key)
        {
            StringBuilder SB = new StringBuilder(255);
            int i = GetPrivateProfileString(section, key, "", SB, 255, m_sFilePath);

            TraceManager.ProcessTrace(String.Format("Get {0} in section [{1}]: {2}.", key, section, SB.ToString()));
            return SB.ToString();
        }

        public static bool GetModuleIni(ref String sIniFile, ref String sAmisPath)
        {
            TraceManager.FunctionStartTrace("IniManager::GetModuleIni");
            bool bResult = false;

            sIniFile = Assembly.GetExecutingAssembly().Location;

            int nIndex = sIniFile.LastIndexOf(".");
            if (nIndex > 0)
            {
                sIniFile = sIniFile.Remove(nIndex);
                sIniFile += ".ini";
                if (File.Exists(sIniFile))
                {
                    TraceManager.ProcessTrace(String.Format("Ini file {0} exists.", sIniFile));
                    try
                    {
                        //YPT 01.02.2013. Fixed exception when ini file is readonly.
                        FileStream fStream = File.Open(sIniFile, FileMode.Open, FileAccess.Read);
                        fStream.Close();
                        m_sFilePath = sIniFile;
                        bResult = true;
                        TraceManager.ProcessTrace("Ini file is accessible.");
                    }
                    catch (Exception e)
                    {
                        TraceManager.ErrorTrace("Can't access ini file.");
                        TraceManager.ErrorTrace(e.Message);
                    }
                }
                else
                {
                    TraceManager.ErrorTrace(String.Format("Ini file {0} doesn't exist", sIniFile));
                }
            }

            if (bResult)
            {
                sAmisPath = Environment.GetEnvironmentVariable("DynPGM");
                //YPT 06.27.2013. Fixed problem on RKCD VMWare.
                if (sAmisPath == null)
                {
                    sAmisPath = "";
                }
                TraceManager.ProcessTrace(String.Format("Get value from DynPGM is {0}\n", sAmisPath));

                if ((sAmisPath.CompareTo("") == 0) || (!Directory.Exists(sAmisPath)))
                {
                    sAmisPath = Read("Variable", "DynPGM");
                    //YPT 10.10.2013. Fixed stand alone problem.
                   // if ((sAmisPath.CompareTo("") == 0) || (!Directory.Exists(sAmisPath)))
                    if (sAmisPath.CompareTo("") == 0)
                    {
                        nIndex = sIniFile.LastIndexOf("\\BSA");
                        if (nIndex > 0)
                        {
                            sAmisPath = sIniFile.Remove(nIndex);
                            TraceManager.ProcessTrace(String.Format("Get Amis path {0}.", sAmisPath));
                        }
                        else
                        {
                            TraceManager.ErrorTrace(String.Format("Can't find BSA inside path {0}.", sIniFile));
                            bResult = false;
                        }
                    }
                }

                if (sAmisPath.LastIndexOf("\\") != (sAmisPath.Length - 1))
                {
                    sAmisPath += "\\";
                }
            }

            TraceManager.FunctionEndTrace("IniManager::GetModuleIni", bResult);
            return bResult;
        }
        public static string GetTracePath()
        {
            string sSection = "TRACE";
            string sKey = "TracePath";
            string sReturnValue = "";

            sReturnValue = Read(sSection, sKey);

            if (string.IsNullOrEmpty(sReturnValue))
            {
                sSection = "Allgemein";
                sKey = "TempPath";
                sReturnValue = Read(sSection, sKey);
         
            }

            if (!string.IsNullOrEmpty(sReturnValue))
            {
                if (sReturnValue.LastIndexOf("\\") != (sReturnValue.Length - 1))
                {
                    sReturnValue += "\\";
                }
            }
            else {
                sReturnValue += "C:\\temp\\";
            }
            TraceManager.ProcessTrace(String.Format("Return {0}.", sReturnValue));

            return sReturnValue;
        }
        //YPT 02.27.2019#1236
        public static string GettempFileName(string sPath,string sFirst,string sEnd)
        {
            TraceManager.FunctionStartTrace("IniManager::GettempFileName");
            int nResult = -1;
            string sReturnFileName = "";
            string sSection = "";
            string sKey = "";
            string sPathTemp = "";


            if (string.IsNullOrEmpty(sPath))
            {
                sSection = "Allgemein";
                sKey = "TempPath";
                sPathTemp = Read(sSection, sKey);
                if (!string.IsNullOrEmpty(sPathTemp))
                {
                    if (sPathTemp.LastIndexOf("\\") != (sPathTemp.Length - 1))
                    {
                        sPathTemp += "\\";
                    }
                    sPath = sPathTemp;
                    TraceManager.ProcessTrace(String.Format("Set tempPath {0}.", sPath));
                }
            }
            if (!string.IsNullOrEmpty(sPath))
            {
                int nRandom = 0;
                nRandom = IntUtil.Random(0, 100);

                sReturnFileName = sPath + sFirst + nRandom.ToString() + sEnd;
                TraceManager.ProcessTrace(String.Format("TempFileName {0}.", sReturnFileName));
                if (!string.IsNullOrEmpty(sPath))
                {
                    nResult = 0;
                }
            }

            TraceManager.FunctionEndTrace("IniManager::GettempFileName", nResult);

            return sReturnFileName;
        }
    }
}
