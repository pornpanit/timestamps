﻿using DocumentFormat.OpenXml.Packaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.DirectoryServices;
using System.Diagnostics;
using System.Xml;
using ManageTimeStamp;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public JObject post = null;
        public Form1()
        {
            InitializeComponent();
        }
        public class ResponseData
        {
            public string errorCode { get; set; }
            public string errorMessage { get; set; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CallAllUserInfo();
        }
        public bool InsertRecordForCalendar(PersonInfoWithUnder pUserinfo)
        {
            bool bStatus = false;
            try
            {
                bool bFinish = false;
                DateTime now = DateTime.Now;
                DateTime dtNextDay = DateTime.Now;
                DateTime dtStartDate = new DateTime(now.Year, now.Month, 1);
                DateTime dtEndDate = dtStartDate.AddMonths(1).AddDays(-1);
                dtNextDay = dtStartDate;
                string sStartDate = "";

                int nIndex = 0;
                do
                {
                    if (dtNextDay <= dtEndDate)
                    {
                        sStartDate = dtNextDay.ToString("yyyy-MM-dd");

                        //Insert data

                        SQLConnect sqlCon = new SQLConnect();
                        sqlCon.Initialize();

                        nIndex = sqlCon.CountRow();
                        nIndex++;

                        bStatus = sqlCon.Insert(nIndex, sStartDate, pUserinfo.id, pUserinfo.nameTH, pUserinfo.surenameTH, "00:00:00", "00:00:00", "00:00:00", 1);
                        if (!bStatus)
                        {
                            bFinish = true;
                        }

                        dtNextDay = dtNextDay.AddDays(+1);
                    }
                    else
                    {
                        bFinish = true;
                    }

                } while (!bFinish);
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        public bool CallAllUserInfo()
        {
            bool bStatus = false;
            string sSoapInput = "";

            try
            {
                sSoapInput = GenerateSoapInput();

                //Builds the connection to the WebService.
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create("https://hr.etda.or.th/webservice-production/Service.asmx");
                req.Headers.Add("SOAPAction", "http://tempuri.org/InquiryPersonnelNewInfo");
                req.ContentType = "text/xml; charset=\"utf-8\"";
                req.Accept = "text/xml";
                req.Method = "POST";

                //Passes the SoapRequest String to the WebService
                using (Stream stm = req.GetRequestStream())
                {
                    using (StreamWriter stmw = new StreamWriter(stm))
                    {
                        stmw.Write(sSoapInput);
                    }
                }

                using (WebResponse Serviceres = req.GetResponse())
                {
                    using (StreamReader rd = new StreamReader(Serviceres.GetResponseStream()))
                    {
                        var ServiceResult = rd.ReadToEnd();
                        bStatus = GetStaffInfo(ServiceResult);
                    }
                }
            }
            catch (Exception e)
            {

            }

            return bStatus;

        }
        public string GenerateSoapInput()
        {
            string sRequest = "";
            sRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">";
            sRequest = sRequest + "<soapenv:Header/>";
            sRequest = sRequest + "<soapenv:Body>";
            sRequest = sRequest + "<tem:InquiryPersonnelNewInfo>";
            sRequest = sRequest + " <tem:staffID>";
            sRequest = sRequest + "</tem:staffID>";
            sRequest = sRequest + "</tem:InquiryPersonnelNewInfo>";
            sRequest = sRequest + "</soapenv:Body>";
            sRequest = sRequest + "</soapenv:Envelope>";

            return sRequest;
        }
        public bool GetStaffInfo(string sSoapData)
        {
            bool bStatus = false;
            string sStaffNameEN = "";
            string sstaffSurnameEN = "";
            string sStaffNameTH = "";
            string sstaffSurnameTH = "";

            string sId = "";

            int nCount = 0;

            try
            {
                if (!string.IsNullOrEmpty(sSoapData))
                {
                    PersonInfoWithUnder personInfo = new PersonInfoWithUnder();

                    XmlDocument Doc = new XmlDocument();

                    Doc.LoadXml(sSoapData);

                    nCount = Doc.GetElementsByTagName("PersonnelNewInfo").Count;

                    for (int i = 0; i < nCount; i++)
                    {
                        XmlNode node = Doc.GetElementsByTagName("PersonnelNewInfo").Item(i);

                        foreach (XmlNode item in node.ChildNodes)
                        {
                            if ((item).NodeType == XmlNodeType.Element)
                            {
                                //Get the Element value here
                                if (String.Equals(item.Name, "staffID", StringComparison.OrdinalIgnoreCase))
                                {
                                    sId = ((item).FirstChild).Value;
                                }
                                else if (String.Equals(item.Name, "staffNameEN", StringComparison.OrdinalIgnoreCase))
                                {
                                    sStaffNameEN = ((item).FirstChild).Value;
                                }
                                else if (String.Equals(item.Name, "staffSurnameEN", StringComparison.OrdinalIgnoreCase))
                                {
                                    sstaffSurnameEN = ((item).FirstChild).Value;
                                }
                                else if (String.Equals(item.Name, "staffNameTH", StringComparison.OrdinalIgnoreCase))
                                {
                                    sStaffNameTH = ((item).FirstChild).Value;
                                }
                                else if (String.Equals(item.Name, "staffSurnameTH", StringComparison.OrdinalIgnoreCase))
                                {
                                    sstaffSurnameTH = ((item).FirstChild).Value;
                                }
                            }
                            else
                            {
                                //Get the Element value here
                                string errorField2 = (item).Value;
                                Console.WriteLine("NodeValue = " + errorField2);
                            }

                        }
                        if (!string.IsNullOrEmpty(sStaffNameTH))
                        {
                            PersonInfoWithUnder person = new PersonInfoWithUnder();
                            person.id = "000" + sId;
                            person.nameTH = sStaffNameTH;
                            person.surenameTH = sstaffSurnameTH;

                            bStatus = InsertRecordForCalendar(person);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }

    }
}
