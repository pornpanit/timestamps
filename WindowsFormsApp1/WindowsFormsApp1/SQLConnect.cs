﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TraceComponent;

namespace ManageTimeStamp
{
    class SQLConnect
    {
        private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;

        public void Initialize()
        {
            server = "10.3.2.201";
            database = "etda_timeattendance";
            uid = "etda_timeattenda";
            password = "S7$JME7Cda";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            /*server = "localhost";
            database = "etda_timeattendance";
            uid = "root";
            password = "1234";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";*/

            TraceManager.ProcessTrace(String.Format("commectionstring [{0}]",connectionString));
            connection = new MySqlConnection(connectionString);
        }

        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        //MessageBox.Show("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        //MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }
        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                //MessageBox.Show(ex.Message);
                TraceManager.ProcessTrace(String.Format("exception [{0}]", ex.Message));
                return false;
            }
        }
        public bool Insert(int nId,string sDate, string sUserid,string sUsername, string sSurname, string sStarttime, string sEndtime,string sWorkingHours,int nStatus)
        {
            bool bStatus = false;
            // string query = "INSERT INTO timestampinfo(id,userid,username,surname,starttime,endtime) VALUE ('2','0002','test','tsetsur','08:00:00','10:00:00')";
            try
            {
                string query = "INSERT INTO timestampinfo(id,datetime,userid,username,surname,starttime,endtime,workinghours,statusinfo) VALUE (";
                query += "'";
                query += nId.ToString();
                query += "'";
                query += ",";
                query += "'";
                query += sDate;
                query += "'";
                query += ",";
                query += "'";
                query += sUserid;
                query += "'";
                query += ",";
                query += "'";
                query += sUsername;
                query += "'";
                query += ",";
                query += "'";
                query += sSurname;
                query += "'";
                query += ",";
                query += "'";
                query += sStarttime;
                query += "'";
                query += ",";
                query += "'";
                query += sEndtime;
                query += "'";
                query += ",";
                query += "'";
                query += sWorkingHours;
                query += "'";
                query += ",";
                query += "'";
                query += nStatus.ToString();
                query += "'";
                query += ")";

                TraceManager.ProcessTrace(String.Format("command [{0}]", query));

                //open connection
                if (this.OpenConnection() == true)
                {
                    //create command and assign the query and connection from the constructor
                    MySqlCommand cmd = new MySqlCommand(query, connection);

                    //Execute command
                    cmd.ExecuteNonQuery();

                    //close connection
                    this.CloseConnection();
                    bStatus = true;
                }
            }
            catch (MySqlException ex)
            {
                //MessageBox.Show(ex.Message);
                TraceManager.ProcessTrace(String.Format("exception [{0}]", ex.Message));
                return false;
            }
            
            return bStatus;
        }
        public int CountRow()
        {
            int nCount = 0;
            try
            {
                string query = "SELECT COUNT(*) FROM timestampinfo;";
             
                //open connection
                if (this.OpenConnection() == true)
                {
                    //create command and assign the query and connection from the constructor
                    // SqlCommand cmd = new SqlCommand(query);
                    MySqlCommand cmd = new MySqlCommand(query, connection);

                    using (MySqlDataAdapter da = new MySqlDataAdapter())
                    {
                        nCount = Convert.ToInt32(cmd.ExecuteScalar());
                    }
                    //close connection
                    this.CloseConnection();
                }
            }
            catch (MySqlException ex)
            {
                //MessageBox.Show(ex.Message);

            }

            return nCount;
        }
        public DataTable Select(string sDateStart, string sDateEnd, string sUserid)
        {
            bool bStatus = false;
            DataTable dtTableInfo = null;
            // string query = "INSERT INTO timestampinfo(id,userid,username,surname,starttime,endtime) VALUE ('2','0002','test','tsetsur','08:00:00','10:00:00')";
            try
            {
                //SELECT* FROM usertimestamp.timestampinfo WHERE datetime = '2020-02-03' and userid = '00000024';
                string query = "SELECT * FROM timestampinfo WHERE datetime BETWEEN";
                query += "'";
                query += sDateStart;
                query += "' ";
                query += "AND '";
                query += sDateEnd;
                query += "'";

                if (!String.Equals(sUserid, "*", StringComparison.OrdinalIgnoreCase))
                {
                    query += " AND userid = '";
                    query += sUserid;
                    query += "'";
                }
                query += " ORDER BY DATE(datetime) ASC;";
                //open connection
                if (this.OpenConnection() == true)
                {
                    //create command and assign the query and connection from the constructor
                    // SqlCommand cmd = new SqlCommand(query);
                    MySqlCommand cmd = new MySqlCommand(query, connection);

                    using (MySqlDataAdapter da = new MySqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            da.Fill(dt);
                            dtTableInfo = dt;
                            // GridView1.DataSource = dt;
                            // GridView1.DataBind();
                        }
                    }
                    //close connection
                    this.CloseConnection();
                    bStatus = true;
                }
            }
            catch (MySqlException ex)
            {
                //MessageBox.Show(ex.Message);

            }

            return dtTableInfo;
        }
        public bool Update(string sDate, string sUserid, string sStarttime, string sEndtime, string sWorkingHours, int nStatus)
        {
            bool bStatus = false;
           try
            {
                string query = "UPDATE timestampinfo set starttime=";
                query += "'";
                query += sStarttime;
                query += "', endtime=";
                query += "'";
                query += sEndtime;
                query += "', workinghours=";
                query += "'";
                query += sWorkingHours;
                query += "', statusinfo=";
                query += "'";
                query += nStatus.ToString();
                query += "'";

                query += " WHERE userid = ";
                query += "'";
                query += sUserid;
                query += "' and datetime=";
                query += "'";
                query += sDate;
                query += "'";
             
                //open connection
                if (this.OpenConnection() == true)
                {
                    //create command and assign the query and connection from the constructor
                    MySqlCommand cmd = new MySqlCommand(query, connection);

                    //Execute command
                    cmd.ExecuteNonQuery();

                    //close connection
                    this.CloseConnection();
                    bStatus = true;
                }
            }
            catch (MySqlException ex)
            {
                //MessageBox.Show(ex.Message);
                return false;
            }

            return bStatus;
        }
    }
}
