﻿using ClosedXML.Excel;
using MySqlDatabase;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebTimeStamps
{
    public partial class TimeSheetsPeriod : System.Web.UI.Page
    {
        public bool m_bStatusDoneActivity = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                try
                {
                    // + Convert.ToString(Session["password"]);
                    bool bStatus = false;

                    string sUserName = "";
                    string sSamaccountname = "";
                    string sName = "";
                    string sEmail = "";
                    string sPassword = "";
                    string sTitle = "";
                    string sUsernamesurenameth = "";
                    string sUserID = "";
                    string sDateStart = "";
                    string sDateEnd = "";

                    sUserName = Convert.ToString(Session["user"]);
                    sPassword = Convert.ToString(Session["password"]);
                    sSamaccountname = Convert.ToString(Session["samaccountname"]);
                    sName = Convert.ToString(Session["name"]);
                    sEmail = Convert.ToString(Session["email"]);
                    sTitle = Convert.ToString(Session["title"]);
                    sUserID = Convert.ToString(Session["userid"]);
                    if (ValidateSession())
                    {
                        sUsernamesurenameth = Convert.ToString(Session["usernamesurenameth"]);
                        lb_username_surname.Text = sUsernamesurenameth;
                        txt_userid.Text = sUserID;
                        lbl_PS.Text = sTitle;

                        DateTime dtCurrent = DateTime.Now;
                        txtDate.Text = dtCurrent.ToString("dd/MM/yyyy");
                        //YPT 12.12.2020
                        //lbl_datedone.Text = dtCurrent.ToString("dd/MM/yyyy");

                        Logik process = new Logik();
                        process.GetStartAndEndDateView(ref sDateStart, ref sDateEnd);

                        DateTime dtDateS = DateTime.ParseExact(sDateStart, "yyyy-MM-dd", null);
                        DateTime dtDateE = DateTime.ParseExact(sDateEnd, "yyyy-MM-dd", null);

                        sDateStart = dtDateS.ToString("dd/MM/yyyy");
                        sDateEnd = dtDateE.ToString("dd/MM/yyyy");

                        txtDateStart.Text = sDateStart;
                        txtDateEnd.Text = sDateEnd;

                        GetProjectList();
                        BindData(true);

                        EnableStaffMode();
                    }
                }
                catch (Exception ex)
                {
                    // MessageBox.Show(ex.Message);

                }

            }
        }
        protected void btnload_Click(object sender, EventArgs e)
        {
            try
            {
                //Insert data
                string sEmpId = "";
                string sTaskId = "";
                string sMessage = "";

                lbl_ok.Text = "";
                lbl_error.Text = "";
                lbl_ok.Visible = false;
                lbl_error.Visible = false;

                bool bUpdateTimeSheet = true;
                bool bStatus = true;

                //YPT 12.12.2020
                if (ValidateLink())
                {
                    lbl_result.ForeColor = System.Drawing.Color.Black;
                   
                    sEmpId = txt_userid.Text;
                    sTaskId = ddp_subact.SelectedItem.Text;

                    sTaskId = GetTaskID(sTaskId);

                    if (String.Equals(ddp_hours.SelectedValue, "0", StringComparison.OrdinalIgnoreCase))
                    {
                        bUpdateTimeSheet = false;
                    }

                    if (bUpdateTimeSheet)
                    {
                        bStatus = UpdateTimeSheet(sEmpId, sTaskId);
                    }
                    //Update status
                    if (bStatus)
                    {
                        bStatus = UpdateProjectStatus(sEmpId, sTaskId);
                        if (bStatus)
                        {
                            //Update status to hidden
                            lbl_hiddenStatus.Text = ddp_status.SelectedValue;
                        }
                    }
                    if (bStatus)
                    {
                        sMessage = "Completed";

                        lbl_error.Visible = false;
                        lbl_ok.Text = sMessage;
                        txt_additional.Text = "";
                        lbl_ok.Visible = true;

                        BindData(true);
                    }
                    else
                    {
                        sMessage = "Failed! Please check the data.";
                        lbl_error.Text = sMessage;
                        lbl_ok.Visible = false;
                        lbl_error.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        public void Onlogout(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("LoginAttendance.aspx");
        }
        public void OnTimeAtt(object sender, EventArgs e)
        {
            Response.Redirect("TimeAtt.aspx");
        }
        public void OnTimeSheet(object sender, EventArgs e)
        {
            Response.Redirect("TimeSheets.aspx");
        }
        public void OnStaffUnder(object sender, EventArgs e)
        {
            Session["parentpage"] = "TimeSheets.aspx";
            Response.Redirect("StaffViewer.aspx");
        }
        public void OnReport(object sender, EventArgs e)
        {
            Response.Redirect("ReportViewer.aspx");
        }
        public void OnReportResult(object sender, EventArgs e)
        {
            Response.Redirect("ReportResult.aspx");
        }

        public bool GetProjectList()
        {
            bool bStatus = false;
            string sCommand = "";
            DataTable dtResult;
            string sSelectionId = "";
            try
            {
                Logik process = new Logik();
                sCommand = "select distinct project from etda_t_project_period where statusview = 1;";
                dtResult = process.SelectDataByCmd(sCommand);
                if (dtResult != null)
                {
                    string sID = "";
                    string sValue = "";

                    foreach (DataRow row in dtResult.Rows)
                    {
                       // sID = row["task_id"].ToString();

                        sValue = row["project"].ToString();
                        sID = sValue.Substring(0, 6);
                        if ((!string.IsNullOrEmpty(sValue)) && (!string.IsNullOrEmpty(sID)))
                        {
                            ddp_mainproject.Items.Add(new ListItem(sValue, sID));
                        }
                    }
                }
                sSelectionId = ddp_mainproject.SelectedValue;
                GetMainAcivityList(sSelectionId);

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        public bool GetMainAcivityList(string sID)
        {
            bool bStatus = false;
            string sCommand = "";
            DataTable dtResult;
            string sSelectionId = "";
            try
            {
                Logik process = new Logik();
                if (string.IsNullOrEmpty(sID))
                {
                    //sCommand = "select distinct substring(task_id,1,6) task_id, main_activity from etda_t_project;";
                    //YPT 11.04.2020 get statusview for validate enable data.
                    sCommand = "select distinct main_activity from etda_t_project_period where statusview = 1;";
                }
                else
                {//YPT 11.04.2020 get statusview for validate enable data.
                    //sCommand = "select distinct substring(task_id,1,6) task_id, main_activity from etda_t_project where substring(task_id,1,3) = substring(";
                    sCommand = "select distinct main_activity from etda_t_project_period where statusview = 1 and substring(main_activity,1,4) = substring(";
                    sCommand += "'";
                    sCommand += sID;
                    sCommand += "'";
                    sCommand += ",1,4);";
                }

                dtResult = process.SelectDataByCmd(sCommand);
                if (dtResult != null)
                {
                    string sIndex = "";
                    string sValue = "";
                    ddp_delimain.Items.Clear();

                    foreach (DataRow row in dtResult.Rows)
                    {
                       // sIndex = row["task_id"].ToString();
                        sValue = row["main_activity"].ToString();
                        sIndex = sValue.Substring(0, 6);
                        ddp_delimain.Items.Add(new ListItem(sValue, sIndex));
                    }
                }
                // sSelectionId = ddp_delimain.SelectedValue;
                sSelectionId = ddp_delimain.SelectedItem.Text;
                GetSubAcivityList(sSelectionId);
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        public bool GetSubAcivityList(string sID)
        {
            bool bStatus = false;
            string sCommand = "";
            DataTable dtResult;
            string sSelectionId = "";
            try
            {
                Logik process = new Logik();
                if (string.IsNullOrEmpty(sID))
                {
                    sCommand = "select distinct sub_activity from etda_t_project_period where statusview = 1;";
                }
                else
                {
                    /* sCommand = "select distinct task_id, sub_activity from etda_t_project where statusview = 1 and substring(task_id,1,6) = substring(";
                     sCommand += "'";
                     sCommand += sID;
                     sCommand += "'";
                     sCommand += ",1,6) order by 2;";*/
                    sCommand = "select distinct sub_activity from etda_t_project_period where statusview = 1 and substring(sub_activity,1,6) = substring(";
                    sCommand += "'";
                    sCommand += sID;
                    sCommand += "'";
                    sCommand += ",1,6);";
                }


                dtResult = process.SelectDataByCmd(sCommand);
                if (dtResult != null)
                {
                    string sIndex = "";
                    string sValue = "";

                    ddp_delisub.Items.Clear();

                    foreach (DataRow row in dtResult.Rows)
                    {
                        // sIndex = row["task_id"].ToString();
                        sValue = row["sub_activity"].ToString();
                        sIndex = sValue.Substring(0,8);
                        ddp_delisub.Items.Add(new ListItem(sValue, sIndex));
                    }

                }
                //YPT 12.12.2020
                sSelectionId = ddp_delisub.SelectedValue;
                // sSelectionId = ddp_delisub.SelectedItem.Text;
                GetdelisubAcivityList(sSelectionId);
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        protected void SubmitAppraisalmygridview_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindData(false);
                myGridView.PageIndex = e.NewPageIndex;
                myGridView.DataBind();

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        public void BindData(bool bSearch)
        {
            bool bStatus = false;
            DataTable dtTableInfo = null;
            string sCommand = "";
            string sDateStart = "";
            string sDateEnd = "";
            try
            {
                Logik process = new Logik();
                //process.GetStartAndEndDateView(ref sDateStart, ref sDateEnd);
                //YPT 04.01.2020
                DateTime dtDateS = DateTime.ParseExact(txtDateStart.Text, "dd/MM/yyyy", null);
                DateTime dtDateE = DateTime.ParseExact(txtDateEnd.Text, "dd/MM/yyyy", null);

                sDateStart = dtDateS.ToString("yyyy-MM-dd");
                sDateEnd = dtDateE.ToString("yyyy-MM-dd");

                sCommand = process.GenerateCommandViewTimeSheetPeriod(txt_userid.Text, sDateStart, sDateEnd);

                dtTableInfo = process.SelectDataByCmd(sCommand);
                if (dtTableInfo != null)
                {
                    if (dtTableInfo.Rows.Count > 0)
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.AddRange(new DataColumn[7] { new DataColumn("trans_id"), new DataColumn("work_date"), new DataColumn("project"), new DataColumn("main_activity"), new DataColumn("sub_activity"), new DataColumn("work_hour"), new DataColumn("txt_remark") });

                        foreach (DataRow row in dtTableInfo.Rows)
                        {
                            DateTime dtDatetime = (DateTime)row["work_date"];
                            string sDatetimeS = dtDatetime.ToString("dd-MM-yyyy");
                            string sProject = row["project"].ToString();
                            string sMainAc = row["main_activity"].ToString();
                            string sSubAc = row["sub_activity"].ToString();
                            string sWH = row["work_hour"].ToString();
                            string sRemark = row["txt_remark"].ToString();
                            string sTransID = row["trans_id"].ToString();

                            dt.Rows.Add(sTransID, sDatetimeS, sProject, sMainAc, sSubAc, sWH, sRemark);

                        }

                        myGridView.PageSize = 10;
                        myGridView.AllowPaging = true;
                        myGridView.DataSource = dt;
                        if (bSearch)
                        {
                            myGridView.PageIndex = 0;
                        }
                        myGridView.DataBind();
                        bStatus = true;
                        myGridView.Visible = true;
                        Ck_Export.Visible = true;

                    }
                    else
                    {
                        myGridView.Visible = false;
                        Ck_Export.Checked = false;
                        Ck_Export.Visible = false;
                        imgExcel.Visible = false;

                        ShowMessage("ไม่พบข้อมูล");
                    }
                }
                else
                {
                    myGridView.Visible = false;
                    Ck_Export.Checked = false;
                    Ck_Export.Visible = false;
                    imgExcel.Visible = false;

                    ShowMessage("ไม่พบข้อมูล");
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
      
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "dele")
            {
                Logik process = new Logik();
                process.DeleteData(e.CommandArgument.ToString(),1);
                BindData(false);
            }
        }
        protected void ddpproject_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sSelectionId = "";
            sSelectionId = ddp_mainproject.SelectedValue;
            GetMainAcivityList(sSelectionId);
            lbl_ok.Visible = false;
            lbl_error.Visible = false;
        }
        protected void ddpamin_SelectedIndexChanged(object sender, EventArgs e)
        {

            string sSelectionId = "";
            //sSelectionId = ddp_delimain.SelectedValue;
            sSelectionId = ddp_delimain.SelectedItem.Text;
            GetSubAcivityList(sSelectionId);
            lbl_ok.Visible = false;
            lbl_error.Visible = false;
        }
        public override void VerifyRenderingInServerForm(Control control)
        {

        }
        
        public void AddDataStaffUnderToTable(List<PersonInfoWithUnder> lsPerson, List<PersonInfoWithUnder> lsPersonStaff)
        {
            AddStaffUnder(lsPerson, lsPersonStaff);
        }
        public void AddStaffUnder(List<PersonInfoWithUnder> lsPerson, List<PersonInfoWithUnder> lsPersonStaff)
        {
            try{
                foreach (PersonInfoWithUnder item in lsPerson)
                {
                    AddDataList(item, lsPersonStaff);
                    if (item.arrPersonInfo != null)
                    {
                        if (item.arrPersonInfo.Count > 0)
                        {
                            AddStaffUnder(item.arrPersonInfo, lsPersonStaff);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
           
        }
        public void AddDataList(PersonInfoWithUnder item,List<PersonInfoWithUnder> lsPerson)
        {
            string sId = "";
            try {

                foreach (PersonInfoWithUnder itemDetails in lsPerson)
                {
                    if (String.Equals(item.name, itemDetails.fullNameEN, StringComparison.OrdinalIgnoreCase))
                    {
                        sId = itemDetails.id;
                        item.fullNameTH = itemDetails.fullNameTH;
                        item.id = sId;
                        item.depNameTH = itemDetails.depNameTH;

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            
        }
        
        public void EnableStaffMode()
        {
            if (String.Equals(lbl_PS.Text, "Division Director", StringComparison.OrdinalIgnoreCase))
            {
                //ck_Staff.Visible = true;
                LnK4.Visible = true;
                lbl_end.Visible = true;
            }
            else
            {
                LnK4.Visible = false;
                lbl_end.Visible = false;
                //  ck_Staff.Visible = false;
                //For test
                 /*if (String.Equals(lbl_PS.Text, "Assistant Division Director", StringComparison.OrdinalIgnoreCase))
                 {
                     //ck_Staff.Visible = true;
                     LnK4.Visible = true;

                 }
                 else
                 {
                     // ck_Staff.Visible = false;
                     LnK4.Visible = false;

                 }*/
            }
        }
        public bool ValidateSession()
        {
            bool bStatus = false;
            string sEmail = "";
            string sPassword = "";

            sPassword = Convert.ToString(Session["password"]);
            sEmail = Convert.ToString(Session["email"]);

            if ((string.IsNullOrEmpty(sPassword)) || (string.IsNullOrEmpty(sEmail)))
            {
                Session.Clear();
                Response.Redirect("LoginAttendance.aspx");
            }
            else
            {
                bStatus = true;
            }

            return bStatus;
        }

        protected void btnloadsearch_Click(object sender, EventArgs e)
        {
            lbl_ok.Visible = false;
            lbl_error.Visible = false;
            
            BindData(true);
        }

        protected void imgExcel_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ExportGridToExcel();

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        private void ExportGridToExcel()
        {
            try
            {
                string sDateStart = "";
                string sDateEnd = "";
                string sFileName = "";
                DataTable dt = null;

                DateTime dtDateS = DateTime.ParseExact(txtDateStart.Text, "dd/MM/yyyy", null);
                DateTime dtDateE = DateTime.ParseExact(txtDateEnd.Text, "dd/MM/yyyy", null);
                sDateStart = dtDateS.ToString("yyyy-MM-dd");
                sDateEnd = dtDateE.ToString("yyyy-MM-dd");

                Logik process = new Logik();
                BindData(true);

                if (myGridView.Visible)
                {
                    process.ExportGridToExcel(sDateStart, sDateEnd, myGridView, 2, ref dt, ref sFileName);
                }
               
                if (dt != null)
                {
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(dt);

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", sFileName);
                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        protected void Ck_Export_CheckedChanged(object sender, EventArgs e)
        {
            if (Ck_Export.Checked)
            {
                imgExcel.Visible = true;
            }
            else
            {
                imgExcel.Visible = false;
            }

        }
        public void ShowMessage(string sText)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("window.onload=function(){");
            sb.Append("alert('");
            sb.Append(sText);
            sb.Append("')};");
            sb.Append("</script>");
            ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
        }

        protected void ddp_status_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (String.Equals(ddp_status.SelectedValue, "Done", StringComparison.OrdinalIgnoreCase))
            { 
                lbl_result.Visible = true;
                txt_linkresult.Visible = true;
                lbl_result.ForeColor = System.Drawing.Color.Black;
            }
            else if (String.Equals(ddp_status.SelectedValue, "Doing", StringComparison.OrdinalIgnoreCase))
            {
                if (String.Equals(lbl_hiddenStatus.Text, "Done", StringComparison.OrdinalIgnoreCase))
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "callJSFunction", "getValue();", true);

                }

                //lbl_duedate.Visible = false;
               // lbl_datedone.Visible = false;
                lbl_result.Visible = false;
                txt_linkresult.Visible = false;
            }
            else {
                //lbl_duedate.Visible = false;
               // lbl_datedone.Visible = false;
                lbl_result.Visible = false;
                txt_linkresult.Visible = false;
            }
            lbl_ok.Visible = false;
            lbl_error.Visible = false;
        }

        //YPT 12.12.2020
        public bool GetdelisubAcivityList(string sID)
        {
            bool bStatus = false;
            string sCommand = "";
            DataTable dtResult;
            try
            {
                Logik process = new Logik();
                if (string.IsNullOrEmpty(sID))
                {
                    sCommand = "select distinct sub_subactivity from etda_t_project_period where statusview = 1;";
                }
                else
                {
                    sCommand = "select distinct sub_subactivity from etda_t_project_period where statusview = 1 and substring(sub_subactivity,1,8) =  substring(";
                    sCommand += "'";
                    sCommand += sID;
                    sCommand += "'";
                    sCommand += ",1,8);";
                }


                dtResult = process.SelectDataByCmd(sCommand);
                if (dtResult != null)
                {
                    string sIndex = "";
                    string sValue = "";

                    ddp_subact.Items.Clear();

                    foreach (DataRow row in dtResult.Rows)
                    {
                        //sIndex = row["task_id"].ToString();
                        sValue = row["sub_subactivity"].ToString();

                        sIndex = sValue.Substring(0, 12);
                        ddp_subact.Items.Add(new ListItem(sValue, sIndex));
                    }
                    if (ddp_subact.Items.Count > 0)
                    {
                        ddp_subact.SelectedIndex = 0;
                        //Check status of sub activity
                        GetStatusAct(ddp_subact.SelectedItem.Text);
                    }
                   
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }

        protected void ddp_delisub_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sSelectionId = "";
             sSelectionId = ddp_delisub.SelectedValue;
            //sSelectionId = ddp_delisub.SelectedItem.Text;
            GetdelisubAcivityList(sSelectionId);
            lbl_ok.Visible = false;
            lbl_error.Visible = false;
        }
        protected bool ValidateLink()
        {
            bool bValid = true;
            string sMessage = "";

            if (String.Equals(ddp_status.SelectedValue, "Done", StringComparison.OrdinalIgnoreCase))
            {
                if (txt_linkresult.Text.Length == 0)
                {
                    // System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "alert('กรุณากรอกข้อมูลผลงานที่ได้รับ');", true);
                    sMessage = "กรุณากรอกข้อมูลผลงานที่ได้รับ...";
                    lbl_error.Text = sMessage;
                    lbl_ok.Visible = false;
                    lbl_error.Visible = true;
                    lbl_result.ForeColor = System.Drawing.Color.Red;
                    bValid = false;
                }

            }
            else if (String.Equals(ddp_status.SelectedValue, "Done", StringComparison.OrdinalIgnoreCase))
            {
                System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "callJSFunction", "getValue();", true);

            }
            return bValid;
        }
        protected bool UpdateTimeSheet(string sEmpId,string sTaskId)
        {
            string sWorkDate = "";
            string sWorkHour = "";
            string sTxtRemark = "";
            string sUpdUsreId = "";
            string sMessage = "";
            bool bStatus = false;

            DateTime dtDate = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null);

            sWorkDate = dtDate.ToString("yyyy-MM-dd");
            sWorkHour = ddp_hours.SelectedValue;
            sTxtRemark = txt_additional.Text;
            sUpdUsreId = txt_userid.Text;

            Logik process = new Logik();
            bStatus = process.InsertData(sEmpId, sTaskId, sWorkDate, sWorkHour, sTxtRemark, sUpdUsreId,1);
           
            return bStatus;
        }
        protected bool UpdateProjectStatus(string sEmpId, string sTaskId)
        {
            bool bValid = true;
          
            string sMessage = "";
            string sUpdUsreId = "";
            bool bStatus = false;
          

            sUpdUsreId = txt_userid.Text;


            if (String.Equals(ddp_status.SelectedValue, "Done", StringComparison.OrdinalIgnoreCase))
            {
                bValid = UpdateStatusWithDate(sUpdUsreId, sTaskId);
            }
            else if (String.Equals(ddp_status.SelectedValue, "Doing", StringComparison.OrdinalIgnoreCase))
            {
                if (String.Equals(lbl_hiddenStatus.Text, "Done", StringComparison.OrdinalIgnoreCase))
                {
                    bValid = UpdateStatusWithReOpen(sUpdUsreId, sTaskId, hiddenReason.Value);
                    txt_linkresult.Text = "";
                }
                else {
                    bValid = UpdateStatus(sUpdUsreId, sTaskId);
                }
               
            }
            else {
                bValid = UpdateStatus(sUpdUsreId, sTaskId);
            }

            return bValid;
        }
        public bool GetStatusAct(string sSubAct)
        {
            bool bStatus = false;
            string sCommand = "";
            DataTable dtResult;
            string csTaskId = "";

            string sDueDate = "";
            string sLinkResult = "";

            try
            {
                Logik process = new Logik();

                string sValue = "";

                //Get taskid
                csTaskId = GetTaskID(sSubAct);
                if (!string.IsNullOrEmpty(csTaskId))
                {
                    sCommand = "select distinct a.task_id, a.sub_subactivity,description,DATE_FORMAT(a.duedate,'%d/%m/%Y') duedate,resultlink from etda_t_project_period a LEFT JOIN etda_t_subact_status b ON(a.status_id = b.status_id) where statusview = 1 and a.task_id=";
                    sCommand += "'";
                    sCommand += csTaskId;
                    sCommand += "';";

                    dtResult = process.SelectDataByCmd(sCommand);
                    if (dtResult != null)
                    {
                        foreach (DataRow row in dtResult.Rows)
                        {
                            sValue = row["description"].ToString();

                            sDueDate = row["duedate"].ToString();

                            sLinkResult = row["resultlink"].ToString();

                            if (!String.IsNullOrEmpty(sValue))
                            {
                                ddp_status.SelectedValue = sValue;
                            }
                           
                        }
                    }
                    if (String.IsNullOrEmpty(sValue))
                    {
                        ddp_status.SelectedIndex = 0;
                    }

                    lbl_hiddenStatus.Text = ddp_status.SelectedValue;

                    //set duedate
                    SetDuedate(sDueDate, process);

                    if (String.Equals(ddp_status.SelectedValue, "Done", StringComparison.OrdinalIgnoreCase))
                    {
                        lbl_result.Visible = true;
                        txt_linkresult.Visible = true;
                        txt_linkresult.Text = sLinkResult;
                    }
                    else {
                        lbl_result.Visible = false;
                        txt_linkresult.Visible = false;
                        txt_linkresult.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        public string GetTaskID(string sSubAct)
        {
            string sTaskId = "";
            string sCommand = "";
            DataTable dtResult;
            try
            {
                Logik process = new Logik();
               
                sCommand = "select distinct task_id from etda_t_project_period where statusview = 1 and sub_subactivity=";
                sCommand += "'";
                sCommand += sSubAct;
                sCommand += "';";


                dtResult = process.SelectDataByCmd(sCommand);
                if (dtResult != null)
                {
                    foreach (DataRow row in dtResult.Rows)
                    {
                        sTaskId = row["task_id"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return sTaskId;
        }

        protected void ddp_subact_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetStatusAct(ddp_subact.SelectedItem.Text);
            lbl_ok.Visible = false;
            lbl_error.Visible = false;
        }
        public bool UpdateStatus(string sEmpId,string sTaskId)
        {
            bool bStatus = false;
            string sCommand = "";
            DataTable dtResult;
            string sReturnId = "";

            try
            {
                Logik process = new Logik();

                string sValue = "";
                if (!string.IsNullOrEmpty(sTaskId))
                {
                    sReturnId = process.InsertProjectStatus(sEmpId,sTaskId, ddp_status.SelectedValue);

                    if (!string.IsNullOrEmpty(sReturnId))
                    {
                        //Update status_id to etda_t_project
                        bStatus = process.UpdateStausId(sTaskId, sReturnId);
                    }
                 }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        public bool UpdateStatusWithDate(string sEmpId, string sTaskId)
        {
            bool bStatus = false;
            string sCommand = "";
            DataTable dtResult;
            string sReturnId = "";

            try
            {
                Logik process = new Logik();

                string sValue = "";
                if (!string.IsNullOrEmpty(sTaskId))
                {
                    string sDueDate = "";
                    DateTime dtDate = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null);

                    sDueDate = dtDate.ToString("yyyy-MM-dd");

                    sReturnId = process.InsertProjectStatusWithDate(sEmpId, sTaskId, ddp_status.SelectedValue, sDueDate,txt_linkresult.Text);

                    if (!string.IsNullOrEmpty(sReturnId))
                    {
                        //Update status_id to etda_t_project
                        bStatus = process.UpdateStausId(sTaskId, sReturnId);
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        public bool UpdateStatusWithReOpen(string sEmpId, string sTaskId,string sReason)
        {
            bool bStatus = false;
            string sCommand = "";
            DataTable dtResult;
            string sReturnId = "";

            try
            {
                Logik process = new Logik();

                string sValue = "";
                if (!string.IsNullOrEmpty(sTaskId))
                {
                    string sReOpenDate = "";
                    DateTime dtDate = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null);

                    sReOpenDate = dtDate.ToString("yyyy-MM-dd");

                    sReturnId = process.InsertProjectStatusWithReOpen(sEmpId, sTaskId, ddp_status.SelectedValue, sReOpenDate, sReason);

                    if (!string.IsNullOrEmpty(sReturnId))
                    {
                        //Update status_id to etda_t_project
                        bStatus = process.UpdateStausId(sTaskId, sReturnId);
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        public bool SetDuedate(string sDate,Logik process)
        {
            bool bStatus = false;

            try
            {
                if (String.IsNullOrEmpty(sDate))
                {
                    sDate = "-";
                    lbl_datedone.ForeColor = System.Drawing.Color.Black;
                }
                else {

                    DateTime dtdate = DateTime.ParseExact(sDate, "dd/MM/yyyy", null);

                    bStatus = process.ValidateDateTime(DateTime.Today, dtdate);
                    if (bStatus)
                    {
                        lbl_datedone.ForeColor = System.Drawing.Color.Black;
                    }
                    else
                    {
                        lbl_datedone.ForeColor = System.Drawing.Color.Red;
                    }
                }
                lbl_datedone.Text = sDate;
                lbl_result.Visible = true;

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
    }
}