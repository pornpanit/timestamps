﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="Management.aspx.cs" Inherits="WebTimeStamps.Manageuser" EnableEventValidation="false"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link href="assets/css/Gridstyle.css" rel="stylesheet" type="text/css" />

    <link type="text/css" href="assets/css/ui-lightness/jquery-ui-1.8.19.custom.css" rel="stylesheet" />
    <link type="text/css" href="assets/css/main.css" rel="stylesheet" />
    <script type="text/javascript" src="Scripts/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.8.19.custom.min.js"></script>
     <style type="text/css">
        .modal
        {
            position: fixed;
            top: 0;
            left: 0;
            background-color: black;
            z-index: 99;
            opacity: 0.8;
            filter: alpha(opacity=80);
            -moz-opacity: 0.8;
            min-height: 100%;
            width: 100%;
        }
        .loading
        {
            font-family: Arial;
            font-size: 10pt;
            border: 5px solid #67CFF5;
            width: 200px;
            height: 100px;
            display: none;
            position: fixed;
            background-color: White;
            z-index: 999;
        }
    </style>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("modal");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }
        $('form').live("submit", function () {
            ShowProgress();
        });
    </script>
</head>
<body>
    <form id="form1" runat="server" class="center">
        <br/>
             <table class="auto-style3">
                 <tr>
                     <td align="right" class="auto-style6">
                         <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                     </td>
                     <td class="auto-style2" align="right">
                         <asp:LinkButton id="lk_logout" runat="server" OnClick="Onlogout">ออกจากระบบ</asp:LinkButton>
                     </td>
                 </tr>
                 <tr>
                     <td align="right" class="auto-style5" align="right">
                         <asp:Label ID="lbl_name" runat="server" Text="Label" Visible="False"></asp:Label>
                         <asp:Label ID="lbl_surname" runat="server" Text="Label" Visible="False"></asp:Label>
                     </td>
                     <td class="auto-style2" align="right">
                         
                     </td>
                 </tr>
                 <tr>
                     <td align="right" class="auto-style5" align="right">
                         <asp:Label ID="lbl_PS" runat="server" Visible="False"></asp:Label>
                     </td>
                     <td class="auto-style2" align="right">
                         <asp:Label ID="lb_username_surname" runat="server" Text="Label"></asp:Label>
                     </td>
                 </tr>
             </table>
        <br/>
         <table class="auto-style3">
                <tr>
                     <td class="auto-style2" align="Left">
                         |&nbsp;&nbsp;<asp:LinkButton id="LnK1" runat="server" OnClick="OnTimeAtt">Time Attendance</asp:LinkButton>&nbsp;&nbsp;|&nbsp;&nbsp;<asp:LinkButton id="LnK2" runat="server" OnClick="OnTimeSheet">Timesheet</asp:LinkButton>&nbsp;&nbsp;|&nbsp;&nbsp;<asp:LinkButton id="LnK6" runat="server" OnClick="OnTimeSheetPeriod">Timesheet period</asp:LinkButton>&nbsp;&nbsp;|&nbsp;&nbsp;<asp:LinkButton id="LnK3" runat="server" OnClick="OnReport">Report</asp:LinkButton>&nbsp;&nbsp;|&nbsp;&nbsp;<asp:Label ID="Label3" runat="server" Text="ข้อมูลของผู้ใต้บังคับบัญชา"></asp:Label>&nbsp;&nbsp;<asp:Label ID="lbl_end" runat="server" Text="|"/>
                     </td>
                 </tr>
             </table>
             <br/>
         <div class="centerwithborder">
            <br/>
            <table align="center">
                <tr>
                       <td class="auto-style1" align="right">
                            <div>
                                <asp:label ID="lbl_userid" Text="รหัสพนักงาน : " AutoCompleteType="Disabled" runat="server" Visible="False"/>
                            </div>
                        </td>
                        <td class="auto-style1">
                            <asp:TextBox ID="txt_userid" runat="server" Visible="False"></asp:TextBox>
                        </td>
                    </tr>
                <tr>
                    <td class="auto-style1"  align="right">
                        <div>
                            <asp:label ID="lbl_DatStart" Text="ตั้งแต่วันที่ : " AutoCompleteType="Disabled" align="right" runat="server"/>
                        </div>
                    </td>
                    <td class="auto-style1">
                        <div>
                        <asp:TextBox ID="txtDateStart" AutoCompleteType="Disabled" runat="server"/>
                        </div>
                    </td>
                    <td class="auto-style1-small"  align="center">
                        <div>
                            <asp:label ID="Label1" Text="ถึง" AutoCompleteType="Disabled" align="right" runat="server"/>
                        </div>
                    </td>
                    <td class="auto-style1">
                        <div>
                            <asp:TextBox ID="txtDateEnd"  AutoCompleteType="Disabled" runat="server"/>
                        </div>
                    </td>
                </tr>
                    <tr>
                        <td class="auto-style1">
                        </td>
                        <td class="auto-style1">
                           
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style1">
                            
                        </td>
                        <td class="auto-style1">
                            <asp:Button class="buttonRounded" ID="btn_seach" runat="server" Text="ค้นหา" OnClick="btn_seach_Click" />
                            <asp:Label ID="lbl_err" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                        </td>
                     </tr>
            </table>
             <br/>
        </div>
        <br/>
        <div>
            <table align="center" class="center">
               
                <tr>
                     <td class="auto-style12" align="left">
                         &nbsp;
                         <asp:RadioButton ID="rdb_update" runat="server" GroupName="typejob" Text="Update" AutoPostBack="True" OnCheckedChanged="rdb_update_CheckedChanged" />
                         &nbsp;&nbsp;&nbsp;&nbsp;
                         <asp:RadioButton ID="rdb_delete" runat="server" GroupName="typejob" Text="Delete" AutoPostBack="True" OnCheckedChanged="rdb_delete_CheckedChanged" />
                         </td>
                </tr>
                 <tr>
                     <td class="auto-style12" align="Left">
                         &nbsp;
                         &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                            
                    </td>
                </tr>
                 <tr>
                     <td class="auto-style12" align="Left">
                        
                            
                         <asp:Label ID="Label4" runat="server" Text="รหัสพนักงาน :"></asp:Label>
&nbsp;
                        
                            
                    </td>
                </tr>
                <tr>
                      <td class="auto-style11">
                           &nbsp;</td>
                </tr>
                <tr>
                      <td class="center">
                           &nbsp;</td>
                </tr>
                </table>
		</div>
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="./img/loader.gif" alt="" />
        </div>
    </form>
</body>
</html>
