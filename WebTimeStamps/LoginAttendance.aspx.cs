﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace WebTimeStamps
{
    public partial class LoginTimeSheet : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void submit_Click(object sender, EventArgs e)
        {
            try
            {
                bool bStatus = false;
                string sUserName = "";
                string sPassword = "";

                string sInputjSon = "";
                sUserName = txt_user.Text;
                sPassword = txt_pwd.Text;

                Logik process = new Logik();
                sInputjSon = process.generateInput(sUserName, sPassword);
                bStatus = process.CallADServiceValidateUser(sInputjSon);
                if (bStatus)
                {
                    //lb_error.Visible = false;
                    Session["user"] = sUserName;
                    Session["password"] = sPassword;
                    Session["email"] = sUserName;
                    Session["samaccountname"] = process.m_personInfo.samaccountname;
                    Session["name"] = process.m_personInfo.name;
                    Session["title"] = process.m_personInfo.title;

                    Response.Redirect("TimeAtt.aspx");
                }
                else
                {
                    //lb_error.Text = "Please check your user and password!";
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);
                //lbl_error.Text = ex.Message;
            }

        }
        
    }
}