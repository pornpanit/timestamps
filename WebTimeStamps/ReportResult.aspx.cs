﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebTimeStamps
{
    public partial class ReportResult : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sStartDate = "";
            string sEndDate = "";
            string sUserId = "";
            string sTitle = "";
            string sUsernamesurenameth = "";

            sUserId = Convert.ToString(Session["userid"]);
            sTitle = Convert.ToString(Session["title"]);

            sUsernamesurenameth = Convert.ToString(Session["usernamesurenameth"]);
            lb_username_surname.Text = sUsernamesurenameth;

            txt_userid.Text = sUserId;
            lbl_PS.Text = sTitle;

            if (!this.IsPostBack)
            {
                if (ValidateSession())
                {
                    Logik process = new Logik();
                    process.GetStartAndEndDate(ref sStartDate, ref sEndDate);
                    txtDateStart.Text = sStartDate;
                    txtDateEnd.Text = sEndDate;

                    txtDateStart.Text = sStartDate;
                    txtDateEnd.Text = sEndDate;
                    txt_userid.Text = sUserId;

                    DateTime dtCurrent = DateTime.Now;
                    txtDateStart.Text = dtCurrent.ToString("dd/MM/yyyy");
                    txtDateEnd.Text = dtCurrent.ToString("dd/MM/yyyy");

                    BindData();
                    EnableStaffMode();
                }
            }
        }
        protected void SubmitAppraisalGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindData();
                GridViewReport.PageIndex = e.NewPageIndex;
                GridViewReport.DataBind();
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        private void BindData()
        {
            string sStartDate = "";
            string sEndDate = "";
            string sUserId = "";
            try
            {

                DateTime dtStart = DateTime.ParseExact(txtDateStart.Text, "dd/MM/yyyy", null);
                DateTime dtEnd = DateTime.ParseExact(txtDateEnd.Text, "dd/MM/yyyy", null);
                sStartDate = dtStart.ToString("yyyy-MM-dd");
                sEndDate = dtEnd.ToString("yyyy-MM-dd");
                sUserId = txt_userid.Text;

                Logik process = new Logik();
                DataTable dtTable = process.SelectReportResult(sUserId, sStartDate, sEndDate);
                if (dtTable != null)
                {
                    if (dtTable.Rows.Count > 0)
                    {
                        GridViewReport.PageSize = 20; //limit items
                        GridViewReport.AllowPaging = true;
                        GridViewReport.PageIndex = 0;
                        GridViewReport.DataSource = dtTable;
                        GridViewReport.DataBind();
                        GridViewReport.Visible = true;
                        Ck_Export.Visible = true;
                    }
                    else
                    {
                        GridViewReport.Visible = false;
                        Ck_Export.Checked = false;
                        Ck_Export.Visible = false;
                        imgExcel.Visible = false;

                        ShowMessage("ไม่พบข้อมูล");
                    }

                }
                else
                {
                    GridViewReport.Visible = false;
                    Ck_Export.Checked = false;
                    Ck_Export.Visible = false;
                    imgExcel.Visible = false;

                    ShowMessage("ไม่พบข้อมูล");
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }


        }
        public void OnTimeAtt(object sender, EventArgs e)
        {
            Response.Redirect("TimeAtt.aspx");
        }

        public void OnTimeSheet(object sender, EventArgs e)
        {
            Response.Redirect("TimeSheets.aspx");
        }
        public void OnTimeSheetPeriod(object sender, EventArgs e)
        {
            Response.Redirect("TimeSheetsPeriod.aspx");
        }
        public void OnReport(object sender, EventArgs e)
        {
            Response.Redirect("ReportViewer.aspx");
        }
        public void OnReportResult(object sender, EventArgs e)
        {
            Response.Redirect("ReportResult.aspx");
        }
        public void Onlogout(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("LoginAttendance.aspx");
        }
        public void OnStaffUnder(object sender, EventArgs e)
        {
            Session["parentpage"] = "ReportViewer.aspx";
            Response.Redirect("StaffViewer.aspx");
        }

        protected void imgExcel_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ExportGridToExcel();

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        private void ExportGridToExcel()
        {
            try
            {
                string sDateStart = "";
                string sDateEnd = "";
                string sFileName = "";
                DataTable dt = null;

                DateTime dtDateS = DateTime.ParseExact(txtDateStart.Text, "dd/MM/yyyy", null);
                DateTime dtDateE = DateTime.ParseExact(txtDateEnd.Text, "dd/MM/yyyy", null);
                sDateStart = dtDateS.ToString("yyyy-MM-dd");
                sDateEnd = dtDateE.ToString("yyyy-MM-dd");

                Logik process = new Logik();
                BindData();

                if (GridViewReport.Visible)
                {
                    process.ExportGridToExcel(sDateStart, sDateEnd, GridViewReport, 1, ref dt, ref sFileName);
                }
                
                if (dt != null)
                {
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(dt);

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", sFileName);
                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        protected void btnload_Click(object sender, EventArgs e)
        {
            try
            {
                BindData();

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        protected void Ck_Export_CheckedChanged(object sender, EventArgs e)
        {
            if (Ck_Export.Checked)
            {
                imgExcel.Visible = true;
            }
            else
            {
                imgExcel.Visible = false;
            }

        }
        public bool ValidateSession()
        {
            bool bStatus = false;
            string sEmail = "";
            string sPassword = "";

            sPassword = Convert.ToString(Session["password"]);
            sEmail = Convert.ToString(Session["email"]);

            if ((string.IsNullOrEmpty(sPassword)) || (string.IsNullOrEmpty(sEmail)))
            {
                Session.Clear();
                Response.Redirect("LoginAttendance.aspx");
            }
            else
            {
                bStatus = true;
            }

            return bStatus;
        }
        public void EnableStaffMode()
        {
            if (String.Equals(lbl_PS.Text, "Division Director", StringComparison.OrdinalIgnoreCase))
            {
                //ck_Staff.Visible = true;
                LnK4.Visible = true;
                lbl_end.Visible = true;
            }
            else
            {
                LnK4.Visible = false;
                lbl_end.Visible = false;
                //  ck_Staff.Visible = false;
                //For test
                /*if (String.Equals(lbl_PS.Text, "Assistant Division Director", StringComparison.OrdinalIgnoreCase))
                {
                    //ck_Staff.Visible = true;
                    LnK4.Visible = true;

                }
                else
                {
                    // ck_Staff.Visible = false;
                    LnK4.Visible = false;

                }*/
            }
        }
        public void ShowMessage(string sText)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("window.onload=function(){");
            sb.Append("alert('");
            sb.Append(sText);
            sb.Append("')};");
            sb.Append("</script>");
            ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
        }

    }
}