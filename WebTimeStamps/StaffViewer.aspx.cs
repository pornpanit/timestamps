﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML.Excel;
using MySqlDatabase;

namespace WebTimeStamps
{
    public partial class StaffViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                try
                {
                    // + Convert.ToString(Session["password"]);
                    bool bStatus = false;

                    string sUsernamesurenameth = "";
                    string sUserId = "";

                    if (ValidateSession())
                    {
                        sUsernamesurenameth = Convert.ToString(Session["usernamesurenameth"]);
                        lb_username_surname.Text = sUsernamesurenameth;

                        sUserId = Convert.ToString(Session["userid"]);
                        txt_userid.Text = sUserId;

                        string script = "$(document).ready(function () { $('[id*=btn_load]').click(); });";
                        ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);

                    }

                }
                catch (Exception ex)
                {
                    // MessageBox.Show(ex.Message);

                }

            }
        }
        public bool ValidateSession()
        {
            bool bStatus = false;
            string sEmail = "";
            string sPassword = "";

            sPassword = Convert.ToString(Session["password"]);
            sEmail = Convert.ToString(Session["email"]);

            if ((string.IsNullOrEmpty(sPassword)) || (string.IsNullOrEmpty(sEmail)))
            {
                Session.Clear();
                Response.Redirect("LoginAttendance.aspx");
            }
            else
            {
                bStatus = true;
            }

            return bStatus;
        }

        protected void btnload_Click(object sender, EventArgs e)
        {
            try
            {
                if (dpp_Staff.Items.Count == 0)
                {
                    bool bStatus = false;

                    string sUserName = "";
                    string sSamaccountname = "";
                    string sName = "";
                    string sEmail = "";
                    string sPassword = "";
                    string sTitle = "";

                    sUserName = Convert.ToString(Session["user"]);
                    sPassword = Convert.ToString(Session["password"]);
                    sSamaccountname = Convert.ToString(Session["samaccountname"]);
                    sName = Convert.ToString(Session["name"]);
                    sEmail = Convert.ToString(Session["email"]);
                    sTitle = Convert.ToString(Session["title"]);

                    lbl_PS.Text = sTitle;

                    Logik process = new Logik();
                    bStatus = process.CallUserInfo(sName);
                    if (bStatus)
                    {
                        GetStaffUnder(ref process, sEmail, sPassword);
                    }

                    DateTime dtCurrent = DateTime.Now;
                    txtDateStart.Text = dtCurrent.ToString("dd/MM/yyyy");
                    txtDateEnd.Text = dtCurrent.ToString("dd/MM/yyyy");

                    rdb_time.Checked = true;

                }
                if (dpp_Staff.Items.Count > 0)
                {
                    if (rdb_time.Checked)
                    {
                        BindGridStaffUnder();
                    }
                    else
                    {
                        BindDataTimeSheet();
                    }
                }
                else
                {
                    string sParentpage = "";
                    sParentpage = Convert.ToString(Session["parentpage"]);

                    string redirectScript = "window.location.href = '" + sParentpage + "';";
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "alert('ไม่พบข้อมูลผู้ใต้บังคับบัญชา');" + redirectScript, true);
                }
               
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        public void Onlogout(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("LoginAttendance.aspx");
        }
        public void OnTimeAtt(object sender, EventArgs e)
        {
            Response.Redirect("TimeAtt.aspx");
        }

        public void OnTimeSheet(object sender, EventArgs e)
        {
            Response.Redirect("TimeSheets.aspx");
        }
        public void OnTimeSheetPeriod(object sender, EventArgs e)
        {
            Response.Redirect("TimeSheetsPeriod.aspx");
        }
        public void OnReport(object sender, EventArgs e)
        {
            Response.Redirect("ReportViewer.aspx");
        }
        public void OnReportResult(object sender, EventArgs e)
        {
            Response.Redirect("ReportResult.aspx");
        }
        protected void ck_all_CheckedChanged(object sender, EventArgs e)
        {
            if (ck_all.Checked)
            {
                GridViewStaff.Visible = false;
            }
            else
            {
                GridViewStaff.Visible = true;
                BindStaffInfo();
            }
        }
        public void BindStaffInfo()
        {
            bool bStatus = false;
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[2] { new DataColumn("userid"), new DataColumn("username") });

                foreach (ListItem item in dpp_Staff.Items)
                {
                    string sUserid = item.Value;
                    string sUserName = item.Text;

                    dt.Rows.Add(sUserid, sUserName);

                }
                GridViewStaff.PageSize = 10;
                GridViewStaff.AllowPaging = true;
                GridViewStaff.DataSource = dt;
                GridViewStaff.PageIndex = 0;
                GridViewStaff.DataBind();
                bStatus = true;

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        public void BindDataTimeSheet()
        {
            bool bStatus = false;
            DataTable dtTableInfo = null;
            string sCommand = "";
            try
            {
                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();

                string sDateStart = "";
                string sDateEnd = "";
                DateTime dtDateS = DateTime.ParseExact(txtDateStart.Text, "dd/MM/yyyy", null);
                DateTime dtDateE = DateTime.ParseExact(txtDateEnd.Text, "dd/MM/yyyy", null);

                sDateStart = dtDateS.ToString("yyyy-MM-dd");
                sDateEnd = dtDateE.ToString("yyyy-MM-dd");

                Logik process = new Logik();
                dtTableInfo = process.ExportDatafromDatabase(txt_userid.Text, sDateStart, sDateEnd, true, dpp_Staff.Items);

                if (dtTableInfo != null)
                {
                    if (dtTableInfo.Rows.Count > 0)
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.AddRange(new DataColumn[7] { new DataColumn("work_date"), new DataColumn("username"), new DataColumn("project"), new DataColumn("main_activity"), new DataColumn("sub_activity"), new DataColumn("work_hour"), new DataColumn("txt_remark") });

                        foreach (DataRow row in dtTableInfo.Rows)
                        {
                            DateTime dtDatetime = (DateTime)row["work_date"];
                            string sDatetimeS = dtDatetime.ToString("dd-MM-yyyy");
                            string sProject = row["project"].ToString();
                            string sMainAc = row["main_activity"].ToString();
                            string sSubAc = row["sub_activity"].ToString();
                            string sWH = row["work_hour"].ToString();
                            string sRemark = row["txt_remark"].ToString();
                            string sUserID = row["userid"].ToString();
                            string sUserName = "";
                            sUserName = GetName(sUserID);

                            dt.Rows.Add(sDatetimeS, sUserName, sProject, sMainAc, sSubAc, sWH, sRemark);

                        }

                        GridViewStaffTimeSheet.PageSize = 10;
                        GridViewStaffTimeSheet.AllowPaging = true;
                        GridViewStaffTimeSheet.DataSource = dt;
                        GridViewStaffTimeSheet.PageIndex = 0;
                        GridViewStaffTimeSheet.DataBind();
                        bStatus = true;
                        GridViewStaffTimeSheet.Visible = true;
                        Ck_Export.Visible = true;

                    }
                    else
                    {
                        GridViewStaffTimeSheet.Visible = false;
                        //lbl_errexport.Visible = true;
                        //lbl_errexport.Text = "ไม่พบข้อมูล";
                        Ck_Export.Checked = false;
                        Ck_Export.Visible = false;
                        imgExcel.Visible = false;

                        ShowMessage("ไม่พบข้อมูล");
                    }
                }
                else
                {
                    GridViewStaffTimeSheet.Visible = false;
                    //lbl_errexport.Visible = true;
                    //lbl_errexport.Text = "ไม่พบข้อมูล";
                    Ck_Export.Checked = false;
                    Ck_Export.Visible = false;
                    imgExcel.Visible = false;

                    ShowMessage("ไม่พบข้อมูล");
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        protected void SubmitAppraisalGridStaff_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindStaffInfo();
               
                GridViewStaff.PageIndex = e.NewPageIndex;
                GridViewStaff.DataBind();
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        protected void SubmitAppraisalGridStaffUnder_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindGridStaffUnder();
                GridViewStaffUnder.PageIndex = e.NewPageIndex;
                GridViewStaffUnder.DataBind();
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        protected void SubmitAppraisalGridStaffTimeSheet_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindDataTimeSheet();
                GridViewStaffTimeSheet.PageIndex = e.NewPageIndex;
                GridViewStaffTimeSheet.DataBind();
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        protected void OnRowDataBoundStaffUnders(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                try
                {
                    bool bStatusStart = false;
                    bool bStatusWorking = false;
                    if ((!string.IsNullOrEmpty(e.Row.Cells[4].Text)) && (!String.Equals(e.Row.Cells[4].Text, "-", StringComparison.OrdinalIgnoreCase)))
                    {
                        DateTime dtStart = DateTime.Parse(e.Row.Cells[4].Text);
                        bStatusStart = ValidateEntryTime(dtStart);
                        DateTime dtHours = DateTime.Parse(e.Row.Cells[6].Text);
                        bStatusWorking = ValidateWorkingHours(dtHours);
                    }
                    if (!String.Equals(e.Row.Cells[4].Text, "-", StringComparison.OrdinalIgnoreCase))
                    {
                        if (!bStatusStart)
                        {
                            e.Row.Cells[4].ForeColor = System.Drawing.Color.Red;
                        }
                        if (bStatusWorking)
                        {
                            if (!bStatusStart)
                            {
                                e.Row.Cells[6].ForeColor = System.Drawing.Color.Red;
                            }
                            else
                            {
                                e.Row.Cells[6].ForeColor = System.Drawing.Color.Green;
                            }

                        }
                        else
                        {
                            e.Row.Cells[6].ForeColor = System.Drawing.Color.Red;
                        }
                    }
                    if ((!string.IsNullOrEmpty(e.Row.Cells[7].Text)) && (!String.Equals(e.Row.Cells[7].Text, "-", StringComparison.OrdinalIgnoreCase)))
                    {
                        DateTime dtStart = DateTime.Parse(e.Row.Cells[7].Text);
                        bStatusStart = ValidateEntryTime(dtStart);
                        DateTime dtHours = DateTime.Parse(e.Row.Cells[9].Text);
                        bStatusWorking = ValidateWorkingHours(dtHours);
                    }
                    if (!String.Equals(e.Row.Cells[7].Text, "-", StringComparison.OrdinalIgnoreCase))
                    {
                        if (!bStatusStart)
                        {
                            e.Row.Cells[7].ForeColor = System.Drawing.Color.Red;
                        }
                        if (bStatusWorking)
                        {
                            if (!bStatusStart)
                            {
                                e.Row.Cells[9].ForeColor = System.Drawing.Color.Red;
                            }
                            else
                            {
                                e.Row.Cells[9].ForeColor = System.Drawing.Color.Green;
                            }

                        }
                        else
                        {
                            e.Row.Cells[9].ForeColor = System.Drawing.Color.Red;
                        }
                    }

                    // e.Row.Cells[7].Attributes["ondblclick"] = Page.ClientScript.GetPostBackClientHyperlink(myGridView, "Edit$" + e.Row.RowIndex);
                    //e.Row.Cells[7].Attributes["style"] = "cursor:pointer";
                }
                catch (Exception ex)
                {
                    // MessageBox.Show(ex.Message);

                }
            }

        }
        private void BindGridStaffUnder()
        {
            bool bStatus = false;

            bool bFoundData = false;
            DateTime dtStart = DateTime.Now;
            DateTime dtEnd = DateTime.Now;
            string sStartDate = "";
            string sEndDate = "";
            string sCommand = "";

            try
            {
                sStartDate = txtDateStart.Text;
                sEndDate = txtDateEnd.Text;
                dtStart = DateTime.ParseExact(sStartDate, "dd/MM/yyyy", null);
                dtEnd = DateTime.ParseExact(sEndDate, "dd/MM/yyyy", null);

                if ((dtStart != null) && (dtEnd != null))
                {
                    //lbl_errorlist.Text = "";
                    //lbl_errorlist.Visible = false;
                    DataTable dtTableInfo = null;
                    string sSelectStaffUnder = "";
                    //if (ck_all.Checked)
                    //{
                    sSelectStaffUnder = "All";
                    //}
                    // sSelectStaffUnder = ddlStaff.SelectedValue;
                    Logik process = new Logik();
                    sCommand = GenerateCommand(sSelectStaffUnder, dtStart, dtEnd);
                    dtTableInfo = process.SelectDataByCmd(sCommand);
                    //YPT 03.18.2020

                    //Select value
                    if (dtTableInfo != null)
                    {
                        DataTable dt = new DataTable();
                        //dt.Columns.AddRange(new DataColumn[8] { new DataColumn("datetime"), new DataColumn("userid"), new DataColumn("username"), new DataColumn("surname"), new DataColumn("starttime"), new DataColumn("endtime"), new DataColumn("workingh"), new DataColumn("comment") });
                        dt.Columns.AddRange(new DataColumn[12] { new DataColumn("datetime"), new DataColumn("userid"), new DataColumn("username"), new DataColumn("surname"), new DataColumn("starttime"), new DataColumn("endtime"), new DataColumn("workingh"), new DataColumn("starttimem"), new DataColumn("endtimem"), new DataColumn("workinghm"), new DataColumn("status_health"), new DataColumn("comment") });
                        foreach (DataRow row in dtTableInfo.Rows)
                        {
                            DateTime dtDatetime = (DateTime)row["datetime"];
                            string sDatetimeS = dtDatetime.ToString("dd-MM-yyyy");
                            string sUserid = row["userid"].ToString();
                            sUserid = sUserid.Substring(3);

                            string sUsername = row["username"].ToString();
                            string sSurname = row["surname"].ToString();

                            TimeSpan tTime = (TimeSpan)row["starttime"];
                            string sStarttime = tTime.ToString("hh\\:mm");
                            if (String.Equals(sStarttime, "00:00", StringComparison.OrdinalIgnoreCase))
                            {
                                sStarttime = "-";
                            }
                            tTime = (TimeSpan)row["endtime"];
                            string sEndtime = tTime.ToString("hh\\:mm");
                            if (String.Equals(sEndtime, "00:00", StringComparison.OrdinalIgnoreCase))
                            {
                                sEndtime = "-";
                            }

                            tTime = (TimeSpan)row["workinghours"];
                            string sWorkingh = tTime.ToString("hh\\:mm");

                            //YPT 03.22.2020
                            string sStarttimem = "00:00";
                            tTime = (TimeSpan)row["starttimem"];
                            if (tTime != null)
                            {
                                sStarttimem = tTime.ToString("hh\\:mm");
                            }

                            if (String.Equals(sStarttimem, "00:00", StringComparison.OrdinalIgnoreCase))
                            {
                                sStarttimem = "-";
                            }

                            string sEndtimem = "00:00";
                            tTime = (TimeSpan)row["endtimem"];
                            if (tTime != null)
                            {
                                sEndtimem = tTime.ToString("hh\\:mm");
                            }

                            if (String.Equals(sEndtimem, "00:00", StringComparison.OrdinalIgnoreCase))
                            {
                                sEndtimem = "-";
                            }

                            string sWorkinghoursm = "00:00";
                            tTime = (TimeSpan)row["workinghoursm"];
                            if (tTime != null)
                            {
                                sWorkinghoursm = tTime.ToString("hh\\:mm");
                            }

                            string stemperature = "";
                            stemperature = row["status_health"].ToString();
                            if (String.IsNullOrEmpty(stemperature))
                            {
                                stemperature = "0";
                            }
                            stemperature = GetNameTemperature(stemperature);

                            string scomment = row["comment"].ToString();

                            if (String.Equals(scomment, "[double click]", StringComparison.OrdinalIgnoreCase))
                            {
                                scomment = "";
                            }
                            // dt.Rows.Add(sDatetimeS, sUserid, sUsername, sSurname, sStarttime, sEndtime, sWorkingh, scomment);
                            dt.Rows.Add(sDatetimeS, sUserid, sUsername, sSurname, sStarttime, sEndtime, sWorkingh, sStarttimem, sEndtimem, sWorkinghoursm, stemperature, scomment);
                            bFoundData = true;
                        }

                        GridViewStaffUnder.PageSize = 10; //limit items
                        GridViewStaffUnder.AllowPaging = true;
                        GridViewStaffUnder.DataSource = dt;
                        GridViewStaffUnder.PageIndex = 0;
                        GridViewStaffUnder.DataBind();

                        GridViewStaffUnder.Visible = true;
                        Ck_Export.Visible = true;
                        /*if (m_bFoundStaff)
                        {
                            lbl_staff.Visible = true;
                            ddlStaff.Visible = true;

                        }*/
                    }
                    if (!bFoundData)
                    {
                        //lbl_err.Text = "ไม่พบข้อมูล";
                        // lbl_err.Visible = true;
                        Ck_Export.Checked = false;
                        Ck_Export.Visible = false;
                        imgExcel.Visible = false;
                        ShowMessage("ไม่พบข้อมูล");
                        GridViewStaffUnder.Visible = false;

                    }
                    else
                    {
                        lbl_err.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        public bool ValidateEntryTime(DateTime dtTime)
        {
            bool bStatus = false;
            int nStatus = 0;

            try
            {
                TimeSpan tBase = new TimeSpan(9, 31, 0);
                TimeSpan tHours = new TimeSpan(dtTime.Hour, dtTime.Minute, dtTime.Second);

                nStatus = TimeSpan.Compare(tHours, tBase);
                if ((nStatus == 1) || (nStatus == 0))
                {
                    bStatus = false;
                }
                else
                {
                    bStatus = true;
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }
        public bool ValidateWorkingHours(DateTime dtTime)
        {
            bool bStatus = false;
            int nStatus = 0;

            try
            {
                TimeSpan tBase = new TimeSpan(9, 0, 0);
                TimeSpan tHours = new TimeSpan(dtTime.Hour, dtTime.Minute, dtTime.Second);

                nStatus = TimeSpan.Compare(tHours, tBase);
                if ((nStatus == 1) || (nStatus == 0))
                {
                    bStatus = true;
                }
                else
                {
                    bStatus = false;
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }
        public string GenerateCommand(string sMode, DateTime dtStartDate, DateTime dtEndDate)
        {
            string sReturnString = "";
            string sUserid = "";
            int nIndex = 0;
            bool bAddFirst = false;
            try
            {
                string query = "select * from (select datetime,userid,username,surname,starttime,endtime,workinghours,starttimem,endtimem,workinghoursm,status_health,comment from timestampinfo order by userid) as t where (datetime between ";
                query += "'";
                query += dtStartDate.ToString("yyyy-MM-dd");
                query += "' ";
                query += "AND '";
                query += dtEndDate.ToString("yyyy-MM-dd");

                query += "') AND (";

                if (String.Equals(sMode, "All", StringComparison.OrdinalIgnoreCase))
                {
                    foreach (ListItem item in dpp_Staff.Items)
                    {
                        query += "userid='000";
                        query += item.Value;
                        query += "'";
                        if (nIndex != dpp_Staff.Items.Count - 1)
                        {
                            query += " OR ";
                        }
                        nIndex++;
                    }
                }
                else
                {
                    foreach (GridViewRow row in GridViewStaff.Rows)
                    {
                        if (nIndex < GridViewStaff.Rows.Count)
                        {

                            bool isChecked = row.Cells[0].Controls.OfType<CheckBox>().FirstOrDefault().Checked;

                            if (isChecked)
                            {
                                if (bAddFirst)
                                {
                                    query += " OR ";
                                }

                                query += "userid='000";
                                query += row.Cells[1].Text;
                                query += "'";
                                bAddFirst = true;

                            }

                        }
                        nIndex++;
                    }
                }

                query += ") ORDER BY DATE( t.datetime) ASC;";

                sReturnString = query;
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return sReturnString;
        }
        protected string GetNameTemperature(string sValue)
        {
            string sReturn = "";
            if (String.Equals(sValue, "0", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "ปกติ";
            }
            else if (String.Equals(sValue, "1", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "มีไข้";
            }
            else if (String.Equals(sValue, "2", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "ผื่่น";
            }
            else if (String.Equals(sValue, "3", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "ตาแดง";
            }
            else if (String.Equals(sValue, "4", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "น้ำมูกไหล";
            }
            else if (String.Equals(sValue, "ปกติ", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "0";
            }
            else if (String.Equals(sValue, "มีไข้", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "1";
            }
            else if (String.Equals(sValue, "ผื่่น", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "2";
            }
            else if (String.Equals(sValue, "ตาแดง", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "3";
            }
            else if (String.Equals(sValue, "น้ำมูกไหล", StringComparison.OrdinalIgnoreCase))
            {
                sReturn = "4";
            }
            else
            {
                sReturn = "0";
            }
            return sReturn;
        }
        protected void GetStaffUnder(ref Logik process, string sEmail, string sPwd)
        {
            bool bStatus = false;
            string sInputjSon = "";
            sInputjSon = process.generateInput(sEmail, sPwd);
            bStatus = process.CallADServiceStaffUnder(sInputjSon);
            if (bStatus)
            {
                AddDataStaffUnderToTable(process.m_personInfo.arrPersonInfo, process.m_arrListStaff);

                dpp_Staff.Items.Clear();
                if (process.m_personInfo.arrPersonInfo != null)
                {
                    if (process.m_personInfo.arrPersonInfo.Count() > 0)
                    {
                        AddStaffToList(process.m_personInfo.arrPersonInfo);
                    }
                }
            }
        }
        public void AddDataStaffUnderToTable(List<PersonInfoWithUnder> lsPerson, List<PersonInfoWithUnder> lsPersonStaff)
        {
            AddStaffUnder(lsPerson, lsPersonStaff);
        }
        public void AddStaffUnder(List<PersonInfoWithUnder> lsPerson, List<PersonInfoWithUnder> lsPersonStaff)
        {
            try
            {
                foreach (PersonInfoWithUnder item in lsPerson)
                {
                    AddDataList(item, lsPersonStaff);
                    if (item.arrPersonInfo != null)
                    {
                        if (item.arrPersonInfo.Count > 0)
                        {
                            AddStaffUnder(item.arrPersonInfo, lsPersonStaff);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        public void AddDataList(PersonInfoWithUnder item, List<PersonInfoWithUnder> lsPerson)
        {
            string sId = "";
            try
            {
                foreach (PersonInfoWithUnder itemDetails in lsPerson)
                {
                    if (String.Equals(item.name, itemDetails.fullNameEN, StringComparison.OrdinalIgnoreCase))
                    {
                        sId = itemDetails.id;
                        item.fullNameTH = itemDetails.fullNameTH;
                        item.id = sId;
                        item.depNameTH = itemDetails.depNameTH;

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        protected void AddStaffToList(List<PersonInfoWithUnder> arrPersonInfos)
        {
            string sText = "";
            string sValue = "";
            foreach (PersonInfoWithUnder itemDetails in arrPersonInfos)
            {
                sValue = itemDetails.id;
                sText = itemDetails.fullNameTH;
                dpp_Staff.Items.Add(new ListItem(sText, sValue));
                if (itemDetails.arrPersonInfo != null)
                {
                    if (itemDetails.arrPersonInfo.Count() > 0)
                    {
                        AddStaffToList(itemDetails.arrPersonInfo);
                    }
                }
            }
        }
        protected string GetName(string sUserid)
        {
            string sReturn = "";
            foreach (ListItem item in dpp_Staff.Items)
            {
                if (String.Equals(item.Value, sUserid, StringComparison.OrdinalIgnoreCase))
                {
                    sReturn = item.Text;
                }

            }
            return sReturn;
        }

        protected void rdb_time_CheckedChanged(object sender, EventArgs e)
        {
            if (rdb_time.Checked)
            {
                GridViewStaffTimeSheet.Visible = false;
                BindGridStaffUnder();
            }
            else
            {
                GridViewStaffUnder.Visible = false;
            }
        }

        protected void rdb_timesheet_CheckedChanged(object sender, EventArgs e)
        {
            if(rdb_timesheet.Checked)
            {
                GridViewStaffUnder.Visible = false;
                BindDataTimeSheet();
            }
            else
            {
                GridViewStaffTimeSheet.Visible = false;
            }
        }

        protected void imgExcel_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ExportGridToExcel();
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }

        private void ExportGridToExcel()
        {
            try
            {
                string sDateStart = "";
                string sDateEnd = "";
                string sFileName = "";
                DataTable dt = null;

                DateTime dtDateS = DateTime.ParseExact(txtDateStart.Text, "dd/MM/yyyy", null);
                DateTime dtDateE = DateTime.ParseExact(txtDateEnd.Text, "dd/MM/yyyy", null);
                sDateStart = dtDateS.ToString("yyyy-MM-dd");
                sDateEnd = dtDateE.ToString("yyyy-MM-dd");

                Logik process = new Logik();
                if (rdb_time.Checked)
                {
                    BindGridStaffUnder();
                }
                else
                {
                    BindDataTimeSheet();
                }

                if (GridViewStaffUnder.Visible)
                {
                    process.ExportGridToExcel(sDateStart, sDateEnd, GridViewStaffUnder,0, ref dt, ref sFileName);
                }
                else
                {
                    process.ExportGridToExcel(sDateStart, sDateEnd, GridViewStaffTimeSheet, 1, ref dt,ref sFileName);
                }
                if (dt != null)
                {
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(dt);

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", sFileName);
                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        
        protected void Ck_Export_CheckedChanged(object sender, EventArgs e)
        {
            if (Ck_Export.Checked)
            {
                imgExcel.Visible = true;
            }
            else
            {
                imgExcel.Visible = false;
            }

        }
        public void ShowMessage(string sText)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("window.onload=function(){");
            sb.Append("alert('");
            sb.Append(sText);
            sb.Append("')};");
            sb.Append("</script>");
            ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
        }
       
    }

}