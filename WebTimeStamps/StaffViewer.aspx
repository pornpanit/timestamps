﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="StaffViewer.aspx.cs" Inherits="WebTimeStamps.StaffViewer" EnableEventValidation="false"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link href="assets/css/Gridstyle.css" rel="stylesheet" type="text/css" />

    <link type="text/css" href="assets/css/ui-lightness/jquery-ui-1.8.19.custom.css" rel="stylesheet" />
    <link type="text/css" href="assets/css/main.css" rel="stylesheet" />
    <script type="text/javascript" src="Scripts/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.8.19.custom.min.js"></script>
    <script type="text/javascript">

        $(function () {
            $("#txtDateStart").datepicker();
            $("#txtDateEnd").datepicker();
            $("#txtDateStartStaff").datepicker();
            $("#txtDateEndStaff").datepicker();
            $("#txt_Date").datepicker();
        });
    </script>
    <script>
        if (window.history.replaceState) {
            window.history.replaceState(null, null, window.location.href);
        }
    </script>
    <style type="text/css">
    .modal
    {
        position: fixed;
        top: 0;
        left: 0;
        background-color: black;
        z-index: 99;
        opacity: 0.8;
        filter: alpha(opacity=80);
        -moz-opacity: 0.8;
        min-height: 100%;
        width: 100%;
    }
    .loading
    {
        font-family: Arial;
        font-size: 10pt;
        border: 5px solid #67CFF5;
        width: 200px;
        height: 100px;
        display: none;
        position: fixed;
        background-color: White;
        z-index: 999;
    }
</style>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
    function ShowProgress() {
        setTimeout(function () {
            var modal = $('<div />');
            modal.addClass("modal");
            $('body').append(modal);
            var loading = $(".loading");
            loading.show();
            var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
            var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
            loading.css({ top: top, left: left });
        }, 200);
    }
    $(document).ready(function () {

        $("#btn_load").click(function () {
           // alert($("#btn_load").attr('id'));
            ShowProgress();
        });
    });
</script>
    
</head>
<body>
    <form id="form1" runat="server" class="center">
        <br/>
             <table class="auto-style3">
                 <tr>
                     <td align="right" class="auto-style6">
                         <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                     </td>
                     <td class="auto-style2" align="right">
                         <asp:LinkButton id="lk_logout" runat="server" OnClick="Onlogout">ออกจากระบบ</asp:LinkButton>
                     </td>
                 </tr>
                 <tr>
                     <td align="right" class="auto-style5" align="right">
                         <asp:Label ID="lbl_name" runat="server" Text="Label" Visible="False"></asp:Label>
                         <asp:Label ID="lbl_surname" runat="server" Text="Label" Visible="False"></asp:Label>
                     </td>
                     <td class="auto-style2" align="right">
                         
                     </td>
                 </tr>
                 <tr>
                     <td align="right" class="auto-style5" align="right">
                         <asp:Label ID="lbl_PS" runat="server" Visible="False"></asp:Label>
                     </td>
                     <td class="auto-style2" align="right">
                         <asp:Label ID="lb_username_surname" runat="server" Text="Label"></asp:Label>
                     </td>
                 </tr>
             </table>
        <br/>
         <table class="auto-style3">
                <tr>
                     <td class="auto-style2" align="Left">
                         |&nbsp;&nbsp;<asp:LinkButton id="LnK1" runat="server" OnClick="OnTimeAtt">Time Attendance</asp:LinkButton>&nbsp;&nbsp;|&nbsp;&nbsp;<asp:LinkButton id="LnK2" runat="server" OnClick="OnTimeSheet">Timesheet</asp:LinkButton>&nbsp;&nbsp;|&nbsp;&nbsp;<asp:LinkButton id="LnK6" runat="server" OnClick="OnTimeSheetPeriod">Timesheet period</asp:LinkButton>&nbsp;&nbsp;|&nbsp;&nbsp;<asp:LinkButton id="LnK3" runat="server" OnClick="OnReport">Report</asp:LinkButton>&nbsp;&nbsp;|&nbsp;&nbsp;<asp:LinkButton id="Lnk5" runat="server" OnClick="OnReportResult">รายงานผลการปฏิบัติงาน</asp:LinkButton>&nbsp;&nbsp;|&nbsp;&nbsp;<asp:Label ID="Label3" runat="server" Text="ข้อมูลของผู้ใต้บังคับบัญชา"></asp:Label>&nbsp;&nbsp;<asp:Label ID="lbl_end" runat="server" Text="|"/>
                     </td>
                 </tr>
             </table>
             <br/>
         <div class="centerwithborder">
            <br/>
            <table align="center">
                <tr>
                       <td class="auto-style1" align="right">
                            <div>
                                <asp:label ID="lbl_userid" Text="รหัสพนักงาน : " AutoCompleteType="Disabled" runat="server" Visible="False"/>
                            </div>
                        </td>
                        <td class="auto-style1">
                            <asp:TextBox ID="txt_userid" runat="server" Visible="False"></asp:TextBox>
                        </td>
                    </tr>
                <tr>
                    <td class="auto-style1"  align="right">
                        <div>
                            <asp:label ID="lbl_DatStart" Text="ตั้งแต่วันที่ : " AutoCompleteType="Disabled" align="right" runat="server"/>
                        </div>
                    </td>
                    <td class="auto-style1">
                        <div>
                        <asp:TextBox ID="txtDateStart" AutoCompleteType="Disabled" runat="server"/>
                        </div>
                    </td>
                    <td class="auto-style1-small"  align="center">
                        <div>
                            <asp:label ID="Label1" Text="ถึง" AutoCompleteType="Disabled" align="right" runat="server"/>
                        </div>
                    </td>
                    <td class="auto-style1">
                        <div>
                            <asp:TextBox ID="txtDateEnd"  AutoCompleteType="Disabled" runat="server"/>
                        </div>
                    </td>
                </tr>
                    <tr>
                        <td class="auto-style1">
                        </td>
                        <td class="auto-style1">
                           
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style1">
                            
                        </td>
                        <td class="auto-style1">
                            <asp:Button class="buttonRounded" ID="btn_load" runat="server" OnClick="btnload_Click" Text="ค้นหา" />
                            <asp:Label ID="lbl_err" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                        </td>
                     </tr>
            </table>
             <br/>
        </div>
        <br/>
        <div>
            <table align="center" class="center">
               
                <tr>
                     <td class="auto-style12" align="left">
                         <asp:RadioButton ID="rdb_time" runat="server" GroupName="typejob" Text="Time Attendance" AutoPostBack="True" OnCheckedChanged="rdb_time_CheckedChanged" />
                         &nbsp;
                         <asp:RadioButton ID="rdb_timesheet" runat="server" GroupName="typejob" Text="Timesheet" AutoPostBack="True" OnCheckedChanged="rdb_timesheet_CheckedChanged" />
                         &nbsp;&nbsp;&nbsp;&nbsp;
                         <asp:DropDownList ID="dpp_Staff" runat="server" Visible="False"></asp:DropDownList>
                     &nbsp;
                       <asp:CheckBox ID="Ck_Export" runat="server" CssClass="checkbox-centered" Text="Export data" OnCheckedChanged="Ck_Export_CheckedChanged" AutoPostBack="true"/> &nbsp;
                        <asp:ImageButton ID="imgExcel" runat="server" Height="24px" ImageUrl="~/img/ex.png" Width="28px" OnClick="imgExcel_Click" ImageAlign="Top" Visible="False" />
                     </td>
                </tr>
                 <tr>
                     <td class="auto-style12" align="Left">
                         &nbsp;
                         <asp:CheckBox ID="ck_all" runat="server" AutoPostBack="True" Text="ทั้งหมด" Visible="False" OnCheckedChanged="ck_all_CheckedChanged" />
&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:DropDownList ID="ddp_dep" runat="server" Height="16px" Width="178px" Visible="False">
                          </asp:DropDownList>

                    </td>
                </tr>
                <tr>
                      <td class="auto-style11">
                           <asp:GridView ID="GridViewStaff" runat="server" AutoGenerateColumns="false" CssClass="Grid" AlternatingRowStyle-CssClass="alt" OnPageIndexChanging="SubmitAppraisalGridStaff_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="userid" HeaderText="รหัส" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Center" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="username" HeaderText="ชื่อ - สกุล" ItemStyle-Width="250" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" ReadOnly="true"/>
                                
                            </Columns>
                        </asp:GridView>
                           <asp:GridView ID="GridViewStaffUnder" runat="server" AutoGenerateColumns="false" OnRowDataBound = "OnRowDataBoundStaffUnders" CssClass="Grid" AlternatingRowStyle-CssClass="alt" OnPageIndexChanging="SubmitAppraisalGridStaffUnder_PageIndexChanging">
                            <Columns>
                                <asp:BoundField DataField="datetime" HeaderText="วันที่" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Center" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="userid" HeaderText="รหัส" ItemStyle-Width="120" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" ReadOnly="true"/>
                                <asp:BoundField DataField="username" HeaderText="ชื่อ" ItemStyle-Width="150" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="surname" HeaderText="สกุล" ItemStyle-Width="200" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="starttime" HeaderText="เวลาเข้า" ItemStyle-Width="100" ItemStyle-HorizontalAlign="Center" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="endtime" HeaderText="เวลาออก" ItemStyle-Width="100" ItemStyle-HorizontalAlign="Center" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="workingh" HeaderText="จำนวน ชม." ItemStyle-Width="80" ItemStyle-HorizontalAlign="Center" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="starttimem" HeaderText="เวลาเข้า(M)" ItemStyle-Width="100" ItemStyle-HorizontalAlign="Center" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="endtimem" HeaderText="เวลาออก(M)" ItemStyle-Width="100" ItemStyle-HorizontalAlign="Center" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="workinghm" HeaderText="จำนวน ชม.(M)" ItemStyle-Width="80" ItemStyle-HorizontalAlign="Center" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="status_health" HeaderText="อาการ" ItemStyle-Width="80" ItemStyle-HorizontalAlign="Center" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                
                                <asp:BoundField DataField="comment" HeaderText="หมายเหตุ" ItemStyle-Width="300" ItemStyle-HorizontalAlign="Left" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                            </Columns>
                        </asp:GridView>
                     </td>
                </tr>
                <tr>
                      <td class="center">
                           <asp:GridView ID="GridViewStaffTimeSheet" runat="server" AutoGenerateColumns="false" CssClass="Grid" AlternatingRowStyle-CssClass="alt" OnPageIndexChanging="SubmitAppraisalGridStaffTimeSheet_PageIndexChanging">
                            <Columns>
                               <asp:BoundField DataField="work_date" HeaderText="Date" ItemStyle-Width="120" ItemStyle-HorizontalAlign="Center" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                 <asp:BoundField DataField="username" HeaderText="Name - Surname" ItemStyle-Width="120" ItemStyle-HorizontalAlign="Left" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="project" HeaderText="Project" ItemStyle-Width="200" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top" ReadOnly="true"/>
                                <asp:BoundField DataField="main_activity" HeaderText="Main output" ItemStyle-Width="200" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="sub_activity" HeaderText="Sub output" ItemStyle-Width="200" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="work_hour" HeaderText="WH" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                 <asp:BoundField DataField="txt_remark" HeaderText="Remark" ItemStyle-Width="400" ItemStyle-HorizontalAlign="Left" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>

                            </Columns>
                        </asp:GridView>
                     </td>
                </tr>
                </table>
		</div>
        <div class="loading" align="center">
            Loading. Please wait.<br />
            <br />
            <img src="./img/loader.gif" alt="" />
        </div>
    </form>
</body>
</html>
