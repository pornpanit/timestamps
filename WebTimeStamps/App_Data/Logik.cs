﻿using ClosedXML.Excel;
using MySql.Data.MySqlClient;
using MySqlDatabase;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using System.Xml;
using Ubiety.Dns.Core;

namespace WebTimeStamps
{
    public class Logik
    {
        public PersonInfoWithUnder m_personInfo = null;
        public string m_sSelectionCmd = "";
        public List<PersonInfoWithUnder> m_arrListStaff = null;

        public class personalData
        {
            public string m_sUserID = "";
            public string m_sUserName = "";
            public string m_sSurname = "";
            public DateTime m_dtStartTime = DateTime.Now;
            public DateTime m_dtEndTime = DateTime.Now;
            public DateTime m_dtWorkingHours = DateTime.Now;
            public DateTime m_dtDate = DateTime.Now;
            public int m_nStatus = 0;
            public int m_nSTypeEntrans = 0;
            public bool m_bStartTime = false;
        }
        public bool ValidateDateTime(DateTime sTimeOld, DateTime sTimeNew)
        {
            bool bStatus = false;
            int nStatus = 0;

            nStatus = DateTime.Compare(sTimeOld, sTimeNew);
            if ((nStatus < 0) || (nStatus == 0))
            {
                bStatus = true;
                //New more than old
            }
            else
            {
                //Console.Write("t1 is greater than t2");
                bStatus = false;
            }

            return bStatus;
        }
        public DataTable SelectPersonalData(string sDateStart, string sDateEnd, string sUserID)
        {
            bool bStatus = false;
            DataTable dtTableInfo = null;

            try
            {
                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();
                dtTableInfo = sqlCon.Select(sDateStart, sDateEnd, sUserID);
                if (dtTableInfo != null)
                {
                    bStatus = true;
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return dtTableInfo;
        }
        public DataTable SelectDataByCmd(string sCommand)
        {
            bool bStatus = false;
            DataTable dtTableInfo = null;

            try
            {
                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();
                dtTableInfo = sqlCon.SelectWithCommand(sCommand);
                if (dtTableInfo != null)
                {
                    bStatus = true;
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return dtTableInfo;
        }

        public string generateInput(string username,string password)
        {
            string sReturnString = "";

            object input = new
            {
                username = username,
                password = password
            };

            sReturnString = (new JavaScriptSerializer()).Serialize(input);

            return sReturnString;

        }

        public bool CallADServiceValidateUser(string inputJson)
        {
            bool bStatus = false;
            string sResponse = "";
            //string serviceUrl = "http://localhost/ADServices/ADServices.svc";

            try {

                string serviceUrl = "http://timeattendance.etda.or.th/ADServices/ADServices.svc";

                HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(new Uri(serviceUrl + "/ValidateAccess"));
                httpRequest.Accept = "application/json";
                httpRequest.ContentType = "application/json";
                httpRequest.Method = "POST";

                byte[] bytes = Encoding.UTF8.GetBytes(inputJson);

                using (Stream stream = httpRequest.GetRequestStream())
                {
                    stream.Write(bytes, 0, bytes.Length);
                    stream.Close();
                }

                using (HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse())
                {
                    using (Stream stream = httpResponse.GetResponseStream())
                    {
                        sResponse = new StreamReader(stream).ReadToEnd();


                        if (!string.IsNullOrEmpty(sResponse))
                        {
                            string s = sResponse.Replace(@"\", string.Empty);
                            string final = s.Trim().Substring(1, s.Length - 2);

                            m_personInfo = JsonConvert.DeserializeObject<PersonInfoWithUnder>(final);

                            if (m_personInfo != null)
                            {
                                if (!string.IsNullOrEmpty(m_personInfo.name))
                                {
                                    bStatus = true;
                                }
                            }
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            
            return bStatus;

        }
        public bool CallUserInfo(string sUsername)
        {
            bool bStatus = false;
            string sSoapInput = "";
            string sStaffID = "";

            try {
                if (!string.IsNullOrEmpty(sUsername))
                {
                    sSoapInput = GenerateSoapInput();

                    //Builds the connection to the WebService.
                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create("https://hr.etda.or.th/webservice-production/Service.asmx");
                    req.Headers.Add("SOAPAction", "http://tempuri.org/InquiryPersonnelNewInfo");
                    req.ContentType = "text/xml; charset=\"utf-8\"";
                    req.Accept = "text/xml";
                    req.Method = "POST";

                    //Passes the SoapRequest String to the WebService
                    using (Stream stm = req.GetRequestStream())
                    {
                        using (StreamWriter stmw = new StreamWriter(stm))
                        {
                            stmw.Write(sSoapInput);
                        }
                    }

                    using (WebResponse Serviceres = req.GetResponse())
                    {
                        using (StreamReader rd = new StreamReader(Serviceres.GetResponseStream()))
                        {
                            var ServiceResult = rd.ReadToEnd();

                            bStatus = GetStaffID(ServiceResult, sUsername);

                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
           
            return bStatus;

        }
        public string GenerateSoapInput()
        {
            string sRequest = "";
            sRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">";
            sRequest = sRequest + "<soapenv:Header/>";
            sRequest = sRequest + "<soapenv:Body>";
            sRequest = sRequest + "<tem:InquiryPersonnelNewInfo>";
            sRequest = sRequest + " <tem:staffID>";
            sRequest = sRequest + "</tem:staffID>";
            sRequest = sRequest + "</tem:InquiryPersonnelNewInfo>";
            sRequest = sRequest + "</soapenv:Body>";
            sRequest = sRequest + "</soapenv:Envelope>";

            return sRequest;
        }
        public bool GetStaffID(string sSoapData, string sUserName)
        {
            bool bStatus = false;
            string sStaffID = "";
            string sStaffNameEN = "";
            string sstaffSurnameEN = "";
            string sStaffNameTH = "";
            string sstaffSurnameTH = "";
            string sdepNameTH = "";

            string sfullNameEN = "";
            string sfullNameTH = "";
            string sId = "";
            int nCount = 0;

            try
            {
                if ((!string.IsNullOrEmpty(sSoapData)) && (!string.IsNullOrEmpty(sUserName)))
                {
                    XmlDocument Doc = new XmlDocument();

                    Doc.LoadXml(sSoapData);

                    m_arrListStaff = new List<PersonInfoWithUnder>();

                    nCount = Doc.GetElementsByTagName("PersonnelNewInfo").Count;

                    for (int i = 0; i < nCount; i++)
                    {
                        XmlNode node = Doc.GetElementsByTagName("PersonnelNewInfo").Item(i);

                        foreach (XmlNode item in node.ChildNodes)
                        {
                            if ((item).NodeType == XmlNodeType.Element)
                            {
                                //Get the Element value here
                                if (String.Equals(item.Name, "staffID", StringComparison.OrdinalIgnoreCase))
                                {
                                    sId = ((item).FirstChild).Value;
                                }
                                else if (String.Equals(item.Name, "staffNameEN", StringComparison.OrdinalIgnoreCase))
                                {
                                    sStaffNameEN = ((item).FirstChild).Value;
                                }
                                else if (String.Equals(item.Name, "staffSurnameEN", StringComparison.OrdinalIgnoreCase))
                                {
                                    sstaffSurnameEN = ((item).FirstChild).Value;
                                }
                                else if (String.Equals(item.Name, "staffNameTH", StringComparison.OrdinalIgnoreCase))
                                {
                                    sStaffNameTH = ((item).FirstChild).Value;
                                }
                                else if (String.Equals(item.Name, "staffSurnameTH", StringComparison.OrdinalIgnoreCase))
                                {
                                    sstaffSurnameTH = ((item).FirstChild).Value;
                                }
                                else if (String.Equals(item.Name, "depNameTH", StringComparison.OrdinalIgnoreCase))
                                {
                                    sdepNameTH = ((item).FirstChild).Value;
                                }
                                
                            }
                            else
                            {
                                //Get the Element value here
                                //string errorField2 = (item).Value;
                                //Console.WriteLine("NodeValue = " + errorField2);
                            }

                        }
                        sfullNameEN = sStaffNameEN + " " + sstaffSurnameEN;
                        //YPT 03.24.2020
                        sfullNameEN = sfullNameEN.Replace("  "," ");

                        sfullNameTH = sStaffNameTH + " " + sstaffSurnameTH;
                        if (!string.IsNullOrEmpty(sfullNameEN))
                        {
                            PersonInfoWithUnder person = new PersonInfoWithUnder();
                            person.id = sId;
                            person.fullNameEN = sfullNameEN;
                            person.fullNameTH = sfullNameTH;
                            person.depNameTH = sdepNameTH;
                            person.usernameTH = sStaffNameTH;
                            person.SurnameTH = sstaffSurnameTH;
                            if (String.Equals(sfullNameEN, sUserName, StringComparison.OrdinalIgnoreCase))
                            {
                                m_personInfo = new PersonInfoWithUnder();
                                m_personInfo.id = person.id;
                                m_personInfo.fullNameEN = person.fullNameEN;
                                m_personInfo.fullNameTH = person.fullNameTH;
                                m_personInfo.usernameTH = sStaffNameTH;
                                m_personInfo.SurnameTH = sstaffSurnameTH;
                            }

                            m_arrListStaff.Add(person);
                        }
                    }

                    if (!string.IsNullOrEmpty(m_personInfo.id))
                    {
                        bStatus = true;
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        public void GetStartAndEndDate(ref string sStartDate,ref string sEndDate)
        {
            DateTime now = DateTime.Now;
            DateTime dtStartDate = new DateTime(now.Year, now.Month, 1);
            DateTime dtEndDate = dtStartDate.AddMonths(1).AddDays(-1);
            sStartDate = dtStartDate.ToString("dd/MM/yyyy");
            sEndDate = dtEndDate.ToString("dd/MM/yyyy");

        }
        public void GetStartAndEndDateView(ref string sStartDate, ref string sEndDate)
        {
            DateTime now = DateTime.Now;
            DateTime dtStartDate = new DateTime(now.Year, now.Month, 1);
            DateTime dtEndDate = dtStartDate.AddMonths(1).AddDays(-1);
            sStartDate = dtStartDate.ToString("yyyy-MM-dd");
            sEndDate = dtEndDate.ToString("yyyy-MM-dd");

        }
        public int GetManDay(DateTime dtDate)
        {
            DateTime dtStartDate = new DateTime(dtDate.Year, dtDate.Month, 1);
            DateTime dtEndDate = dtStartDate.AddMonths(1).AddDays(-1);
            return dtEndDate.Day;
        }

        public bool InsertData(string sEmpId, string sTaskId, string sWorkDate, string sWorkHour, string sTxtRemark, string sUpdUsreId,int nMode)
        {
            bool bStatus = false;
            DataTable dtTableInfo = null;
            try
            {
                int nIndex = 0;
                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();
                //YPT 04.18.2021   nMode 1 = period others = old
                if (nMode == 1)
                {
                    nIndex = sqlCon.CountRowPeriod();
                    nIndex++;

                    bStatus = sqlCon.InsertTimesheetPeriod(nIndex, sEmpId, sTaskId, sWorkDate, sWorkHour, sTxtRemark, sUpdUsreId);
                }
                else {
                    nIndex = sqlCon.CountRow();
                    nIndex++;

                    bStatus = sqlCon.InsertTimesheet(nIndex, sEmpId, sTaskId, sWorkDate, sWorkHour, sTxtRemark, sUpdUsreId);
                }
                
               
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }
        public bool DeleteData(string sID,int nMode)
        {
            bool bStatus = false;
            DataTable dtTableInfo = null;
            try
            {
                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();

                if (nMode == 1)
                {
                    sqlCon.DeletePeriod(sID);
                }
                else {
                    sqlCon.Delete(sID);
                }
                bStatus = true;
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }
        public bool CallADServiceStaffUnder(string inputJson)
        {
            bool bStatus = false;
            string sResponse = "";
            //string serviceUrl = "http://localhost/ADServices/ADServices.svc";
            try
            {
                string serviceUrl = "http://timeattendance.etda.or.th/ADServices/ADServices.svc";

                HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(new Uri(serviceUrl + "/GetStaffUnder"));
                httpRequest.Accept = "application/json";
                httpRequest.ContentType = "application/json";
                httpRequest.Method = "POST";

                byte[] bytes = Encoding.UTF8.GetBytes(inputJson);

                using (Stream stream = httpRequest.GetRequestStream())
                {
                    stream.Write(bytes, 0, bytes.Length);
                    stream.Close();
                }

                using (HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse())
                {
                    using (Stream stream = httpResponse.GetResponseStream())
                    {
                        sResponse = new StreamReader(stream).ReadToEnd();


                        if (!string.IsNullOrEmpty(sResponse))
                        {

                            string s = sResponse.Replace(@"\", string.Empty);

                            string final = s.Trim().Substring(1, s.Length - 2);

                            m_personInfo = JsonConvert.DeserializeObject<PersonInfoWithUnder>(final);

                            if (m_personInfo.arrPersonInfo.Count > 0)
                            {
                                bStatus = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;

        }
        public string GenerateCommand(string sUserid, string sDateStart, string sDateEnd,bool bStaffUnder, ListItemCollection listItem)
        {
            string sReturnString = "";
            int nIndex = 0;

            string query = "select * from (select work_date,userid ,project,main_activity, sub_activity, work_hour, txt_remark from etda_t_project a, etda_t_timesheet b where a.task_id = b.task_id order by b.work_date) as t where (t.work_date between ";
            query += "'";
            query += sDateStart;
            query += "' and ";
            query += "'";
            query += sDateEnd;
            query += "') and ";
            query += "(t.userid=";
            query += "'";
            query += sUserid;
            query += "' ";
            if (bStaffUnder)
            {
                query += "OR ";
                foreach (ListItem item in listItem)
                {
                    query += "t.userid='";
                    query += item.Value;
                    query += "'";
                    if (nIndex != listItem.Count - 1)
                    {
                        query += " OR ";
                    }
                    else
                    {
                        query += ")";
                    }
                    nIndex++;
                }
            }
            else
            {
                query += ")";
            }

            query += " order by t.work_date asc;";

            sReturnString = query;
            return sReturnString;
        }
        
        public DataTable ExportDatafromDatabase(string sUserid, string sDateStart, string sDateEnd, bool bStaffUnder, ListItemCollection listItem)
        {
            string sCommand = "";
            DataTable dtTable = null;

            try {

                sCommand = GenerateCommand(sUserid, sDateStart, sDateEnd, bStaffUnder, listItem);
                dtTable = SelectDataByCmd(sCommand);
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            
            return dtTable;
        }
      
        public bool IsValidTime(string sValue)
        {
            string[] textParts = GetParts(sValue);

            if (textParts.Length != 2) return false;
            if (sValue.Length != 5) return false;
            string hourString = textParts[0];

            string minuteString = textParts[1];
            int hour, minute;
            if (int.TryParse(hourString, out hour))
            {
                if (int.TryParse(minuteString, out minute))
                {
                    if (isMinute(minute) && IsHour(hour)) return true;
                    else return false;
                }
                else return false;
            }
            else return false;
        }
        private bool isMinute(int minute)
        {
            return ((minute >= 0) && (minute < 60));
        }
        private bool IsHour(int hour)
        {
            return ((hour >= 0) && (hour < 24));
        }
        private static string[] GetParts(string text)
        {
            return text.Split(':');
        }
        public bool InsertPersonData(personalData un)
        {
            bool bStatus = false;
            bool bUsedefault = false;

            int nIndex = 0;
            try
            {
                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();
                bStatus = ValidateUser(un);
                if (!bStatus)
                {
                    nIndex = sqlCon.CountRow();
                    nIndex++;
                    bStatus = sqlCon.InsertManual(nIndex, un.m_dtDate.ToString("yyyy-MM-dd"), un.m_sUserID, un.m_sUserName, un.m_sSurname, un.m_dtStartTime.ToString("HH:mm:ss"), un.m_dtEndTime.ToString("HH:mm:ss"), un.m_dtWorkingHours.ToString("HH:mm:ss"), un.m_nStatus);
                    
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }
        public bool ValidateUser(personalData unData)
        {
            bool bStatus = false;

            try
            {
                string sCurrentDate = "";

                DataTable dtTableInfo = null;
                DateTime dtCurrent = unData.m_dtDate;

                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();

                sCurrentDate = dtCurrent.ToString("yyyy-MM-dd");
                
                dtTableInfo = sqlCon.Select(sCurrentDate, sCurrentDate, unData.m_sUserID);
                if (dtTableInfo != null)
                {
                    if (dtTableInfo.Rows.Count > 0)
                    {
                        //sDate = dtTableInfo.Rows[i].ItemArray[j].ToString();
                        bStatus = sqlCon.Update(sCurrentDate, unData.m_sUserID, unData.m_dtStartTime.ToString("HH:mm:ss"), unData.m_dtEndTime.ToString("HH:mm:ss"), unData.m_dtWorkingHours.ToString("HH:mm:ss"), unData.m_nStatus);
                        
                    }

                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }
        public DateTime ValidateStartTime(DateTime dtDateTDef, DateTime dtDate)
        {
            bool bStatus = false;
            DateTime dtTimeBase;
            DateTime dtTimeReturn = DateTime.Now;

            if (dtDateTDef == null)
            {
                dtTimeBase = CreateTimeFromDate(7, 0, 0, dtDate);
            }
            else
            {
                dtTimeBase = dtDateTDef;
            }

            bStatus = ValidateDateTime(dtTimeBase, dtDate);
            if (bStatus)
            {
                dtTimeReturn = dtDate;
            }
            else
            {
                dtTimeReturn = dtTimeBase;
            }

            return dtTimeReturn;
        }
        public DateTime CreateTimeFromDate(int nHour, int nMinute, int nSecond, DateTime dtTime)
        {
            return new DateTime(dtTime.Year, dtTime.Month, dtTime.Day, nHour, nMinute, nSecond);
        }
        public bool CalculateWorkingHours(DateTime timeStartDef, DateTime timeStart, DateTime timeEnd, ref TimeSpan tWorkingH)
        {
            bool bStatus = false;
            bool bStatusStart = false;
            int nStatus = 0;

            try
            {
                TimeSpan tBase = new TimeSpan(9, 0, 0);
                //TimeSpan tBreak = new TimeSpan(1, 0, 0);
                timeStart = ValidateStartTime(timeStartDef, timeStart);

                if (timeEnd < timeStart)
                {
                    timeEnd = timeStart;
                }
                //YPT 04.0
                bStatusStart = ValidateEntryTime(timeStart);

                tWorkingH = timeEnd - timeStart;
                //tWorkingH = tWorkingH - tBreak;
                nStatus = TimeSpan.Compare(tWorkingH, tBase);

                if (((nStatus == 1) || (nStatus == 0)) && (bStatusStart))
                {
                    // Console.Write("Working is greater than basetime");
                    bStatus = true;
                }
                else
                {
                    //Console.Write("basetime is greater than workingTime");
                    bStatus = false;
                    if (timeEnd == timeStart)
                    {
                        timeEnd = CreateTimeFromDate(0, 0, 0, timeEnd);
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }
        public DataTable SelectReport(string userId, string sStartDate, string sEndDate)
        {
            bool bStatus = false;
            DataTable dtTableInfo = null;

            try
            {
                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();
                dtTableInfo = sqlCon.GetReportByUserID(userId, sStartDate, sEndDate);
                if (dtTableInfo != null)
                {
                    bStatus = true;
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return dtTableInfo;
        }
        public DataTable SelectReportResult(string userId, string sStartDate, string sEndDate)
        {
            bool bStatus = false;
            DataTable dtTableInfo = null;

            try
            {
                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();
                dtTableInfo = sqlCon.GetReportResultByUserID(userId, sStartDate, sEndDate);
                if (dtTableInfo != null)
                {
                    bStatus = true;
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return dtTableInfo;
        }
        public bool UpdateData(string sCmd, string sAction)
        {
            bool bStatus = false;
            try
            {
                DataTable dtTableInfo = null;
                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();

                dtTableInfo = sqlCon.ExecuteQuery(sCmd, sAction);
                if (dtTableInfo != null)
                {
                    bStatus = true;
                }

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        public string GenerateCommandViewTimeSheet(string sUserID,string sStartDate,string sEndDate)
        {
            string sReturnString = "";
            int nIndex = 0;

            sReturnString = "select  trans_id,work_date ,project,main_activity, sub_activity, work_hour, txt_remark from etda_t_project a, etda_t_timesheet b where a.task_id = b.task_id and userid = '";
            sReturnString += sUserID;
            sReturnString += "' ";
            sReturnString += "and (work_date BETWEEN ";
            sReturnString += "'";
            sReturnString += sStartDate;
            sReturnString += "' ";
            sReturnString += "AND '";
            sReturnString += sEndDate;
            sReturnString += "') order by work_date desc";

            return sReturnString;
        }
        public string GenerateCommandViewTimeSheetPeriod(string sUserID, string sStartDate, string sEndDate)
        {
            string sReturnString = "";
            int nIndex = 0;

            sReturnString = "select  trans_id,work_date ,project,main_activity, sub_activity, work_hour, txt_remark from etda_t_project_period a, etda_t_timesheet_period b where a.task_id = b.task_id and userid = '";
            sReturnString += sUserID;
            sReturnString += "' ";
            sReturnString += "and (work_date BETWEEN ";
            sReturnString += "'";
            sReturnString += sStartDate;
            sReturnString += "' ";
            sReturnString += "AND '";
            sReturnString += sEndDate;
            sReturnString += "') order by work_date desc";

            return sReturnString;
        }
        public string GenerateCommandViewTimeSheetExport(string sUserID, string sStartDate, string sEndDate)
        {
            string sReturnString = "";
            int nIndex = 0;

            sReturnString = "select DATE_FORMAT(work_date,'%d-%m-%Y') as work_date ,project,main_activity, sub_activity, work_hour, txt_remark from etda_t_project a, etda_t_timesheet b where a.task_id = b.task_id and userid = '";
            sReturnString += sUserID;
            sReturnString += "' ";
            sReturnString += "and (work_date BETWEEN ";
            sReturnString += "'";
            sReturnString += sStartDate;
            sReturnString += "' ";
            sReturnString += "AND '";
            sReturnString += sEndDate;
            sReturnString += "') order by work_date desc";

            return sReturnString;
        }
        public bool ValidateUser(string sUserID)
        {
            bool bStatus = false;
            DataTable dtTableInfo = null;
            try
            {
                string sCommand = "";
                sCommand = GenerateCmdAdmin(sUserID);

                dtTableInfo = SelectDataByCmd(sCommand);
                if (dtTableInfo != null)
                {
                    if (dtTableInfo.Rows.Count > 0)
                    {
                        bStatus = true;
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }
        public string GenerateCmdAdmin(string sUserID)
        {
            string sReturnString = "";
            int nIndex = 0;

            sReturnString = "select * from etda_t_admin where emp_id='";
            sReturnString += sUserID;
            sReturnString += "'";

            return sReturnString;
        }
        //YPT 04.07.2020
        public bool ValidateEntryTime(DateTime dtTime)
        {
            bool bStatus = false;
            int nStatus = 0;

            try
            {
                TimeSpan tBase = new TimeSpan(9, 31, 0);
                TimeSpan tHours = new TimeSpan(dtTime.Hour, dtTime.Minute, dtTime.Second);

                nStatus = TimeSpan.Compare(tHours, tBase);
                if ((nStatus == 1) || (nStatus == 0))
                {
                    bStatus = false;
                }
                else
                {
                    bStatus = true;
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }
        public bool ValidateWorkingHours(DateTime dtTime)
        {
            bool bStatus = false;
            int nStatus = 0;

            try
            {
                TimeSpan tBase = new TimeSpan(9, 0, 0);
                TimeSpan tHours = new TimeSpan(dtTime.Hour, dtTime.Minute, dtTime.Second);

                nStatus = TimeSpan.Compare(tHours, tBase);
                if ((nStatus == 1) || (nStatus == 0))
                {
                    bStatus = true;
                }
                else
                {
                    bStatus = false;
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }
        public bool ExportGridToExcel(string sdtStart,string sdtEnd,GridView grv,int nMode,ref DataTable dt,ref string sFileName)
        {
            bool bStatus = false;
            try
            {
                string sCommand = "";
                
                dt = new DataTable("Report");

                DataTable dtTable = null;
                string sTopic = "";
                Logik process = new Logik();
                if (nMode == 0)
                {
                    sTopic = "TimeAtt_";
                }
                else if (nMode == 1)
                {
                    sTopic = "Report_";
                }
                else
                {
                    sTopic = "TimeSheet_";
                }

                if (String.Equals(sdtStart, sdtEnd, StringComparison.OrdinalIgnoreCase))
                {
                    sFileName = "attachment;filename=" + sTopic + sdtStart + ".xlsx";
                }
                else
                {
                    sFileName = "attachment;filename=" + sTopic + sdtStart + "_" + sdtEnd + ".xlsx";
                }

                if (grv.Visible)
                {
                    bStatus = process.ExportDataFromGridView(grv, dt);
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }
        public bool ExportDataFromGridView(GridView grv, DataTable dt)
        {
            bool bStatus = false;
            int nRowIndex = 0;
            string sCellText = "";
            bool bFoundIfnore = false;
            int nstartIndex = 0;
            int nCount = 0;
            try
            {
                foreach (TableCell cell in grv.HeaderRow.Cells)
                {
                    if (String.Equals(cell.Text, "transid", StringComparison.OrdinalIgnoreCase))
                    {
                        bFoundIfnore = true;
                        nstartIndex = 1;
                    }
                    if ((!String.Equals(cell.Text, "transid", StringComparison.OrdinalIgnoreCase)) && (!String.Equals(cell.Text, "&nbsp;", StringComparison.OrdinalIgnoreCase)))
                    {
                        dt.Columns.Add(cell.Text);
                    }
                }

                //Loop through All Pages
                for (int i = 0; i < grv.PageCount; i++)
                {
                    //Set Page Index
                    grv.SetPageIndex(i);
                    foreach (GridViewRow gvr in grv.Rows)
                    {
                        dt.Rows.Add();
                        if (bFoundIfnore)
                        {
                            nCount = gvr.Cells.Count - 1;
                        }
                        else
                        {
                            nCount = gvr.Cells.Count;
                        }
                        for (int j = nstartIndex; j < nCount; j++)
                        {
                            sCellText = gvr.Cells[j].Text;
                            if (String.Equals(sCellText, "&nbsp;", StringComparison.OrdinalIgnoreCase))
                            {
                                sCellText = "";
                            }
                            if (bFoundIfnore)
                            {
                                dt.Rows[nRowIndex][j - 1] = sCellText;
                            }
                            else
                            {
                                dt.Rows[nRowIndex][j] = sCellText;
                            }
                        }
                        nRowIndex++;
                    }
                }
                if (nRowIndex != 0)
                {
                    bStatus = true;
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }
        public string InsertProjectStatus(string sEmpId,string sTaskId,string sStatus)
        {
            bool bStatus = false;
            string sLastID = "";

            int nIndex = 0;
            try
            {
                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();
                sLastID = sqlCon.InsertProjectStatus(sEmpId,sTaskId, sStatus);

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return sLastID;
        }
        public string InsertProjectStatusWithDate(string sEmpId, string sTaskId, string sStatus,string sDate,string sResultLink)
        {
            bool bStatus = false;
            string sLastID = "";

            int nIndex = 0;
            try
            {
                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();
                sLastID = sqlCon.InsertProjectStatusWithDate(sEmpId, sTaskId, sStatus, sDate, sResultLink);

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return sLastID;
        }
        public string InsertProjectStatusWithReOpen(string sEmpId, string sTaskId, string sStatus, string sDate, string sReason)
        {
            bool bStatus = false;
            string sLastID = "";

            int nIndex = 0;
            try
            {
                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();
                sLastID = sqlCon.InsertProjectStatusWithReOpen(sEmpId, sTaskId, sStatus, sDate, sReason);

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return sLastID;
        }
        public bool UpdateStausId(string sTaskId, string sStatus)
        {
            bool bStatus = false;

            try
            {
                SQLConnect sqlCon = new SQLConnect();
                sqlCon.Initialize();

                 bStatus = sqlCon.UpdateStatusId(sTaskId, sStatus);

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            return bStatus;
        }
    }
}