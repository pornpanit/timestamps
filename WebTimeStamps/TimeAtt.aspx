﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="TimeAtt.aspx.cs" Inherits="WebTimeStamps.TimeAtt" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <link href="assets/css/Gridstyle.css" rel="stylesheet" type="text/css" />

    <link type="text/css" href="assets/css/ui-lightness/jquery-ui-1.8.19.custom.css" rel="stylesheet" />
    <link type="text/css" href="assets/css/main.css" rel="stylesheet" />
    <script type="text/javascript" src="Scripts/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.8.19.custom.min.js"></script>
    <script type="text/javascript">

        $(function () {
            $("#txtDateStart").datepicker();
            $("#txtDateEnd").datepicker();
            $("#txtDateStartStaff").datepicker();
            $("#txtDateEndStaff").datepicker();
            $("#txt_Date").datepicker();
        });
    </script>
    
         <script type="text/javascript"> 
             function hidePopup() {
                 $find('mp1').hide();
             }
        </script>
    <script>
        if (window.history.replaceState) {
            window.history.replaceState(null, null, window.location.href);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" class="center">
         <br/>
             <table class="auto-style3">
                 <tr>
                     <td align="right" class="auto-style6">
                         <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                     </td>
                     <td class="auto-style2" align="right">
                         <asp:LinkButton id="lk_logout" runat="server" OnClick="Onlogout">ออกจากระบบ</asp:LinkButton>
                     </td>
                 </tr>
                 <tr>
                     <td align="right" class="auto-style5" align="right">
                         <asp:Label ID="lbl_name" runat="server" Text="Label" Visible="False"></asp:Label>
                         <asp:Label ID="lbl_surname" runat="server" Text="Label" Visible="False"></asp:Label>
                     </td>
                     <td class="auto-style2" align="right">
                         
                     </td>
                 </tr>
                 <tr>
                     <td align="right" class="auto-style5" align="right">
                         <asp:Label ID="lbl_PS" runat="server" Visible="False"></asp:Label>
                     </td>
                     <td class="auto-style2" align="right">
                         <asp:Label ID="lb_username_surname" runat="server" Text="Label"></asp:Label>
                     </td>
                 </tr>
             </table>
        <br/>
         <table class="auto-style3">
                 <tr>
                     <td class="auto-style2" align="Left">
                         |&nbsp;&nbsp;<asp:Label ID="lbl_timeattendance" runat="server" Text="Time Attendance"></asp:Label>&nbsp;&nbsp;|&nbsp;&nbsp;<asp:LinkButton id="LnK2" runat="server" OnClick="OnTimeSheet">Timesheet</asp:LinkButton>&nbsp;&nbsp;|&nbsp;&nbsp;<asp:LinkButton id="LnK6" runat="server" OnClick="OnTimeSheetPeriod">Timesheet period</asp:LinkButton>&nbsp;&nbsp;|&nbsp;&nbsp;<asp:LinkButton id="LnK3" runat="server" OnClick="OnReport">Report</asp:LinkButton>&nbsp;&nbsp;|&nbsp;&nbsp;<asp:LinkButton id="Lnk5" runat="server" OnClick="OnReportResult">รายงานผลการปฏิบัติงาน</asp:LinkButton>&nbsp;&nbsp;|&nbsp;&nbsp;<asp:LinkButton id="LnK4" runat="server" OnClick="OnStaffUnder">ข้อมูลของผู้ใต้บังคับบัญชา</asp:LinkButton>&nbsp;&nbsp;<asp:Label ID="lbl_end" runat="server" Text="|"/>
                     </td>
                 </tr>
             </table>
             <br/>
         <div class="centerwithborder">
            <br/>
            <table align="center">
                <tr>
                       <td class="auto-style1" align="right">
                            <div>
                                <asp:label ID="lbl_userid" Text="รหัสพนักงาน : " AutoCompleteType="Disabled" runat="server" Visible="False"/>
                            </div>
                        </td>
                        <td class="auto-style1">
                            <asp:TextBox ID="txt_userid" runat="server" Visible="False"></asp:TextBox>
                        </td>
                    </tr>
                <tr>
                    <td class="auto-style1"  align="right">
                        <div>
                            <asp:label ID="lbl_DatStart" Text="ตั้งแต่วันที่ : " AutoCompleteType="Disabled" align="right" runat="server"/>
                        </div>
                    </td>
                    <td class="auto-style1">
                        <div>
                        <asp:TextBox ID="txtDateStart" AutoCompleteType="Disabled" runat="server"/>
                        </div>
                    </td>
                    <td class="auto-style1-small"  align="center">
                        <div>
                            <asp:label ID="Label1" Text="ถึง" AutoCompleteType="Disabled" align="right" runat="server"/>
                        </div>
                    </td>
                    <td class="auto-style1">
                        <div>
                            <asp:TextBox ID="txtDateEnd"  AutoCompleteType="Disabled" runat="server"/>
                        </div>
                    </td>
                </tr>
                    <tr>
                        <td class="auto-style1">
                        </td>
                        <td class="auto-style1">
                           
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style1">
                            
                        </td>
                        <td class="auto-style1">
                            <asp:Button class="buttonRounded" ID="btn_load" runat="server" OnClick="btnload_Click" Text="ค้นหา" />
                            <asp:Label ID="lbl_err" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                        </td>
                     </tr>
            </table>
             <br/>
        </div>
        <br/>

        <div>
            <table align="center" class="center">
                <tr>
                     <td class="auto-style1" align="right">
                           <asp:Button ID="btnUpdate" class="buttonEdit" runat="server" Text="บันทึก" OnClick = "OnUpdate" Visible = "false"/>
                           <asp:Button ID="btnCancle" class="buttonEdit" runat="server" Text="ยกเลิก" OnClick = "OnCancel" Visible = "false"/>
                     </td>
                </tr>
                <tr>
                     <td class="auto-style11">
                          <asp:GridView ID="myGridView" runat="server" AutoGenerateColumns="false" OnRowDataBound = "OnRowDataBound" CssClass="Grid" AlternatingRowStyle-CssClass="alt" OnRowEditing="OnRowEditing" OnPageIndexChanging="SubmitAppraisalGrid_PageIndexChanging" onrowcommand="GridView1_RowCommand">
                            <Columns>
                                <asp:BoundField DataField="datetime" HeaderText="วันที่" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Center" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="userid" HeaderText="รหัส" ItemStyle-Width="120" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" ReadOnly="true"/>
                                <asp:BoundField DataField="username" HeaderText="ชื่อ" ItemStyle-Width="150" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="surname" HeaderText="สกุล" ItemStyle-Width="200" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="starttime" HeaderText="เวลาเข้า" ItemStyle-Width="100" ItemStyle-HorizontalAlign="Center" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="endtime" HeaderText="เวลาออก" ItemStyle-Width="100" ItemStyle-HorizontalAlign="Center" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="workingh" HeaderText="จำนวน ชม." ItemStyle-Width="80" ItemStyle-HorizontalAlign="Center" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
        
                                <asp:TemplateField HeaderText="เวลาเข้า(M)" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <asp:Label ID="lbb_in" runat="server" Text='<%# Eval("starttimem") %>' AutoCompleteType="Disabled" ItemStyle-VerticalAlign="Top" Visible='<%# !String.Equals(Eval("starttimem").ToString(),"-", StringComparison.OrdinalIgnoreCase) || !GetVisible(Eval("datetime").ToString() )%>'></asp:Label>
                                        <asp:Button Visible='<%# String.Equals(Eval("starttimem").ToString(),"-", StringComparison.OrdinalIgnoreCase) && GetVisible(Eval("datetime").ToString())%>' runat="server" CommandArgument='<%#Eval("datetime") + "," + Eval("endtimem") + "," + "Start"%>' Text="ลงเวลา" ID="AddStart" CommandName="InTime" />
                                       <cc1:ConfirmButtonExtender ID="ScriptManager1" runat="server" ConfirmText="ท่านต้องการลงเวลาเข้าใช่หรือไม่ ถ้าคลิก 'OK' จะไม่สามาถแก้ไขข้อมูลได้?"
                                        TargetControlID="AddStart"></cc1:ConfirmButtonExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="เวลาออก(M)" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                       <asp:Label ID="lbb_out" runat="server" Text='<%# Eval("endtimem") %>' AutoCompleteType="Disabled" ItemStyle-VerticalAlign="Top" Visible='<%# !String.Equals(Eval("endtimem").ToString(),"-", StringComparison.OrdinalIgnoreCase) || !GetVisible(Eval("datetime").ToString())%>'></asp:Label>
                                        <asp:Button Visible='<%# String.Equals(Eval("endtimem").ToString(),"-", StringComparison.OrdinalIgnoreCase) && GetVisible(Eval("datetime").ToString()) %>' runat="server" CommandArgument='<%#Eval("datetime") + "," + Eval("starttimem") + "," + "End" %>' Text="ลงเวลา" ID="AddEnd" CommandName="InTime" />
                                       <cc1:ConfirmButtonExtender ID="ScriptManager2" runat="server" ConfirmText="ท่านต้องการลงเวลาออกใช่หรือไม่ ถ้าคลิก 'OK' จะไม่สามาถแก้ไขข้อมูลได้?"
                                        TargetControlID="AddEnd"></cc1:ConfirmButtonExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:BoundField DataField="workinghm" HeaderText="จำนวน ชม.(M)" ItemStyle-Width="80" ItemStyle-HorizontalAlign="Center" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
        
                                <asp:TemplateField HeaderText = "อาการ" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_temperature" runat="server" Text='<%# Eval("status_health") %>' Visible = "false" />
                                        <asp:DropDownList ID="ddltemperatures" runat="server" OnSelectedIndexChanged="ddltemperature_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-Width="300px">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# Eval("comment") %>' AutoCompleteType="Disabled" ItemStyle-VerticalAlign="Top"></asp:Label>
                                        <asp:TextBox ID="txt_comment" runat="server" Text='<%# Eval("comment") %>' Visible="false"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                </table>
		</div>
         <asp:ScriptManager ID="ScriptManager1" runat="server">
         </asp:ScriptManager>
    </form>
</body>
</html>

