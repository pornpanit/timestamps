﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="TimeSheets.aspx.cs" Inherits="WebTimeStamps.TimeSheets" EnableEventValidation = "false" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <link href="assets/css/Gridstyle.css" rel="stylesheet" type="text/css" />

    <link type="text/css" href="assets/css/ui-lightness/jquery-ui-1.8.19.custom.css" rel="stylesheet" />
    <link type="text/css" href="assets/css/main.css" rel="stylesheet" />
    <script type="text/javascript" src="Scripts/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.8.19.custom.min.js"></script>
    <script type="text/javascript">

        $(function () {
            $("#txtDate").datepicker();
            $("#txtDateStart").datepicker();
            $("#txtDateEnd").datepicker();
        });
    </script>
    
         <script type="text/javascript"> 
             function hidePopup() {
                 $find('mp1').hide();
             }
        </script>
         <script>
             if (window.history.replaceState) {
                 window.history.replaceState(null, null, window.location.href);
             }
        </script>
     
     </head>
<body>
    <form id="form1" runat="server" class="center">
         <br/>
             <table class="auto-style3">
                 <tr>
                     <td align="right" class="auto-style6">
                         <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                     </td>
                     <td class="auto-style2" align="right">
                         <asp:LinkButton id="lk_logout" runat="server" OnClick="Onlogout">ออกจากระบบ</asp:LinkButton>
                     </td>
                 </tr>
                 <tr>
                     <td align="right" class="auto-style5" align="right">
                         &nbsp;</td>
                     <td class="auto-style2" align="right">
                         
                     </td>
                 </tr>
                 <tr>
                     <td align="right" class="auto-style5" align="right">
                         <asp:Label ID="lbl_PS" runat="server" Visible="False"></asp:Label>
                     </td>
                     <td class="auto-style2" align="right">
                         <asp:Label ID="lb_username_surname" runat="server" Text="Label"></asp:Label>
                     </td>
                 </tr>
             </table>
        <br/>
        <table class="auto-style3">
                  <tr>
                     <td class="auto-style2" align="Left">
                          |&nbsp;&nbsp;<asp:LinkButton id="LnK1" runat="server" OnClick="OnTimeAtt">Time Attendance</asp:LinkButton>&nbsp;&nbsp;|&nbsp;&nbsp;<asp:Label ID="lbl_timeattendance" runat="server" Text="Timesheet"></asp:Label>&nbsp;&nbsp;|&nbsp;&nbsp;<asp:LinkButton id="LnK6" runat="server" OnClick="OnTimeSheetPeriod">Timesheet period</asp:LinkButton>&nbsp;&nbsp;|&nbsp;&nbsp;<asp:LinkButton id="LnK3" runat="server" OnClick="OnReport">Report</asp:LinkButton>&nbsp;&nbsp;|&nbsp;&nbsp;<asp:LinkButton id="Lnk5" runat="server" OnClick="OnReportResult">รายงานผลการปฏิบัติงาน</asp:LinkButton>&nbsp;&nbsp;|&nbsp;&nbsp;<asp:LinkButton id="LnK4" runat="server" OnClick="OnStaffUnder">ข้อมูลของผู้ใต้บังคับบัญชา</asp:LinkButton>&nbsp;&nbsp;<asp:Label ID="lbl_end" runat="server" Text="|"/>
                     </td>
                 </tr>
				 
             </table>
             <br/>
        <div class="centerwithborder">
            <br/>
            <table class="auto-style3">
                <tr>
                       <td class="auto-style1" align="right">
                            <div>
                                <asp:label ID="lbl_userid" Text="รหัสพนักงาน : " AutoCompleteType="Disabled" runat="server" Visible="False"/>
                            </div>
                        </td>
                        <td class="auto-style1">
                            <asp:TextBox ID="txt_userid" runat="server" Visible="False"></asp:TextBox>
                        </td>
                    </tr>
                <tr>
                    <td class="auto-style1"  align="right">
                        <div>
                            <asp:label ID="lbl_DatStart" Text="ตั้งแต่วันที่ : " AutoCompleteType="Disabled" align="right" runat="server"/>
                        </div>
                    </td>
                    <td class="auto-style1">
                        <div>
                        <asp:TextBox ID="txtDate" AutoCompleteType="Disabled" runat="server"/>
                        </div>
                    </td>
                  
                </tr>
                    <tr>
                        <td class="auto-style1"  align="right">
                        <div>
                            <asp:label ID="Label1" Text=" โครงการสำนัก : " AutoCompleteType="Disabled" align="right" runat="server"/>
                        </div>
                    </td>
                    <td class="auto-style1">
                        <div>
                            <asp:DropDownList ID="ddp_mainproject" runat="server" Height="30px" Width="273px" OnSelectedIndexChanged="ddpproject_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </td>
                    </tr>
                <tr>
                        <td class="auto-style1"  align="right">
                        <div>
                            <asp:label ID="Label3" Text="กิจกรรม : " AutoCompleteType="Disabled" align="right" runat="server"/>
                        </div>
                    </td>
                    <td class="auto-style1">
                        <div>
                            <asp:DropDownList ID="ddp_delimain" runat="server" Height="30px" Width="272px" OnSelectedIndexChanged="ddpamin_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </td>
                    </tr>
                <tr>
                        <td class="auto-style1"  align="right">
                        <div>
                            <asp:label ID="Label4" Text="สิ่งส่งมอบหลัก : " AutoCompleteType="Disabled" align="right" runat="server"/>
                        </div>
                    </td>
                    <td class="auto-style1">
                        <div>
                            <asp:DropDownList ID="ddp_delisub" runat="server" Height="30px" Width="272px" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </td>
                    </tr>
                <tr>
                        <td class="auto-style1"  align="right">
                        <div>
                            <asp:label ID="Label5" Text="จำนวนชั่วโมง : " AutoCompleteType="Disabled" align="right" runat="server"/>
                        </div>
                    </td>
                    <td class="auto-style1">
                        <div>
                            <asp:DropDownList ID="ddp_hours" runat="server" Height="30px" Width="47px">
                                <asp:ListItem>1</asp:ListItem>
                                <asp:ListItem>2</asp:ListItem>
                                <asp:ListItem>3</asp:ListItem>
                                <asp:ListItem>4</asp:ListItem>
                                <asp:ListItem>5</asp:ListItem>
                                <asp:ListItem>6</asp:ListItem>
                                <asp:ListItem>7</asp:ListItem>
                                <asp:ListItem>8</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </td>
                    </tr>
                <tr>
                        <td class="auto-style1" align="right" valign="top">
                        <div>
                            <asp:label ID="Label6" Text="รายละเอียดเพิ่มเติม : " AutoCompleteType="Disabled" align="right" runat="server"/>
                        </div>
                    </td>
                    <td class="auto-style1">
                        <div>
                        <asp:TextBox ID="txt_additional" AutoCompleteType="Disabled" runat="server" Height="94px" Width="272px" TextMode="MultiLine"/>
                        </div>
                    </td>
                    </tr>
                <tr>
                        <td class="auto-style1"  align="right">
                        <div>
                        </div>
                    </td>
                    <td class="auto-style1">
                        <div>
                        </div>
                    </td>
                    </tr>
                    <tr>
                        <td class="auto-style1">
                            
                        </td>
                        <td class="auto-style1">
                            <asp:Button class="buttonRounded" ID="btn_load" runat="server" OnClick="btnload_Click" Text="Submit" />
                            &nbsp;&nbsp;<asp:Label ID="lbl_error" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                            <asp:Label ID="lbl_ok" runat="server" Visible="False" ForeColor="#009933"></asp:Label>
                        </td>
                     </tr>

            </table>
             <br/>
        </div>
        <br/>
        <div>
            <table align="center" class="center">
              
                <tr>
                     <td class="auto-style5" align="left" colspan="2">
                         
                    &nbsp;&nbsp;<asp:Label ID="lbl_start" runat="server" Text="ตั้งแต่วันที่ "></asp:Label>
                         &nbsp;<asp:TextBox ID="txtDateStart" AutoCompleteType="Disabled" runat="server"/>
                        &nbsp; <asp:Label ID="lbl_to" runat="server" Text="ถึง"></asp:Label>
&nbsp; <asp:TextBox ID="txtDateEnd" AutoCompleteType="Disabled" runat="server"/>
                        &nbsp;&nbsp;<asp:Button class="buttonRounded" ID="btn_search" runat="server" OnClick="btnloadsearch_Click" Text="ค้นหา" />
                            &nbsp;
                          &nbsp;
                         
                    &nbsp;&nbsp;
                       <asp:CheckBox ID="Ck_Export" runat="server" CssClass="checkbox-centered" Text="Export data" OnCheckedChanged="Ck_Export_CheckedChanged" AutoPostBack="true"/> &nbsp;
                        <asp:ImageButton ID="imgExcel" runat="server" Height="24px" ImageUrl="~/img/ex.png" Width="28px" Visible="False" OnClick="imgExcel_Click" ImageAlign="Top" />
                     &nbsp;
                     </td>
                    
                </tr>
                 <tr>
                     <td class="auto-style1" align="right">
                        
                     </td>
                </tr>
                <tr>
                     <td class="center">
                         <asp:GridView ID="myGridView" runat="server" AutoGenerateColumns="false" CssClass="Grid" AlternatingRowStyle-CssClass="alt" OnPageIndexChanging="SubmitAppraisalmygridview_PageIndexChanging" onrowcommand="GridView1_RowCommand">
                            <Columns>
                                <asp:BoundField DataField="trans_id" HeaderText="transid" ItemStyle-Width="120" ItemStyle-HorizontalAlign="Center" ReadOnly="true" ItemStyle-VerticalAlign="Top" Visible =" false"/>
                                <asp:BoundField DataField="work_date" HeaderText="Date" ItemStyle-Width="120" ItemStyle-HorizontalAlign="Center" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="project" HeaderText="Project" ItemStyle-Width="200" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top" ReadOnly="true"/>
                                <asp:BoundField DataField="main_activity" HeaderText="Main output" ItemStyle-Width="200" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="sub_activity" HeaderText="Sub output" ItemStyle-Width="200" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                <asp:BoundField DataField="work_hour" HeaderText="WH" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>
                                 <asp:BoundField DataField="txt_remark" HeaderText="Remark" ItemStyle-Width="400" ItemStyle-HorizontalAlign="Left" ReadOnly="true" ItemStyle-VerticalAlign="Top"/>

                               <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server"
                                            CommandArgument='<%# Eval("trans_id") %>' 
				                             CommandName="dele" OnClientClick="if (!confirm('คุณต้องการที่จะลบรายการนี้ ?')) return false;">Delete</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                </table>
		</div>
    </form>
</body>
</html>
