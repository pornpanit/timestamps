﻿using ClosedXML.Excel;
using MySqlDatabase;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebTimeStamps
{
    public partial class TimeSheets : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                try
                {
                    // + Convert.ToString(Session["password"]);
                    bool bStatus = false;

                    string sUserName = "";
                    string sSamaccountname = "";
                    string sName = "";
                    string sEmail = "";
                    string sPassword = "";
                    string sTitle = "";
                    string sUsernamesurenameth = "";
                    string sUserID = "";
                    string sDateStart = "";
                    string sDateEnd = "";

                    sUserName = Convert.ToString(Session["user"]);
                    sPassword = Convert.ToString(Session["password"]);
                    sSamaccountname = Convert.ToString(Session["samaccountname"]);
                    sName = Convert.ToString(Session["name"]);
                    sEmail = Convert.ToString(Session["email"]);
                    sTitle = Convert.ToString(Session["title"]);
                    sUserID = Convert.ToString(Session["userid"]);
                    if (ValidateSession())
                    {
                        sUsernamesurenameth = Convert.ToString(Session["usernamesurenameth"]);
                        lb_username_surname.Text = sUsernamesurenameth;
                        txt_userid.Text = sUserID;
                        lbl_PS.Text = sTitle;

                        DateTime dtCurrent = DateTime.Now;
                        txtDate.Text = dtCurrent.ToString("dd/MM/yyyy");

                        Logik process = new Logik();
                        process.GetStartAndEndDateView(ref sDateStart, ref sDateEnd);

                        DateTime dtDateS = DateTime.ParseExact(sDateStart, "yyyy-MM-dd", null);
                        DateTime dtDateE = DateTime.ParseExact(sDateEnd, "yyyy-MM-dd", null);

                        sDateStart = dtDateS.ToString("dd/MM/yyyy");
                        sDateEnd = dtDateE.ToString("dd/MM/yyyy");

                        txtDateStart.Text = sDateStart;
                        txtDateEnd.Text = sDateEnd;

                        GetProjectList();
                        BindData(true);

                        EnableStaffMode();
                    }
                }
                catch (Exception ex)
                {
                    // MessageBox.Show(ex.Message);

                }

            }
        }
        protected void btnload_Click(object sender, EventArgs e)
        {
            try
            {

                //Insert data
                string sEmpId = "";
                string sTaskId = "";
                string sWorkDate = "";
                string sWorkHour = "";
                string sTxtRemark = "";
                string sUpdUsreId = "";
                string sMessage = "";
                bool bStatus = false;

                lbl_ok.Text = sMessage;
                lbl_error.Text = sMessage;
                lbl_ok.Visible = false;
                lbl_error.Visible = false;

                sEmpId = txt_userid.Text;
                sTaskId = ddp_delisub.SelectedValue;

                DateTime dtDate = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null);

                sWorkDate = dtDate.ToString("yyyy-MM-dd");
                sWorkHour = ddp_hours.SelectedValue;
                sTxtRemark = txt_additional.Text;
                sUpdUsreId = txt_userid.Text;

                Logik process = new Logik();
                bStatus = process.InsertData(sEmpId, sTaskId, sWorkDate, sWorkHour, sTxtRemark, sUpdUsreId,0);
                if (bStatus)
                {
                    sMessage = "Completed";
                    /*System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append("<script type = 'text/javascript'>");
                    sb.Append("window.onload=function(){");
                    sb.Append("alert('");
                    sb.Append(sMessage);
                    sb.Append("')};");
                    sb.Append("</script>");
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());*/

                    lbl_error.Visible = false;
                    lbl_ok.Text = sMessage;
                    txt_additional.Text = "";
                    lbl_ok.Visible = true;

                    BindData(true);
                }
                else
                {
                    sMessage = "Failed! Please check the data.";
                    /*System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append("<script type = 'text/javascript'>");
                    sb.Append("window.onload=function(){");
                    sb.Append("alert('");
                    sb.Append(sMessage);
                    sb.Append("')};");
                    sb.Append("</script>");
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());*/
                    lbl_error.Text = sMessage;
                    lbl_ok.Visible = false;
                    lbl_error.Visible = true;
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        public void Onlogout(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("LoginAttendance.aspx");
        }
        public void OnTimeAtt(object sender, EventArgs e)
        {
            Response.Redirect("TimeAtt.aspx");
        }
        public void OnTimeSheetPeriod(object sender, EventArgs e)
        {
            Response.Redirect("TimeSheetsPeriod.aspx");
        }
        public void OnStaffUnder(object sender, EventArgs e)
        {
            Session["parentpage"] = "TimeSheets.aspx";
            Response.Redirect("StaffViewer.aspx");
        }
        public void OnReport(object sender, EventArgs e)
        {
            Response.Redirect("ReportViewer.aspx");
        }
        public void OnReportResult(object sender, EventArgs e)
        {
            Response.Redirect("ReportResult.aspx");
        }

        public bool GetProjectList()
        {
            bool bStatus = false;
            string sCommand = "";
            DataTable dtResult;
            string sSelectionId = "";
            try
            {
                Logik process = new Logik();
                sCommand = "select distinct substring(task_id,1,3) task_id, project from etda_t_project where statusview = 1 order by 2;";
                dtResult = process.SelectDataByCmd(sCommand);
                if (dtResult != null)
                {
                    string sID = "";
                    string sValue = "";

                    foreach (DataRow row in dtResult.Rows)
                    {
                        sID = row["task_id"].ToString();
                        sValue = row["project"].ToString();
                        if ((!string.IsNullOrEmpty(sValue)) && (!string.IsNullOrEmpty(sID)))
                        {
                            ddp_mainproject.Items.Add(new ListItem(sValue, sID));
                        }
                    }
                }
                sSelectionId = ddp_mainproject.SelectedValue;
                GetMainAcivityList(sSelectionId);

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        public bool GetMainAcivityList(string sID)
        {
            bool bStatus = false;
            string sCommand = "";
            DataTable dtResult;
            string sSelectionId = "";
            try
            {
                Logik process = new Logik();
                if (string.IsNullOrEmpty(sID))
                {
                    //sCommand = "select distinct substring(task_id,1,6) task_id, main_activity from etda_t_project;";
                    //YPT 11.04.2020 get statusview for validate enable data.
                    sCommand = "select distinct substring(task_id,1,6) task_id, main_activity from etda_t_project where statusview = 1;";
                }
                else
                {//YPT 11.04.2020 get statusview for validate enable data.
                    //sCommand = "select distinct substring(task_id,1,6) task_id, main_activity from etda_t_project where substring(task_id,1,3) = substring(";
                    sCommand = "select distinct substring(task_id,1,6) task_id, main_activity from etda_t_project where statusview = 1 and substring(task_id,1,3) = substring(";
                    sCommand += "'";
                    sCommand += sID;
                    sCommand += "'";
                    sCommand += ",1,3) order by 2;";
                }

                dtResult = process.SelectDataByCmd(sCommand);
                if (dtResult != null)
                {
                    string sIndex = "";
                    string sValue = "";
                    ddp_delimain.Items.Clear();

                    foreach (DataRow row in dtResult.Rows)
                    {
                        sIndex = row["task_id"].ToString();
                        sValue = row["main_activity"].ToString();
                        ddp_delimain.Items.Add(new ListItem(sValue, sIndex));
                    }
                }
                sSelectionId = ddp_delimain.SelectedValue;
                GetSubAcivityList(sSelectionId);
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        public bool GetSubAcivityList(string sID)
        {
            bool bStatus = false;
            string sCommand = "";
            DataTable dtResult;
            try
            {
                Logik process = new Logik();
                if (string.IsNullOrEmpty(sID))
                {
                    sCommand = "select distinct task_id, sub_activity from etda_t_project where statusview = 1;";
                }
                else
                {
                    sCommand = "select distinct task_id, sub_activity from etda_t_project where statusview = 1 and substring(task_id,1,6) = substring(";
                    sCommand += "'";
                    sCommand += sID;
                    sCommand += "'";
                    sCommand += ",1,6) order by 2;";
                }


                dtResult = process.SelectDataByCmd(sCommand);
                if (dtResult != null)
                {
                    string sIndex = "";
                    string sValue = "";

                    ddp_delisub.Items.Clear();

                    foreach (DataRow row in dtResult.Rows)
                    {
                        sIndex = row["task_id"].ToString();
                        sValue = row["sub_activity"].ToString();
                        ddp_delisub.Items.Add(new ListItem(sValue, sIndex));
                    }

                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

            return bStatus;
        }
        protected void SubmitAppraisalmygridview_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                BindData(false);
                myGridView.PageIndex = e.NewPageIndex;
                myGridView.DataBind();

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }

        }
        public void BindData(bool bSearch)
        {
            bool bStatus = false;
            DataTable dtTableInfo = null;
            string sCommand = "";
            string sDateStart = "";
            string sDateEnd = "";
            try
            {
                Logik process = new Logik();
                //process.GetStartAndEndDateView(ref sDateStart, ref sDateEnd);
                //YPT 04.01.2020
                DateTime dtDateS = DateTime.ParseExact(txtDateStart.Text, "dd/MM/yyyy", null);
                DateTime dtDateE = DateTime.ParseExact(txtDateEnd.Text, "dd/MM/yyyy", null);

                sDateStart = dtDateS.ToString("yyyy-MM-dd");
                sDateEnd = dtDateE.ToString("yyyy-MM-dd");

                sCommand = process.GenerateCommandViewTimeSheet(txt_userid.Text, sDateStart, sDateEnd);

                dtTableInfo = process.SelectDataByCmd(sCommand);
                if (dtTableInfo != null)
                {
                    if (dtTableInfo.Rows.Count > 0)
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.AddRange(new DataColumn[7] { new DataColumn("trans_id"), new DataColumn("work_date"), new DataColumn("project"), new DataColumn("main_activity"), new DataColumn("sub_activity"), new DataColumn("work_hour"), new DataColumn("txt_remark") });

                        foreach (DataRow row in dtTableInfo.Rows)
                        {
                            DateTime dtDatetime = (DateTime)row["work_date"];
                            string sDatetimeS = dtDatetime.ToString("dd-MM-yyyy");
                            string sProject = row["project"].ToString();
                            string sMainAc = row["main_activity"].ToString();
                            string sSubAc = row["sub_activity"].ToString();
                            string sWH = row["work_hour"].ToString();
                            string sRemark = row["txt_remark"].ToString();
                            string sTransID = row["trans_id"].ToString();

                            dt.Rows.Add(sTransID, sDatetimeS, sProject, sMainAc, sSubAc, sWH, sRemark);

                        }

                        myGridView.PageSize = 10;
                        myGridView.AllowPaging = true;
                        myGridView.DataSource = dt;
                        if (bSearch)
                        {
                            myGridView.PageIndex = 0;
                        }
                        myGridView.DataBind();
                        bStatus = true;
                        myGridView.Visible = true;
                        Ck_Export.Visible = true;

                    }
                    else
                    {
                        myGridView.Visible = false;
                        Ck_Export.Checked = false;
                        Ck_Export.Visible = false;
                        imgExcel.Visible = false;

                        ShowMessage("ไม่พบข้อมูล");
                    }
                }
                else
                {
                    myGridView.Visible = false;
                    Ck_Export.Checked = false;
                    Ck_Export.Visible = false;
                    imgExcel.Visible = false;

                    ShowMessage("ไม่พบข้อมูล");
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
      
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "dele")
            {
                Logik process = new Logik();
                process.DeleteData(e.CommandArgument.ToString(),0);
                BindData(false);
            }
        }
        protected void ddpproject_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sSelectionId = "";
            sSelectionId = ddp_mainproject.SelectedValue;
            GetMainAcivityList(sSelectionId);
        }
        protected void ddpamin_SelectedIndexChanged(object sender, EventArgs e)
        {

            string sSelectionId = "";
            sSelectionId = ddp_delimain.SelectedValue;
            GetSubAcivityList(sSelectionId);
        }
        public override void VerifyRenderingInServerForm(Control control)
        {

        }
        
        public void AddDataStaffUnderToTable(List<PersonInfoWithUnder> lsPerson, List<PersonInfoWithUnder> lsPersonStaff)
        {
            AddStaffUnder(lsPerson, lsPersonStaff);
        }
        public void AddStaffUnder(List<PersonInfoWithUnder> lsPerson, List<PersonInfoWithUnder> lsPersonStaff)
        {
            try{
                foreach (PersonInfoWithUnder item in lsPerson)
                {
                    AddDataList(item, lsPersonStaff);
                    if (item.arrPersonInfo != null)
                    {
                        if (item.arrPersonInfo.Count > 0)
                        {
                            AddStaffUnder(item.arrPersonInfo, lsPersonStaff);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
           
        }
        public void AddDataList(PersonInfoWithUnder item,List<PersonInfoWithUnder> lsPerson)
        {
            string sId = "";
            try {

                foreach (PersonInfoWithUnder itemDetails in lsPerson)
                {
                    if (String.Equals(item.name, itemDetails.fullNameEN, StringComparison.OrdinalIgnoreCase))
                    {
                        sId = itemDetails.id;
                        item.fullNameTH = itemDetails.fullNameTH;
                        item.id = sId;
                        item.depNameTH = itemDetails.depNameTH;

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
            
        }
        
        public void EnableStaffMode()
        {
            if (String.Equals(lbl_PS.Text, "Division Director", StringComparison.OrdinalIgnoreCase))
            {
                //ck_Staff.Visible = true;
                LnK4.Visible = true;
                lbl_end.Visible = true;
            }
            else
            {
                LnK4.Visible = false;
                lbl_end.Visible = false;
                //  ck_Staff.Visible = false;
                //For test
                 /*if (String.Equals(lbl_PS.Text, "Assistant Division Director", StringComparison.OrdinalIgnoreCase))
                 {
                     //ck_Staff.Visible = true;
                     LnK4.Visible = true;

                 }
                 else
                 {
                     // ck_Staff.Visible = false;
                     LnK4.Visible = false;

                 }*/
            }
        }
        public bool ValidateSession()
        {
            bool bStatus = false;
            string sEmail = "";
            string sPassword = "";

            sPassword = Convert.ToString(Session["password"]);
            sEmail = Convert.ToString(Session["email"]);

            if ((string.IsNullOrEmpty(sPassword)) || (string.IsNullOrEmpty(sEmail)))
            {
                Session.Clear();
                Response.Redirect("LoginAttendance.aspx");
            }
            else
            {
                bStatus = true;
            }

            return bStatus;
        }

        protected void btnloadsearch_Click(object sender, EventArgs e)
        {
            lbl_ok.Visible = false;
            lbl_error.Visible = false;
            
            BindData(true);
        }

        protected void imgExcel_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ExportGridToExcel();

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        private void ExportGridToExcel()
        {
            try
            {
                string sDateStart = "";
                string sDateEnd = "";
                string sFileName = "";
                DataTable dt = null;

                DateTime dtDateS = DateTime.ParseExact(txtDateStart.Text, "dd/MM/yyyy", null);
                DateTime dtDateE = DateTime.ParseExact(txtDateEnd.Text, "dd/MM/yyyy", null);
                sDateStart = dtDateS.ToString("yyyy-MM-dd");
                sDateEnd = dtDateE.ToString("yyyy-MM-dd");

                Logik process = new Logik();
                BindData(true);

                if (myGridView.Visible)
                {
                    process.ExportGridToExcel(sDateStart, sDateEnd, myGridView, 2, ref dt, ref sFileName);
                }
               
                if (dt != null)
                {
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(dt);

                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", sFileName);
                        using (MemoryStream MyMemoryStream = new MemoryStream())
                        {
                            wb.SaveAs(MyMemoryStream);
                            MyMemoryStream.WriteTo(Response.OutputStream);
                            Response.Flush();
                            Response.End();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);

            }
        }
        protected void Ck_Export_CheckedChanged(object sender, EventArgs e)
        {
            if (Ck_Export.Checked)
            {
                imgExcel.Visible = true;
            }
            else
            {
                imgExcel.Visible = false;
            }

        }
        public void ShowMessage(string sText)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("window.onload=function(){");
            sb.Append("alert('");
            sb.Append(sText);
            sb.Append("')};");
            sb.Append("</script>");
            ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
        }

    }
}