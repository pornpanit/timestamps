﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebTimeStamps
{
    public partial class Manageuser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                try
                {
                    // + Convert.ToString(Session["password"]);
                    bool bStatus = false;

                    string sUsernamesurenameth = "";
                    string sUserId = "";

                    if (ValidateSession())
                    {
                        sUsernamesurenameth = Convert.ToString(Session["usernamesurenameth"]);
                        lb_username_surname.Text = sUsernamesurenameth;

                        sUserId = Convert.ToString(Session["userid"]);
                        txt_userid.Text = sUserId;

                        rdb_update.Checked = true;

                        string script = "$(document).ready(function () { $('[id*=rdb_update]').Checked(); });";
                        ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);

                    }

                }
                catch (Exception ex)
                {
                    // MessageBox.Show(ex.Message);

                }

            }
        }
        public bool ValidateSession()
        {
            bool bStatus = false;
            string sEmail = "";
            string sPassword = "";

            sPassword = Convert.ToString(Session["password"]);
            sEmail = Convert.ToString(Session["email"]);

            if ((string.IsNullOrEmpty(sPassword)) || (string.IsNullOrEmpty(sEmail)))
            {
                Session.Clear();
                Response.Redirect("LoginAttendance.aspx");
            }
            else
            {
                bStatus = true;
            }

            return bStatus;
        }
        public void Onlogout(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("LoginAttendance.aspx");
        }
        public void OnTimeAtt(object sender, EventArgs e)
        {
            Response.Redirect("TimeAtt.aspx");
        }

        public void OnTimeSheet(object sender, EventArgs e)
        {
            Response.Redirect("TimeSheets.aspx");
        }
        public void OnTimeSheetPeriod(object sender, EventArgs e)
        {
            Response.Redirect("TimeSheetsPeriod.aspx");
        }
        public void OnReport(object sender, EventArgs e)
        {
            Response.Redirect("ReportViewer.aspx");
        }

        protected void rdb_update_CheckedChanged(object sender, EventArgs e)
        {
            int nResult = 0;
        }

        protected void btn_seach_Click(object sender, EventArgs e)
        {

        }

        protected void rdb_delete_CheckedChanged(object sender, EventArgs e)
        {

        }
       
    }
}